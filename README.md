# Timbrel

Custom ubuntu ISO creator (cubic fork)

![Timbrel main window screenshot](https://raw.githubusercontent.com/sallecta/timbrel/master/screenshots/timbrel-01-main_window.png)


# Dependencies

```console
sudo apt install gir1.2-gtksource-3.0 gir1.2-vte-2.91 schroot
```

# Run

## Run directly

```console
sudo -H python3 src/timbrel.py
```

## Run via script

```console
./run_timbrel.sh
```


