# https://linuxconfig.org/how-to-extract-and-repackage-initial-ram-disk-initrd

export LANG=C

pthis="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
if [[ "$pthis" == "" ]]  || [[ "$pthis" = "/" ]];then
	echo "$LINENO: Wrong working path: $pthis"
	exit
fi

pignore=$pthis"/ignore"
pextracted=$pignore"/extracted"
initrd_cpio='initrd'
initrd_lz=$initrd_cpio'.lz'
new_initrd_lz=$initrd_cpio'.new.lz'

fn_stoponerror ()
{
	# Usage:
	# fn_stoponerror $BASH_SOURCE $LINENO $?
	from=$1
	line=$2
	error=$3
	if [[ $error -ne 0 ]]; then
		printf "\n$from: line $line: error: $error\n"
		exit $error
	fi
}

fn_unpack()
{
	if [ -f $pthis/$initrd_cpio ]; then
		echo "  $LINENO: Deleting $pthis/$initrd_cpio"
		rm $pthis/$initrd_cpio
		fn_stoponerror $BASH_SOURCE $LINENO $?
	fi
	echo "  $LINENO: Changing to dir $pignore"
	cd $pignore
	fn_stoponerror $BASH_SOURCE $LINENO $?
	if [ -f $pignore/$initrd_cpio ]; then
		echo "$LINENO: Deleting file $pignore/$initrd_cpio"
		rm $pignore/$initrd_cpio
		fn_stoponerror $BASH_SOURCE $LINENO $?
	fi
	echo "  $LINENO: Extracting $initrd_lz"
	7z e $pignore/$initrd_lz
	fn_stoponerror $BASH_SOURCE $LINENO $?
	
	echo "  $LINENO: Extracting $initrd_cpio"
	if [ -d $pextracted ]; then
		echo "  $LINENO: Deleting $pextracted"
		rm -r $pextracted
		fn_stoponerror $BASH_SOURCE $LINENO $?
	fi
	echo "  $LINENO: Creatin dir $pextracted"
	mkdir $pextracted
	fn_stoponerror $BASH_SOURCE $LINENO $?
	echo "  $LINENO: Changing to dir $pextracted"
	cd $pextracted
	fn_stoponerror $BASH_SOURCE $LINENO $?
	
	echo "  $LINENO: Extracting $initrd_cpio"
	cpio --extract --make-directories < ../$initrd_cpio
	fn_stoponerror $BASH_SOURCE $LINENO $?

	echo "  $LINENO: Deleting $pignore/$initrd_cpio"
	rm $pignore/$initrd_cpio
	fn_stoponerror $BASH_SOURCE $LINENO $?

	echo "  $LINENO: Done"
	#7z e -so ../$initrd_lz | cpio -id
}

fn_pack()
{
	if [ ! -d $pextracted ]; then
		echo "  $LINENO: Missing source dir $pextracted"
		return
	fi
	if [ -f $pignore/$new_initrd_lz ]; then
		echo "$LINENO: Deleting file $pignore/$new_initrd_lz"
		rm $pignore/$new_initrd_lz
		fn_stoponerror $BASH_SOURCE $LINENO $?
	fi
	if [ -f $pignore/$initrd_cpio ]; then
		echo "$LINENO: Deleting file $pignore/$initrd_cpio"
		rm $pignore/$initrd_cpio
		fn_stoponerror $BASH_SOURCE $LINENO $?
	fi
	echo "  $LINENO: Changing to dir $pextracted"
	cd $pextracted
	fn_stoponerror $BASH_SOURCE $LINENO $?
	
	echo "  $LINENO: Creating cpio $initrd_cpio"
	find | cpio -o -H newc > ../$initrd_cpio
	fn_stoponerror $BASH_SOURCE $LINENO $?

	echo "  $LINENO: Changing to dir $pignore"
	cd $pignore
	fn_stoponerror $BASH_SOURCE $LINENO $?
	
	echo "  $LINENO: Creating lz $new_initrd_lz"
	#7z a -m0=lzma:a=1 $new_initrd_lz $initrd_cpio
	lzma --stdout $initrd_cpio > $new_initrd_lz
	fn_stoponerror $BASH_SOURCE $LINENO $?
	
	#echo "  $LINENO: Deleting $pignore/$initrd_cpio"
	#rm $pignore/$initrd_cpio
	#fn_stoponerror $BASH_SOURCE $LINENO $?
	
	echo "  $LINENO: Done"
	
}

argc1=$1
if [[ "$argc1" == 'unpack' ]];then
	echo "$LINENO: Unpacking"
	fn_unpack
elif [[ "$argc1" == 'pack' ]];then
	echo "$LINENO: Packing"
	fn_pack
else
	echo "$LINENO: Wrong arg: $argc1"
fi

