#!/usr/bin/python3

from dbg import *
dbg = dbg_class(nd=1).dbg

import vars

def version():
	version = "incorrectVersion"
	try:
		f = open(vars.app.path + "/version.txt", "r")
		version = f.read(15)
	except FileNotFoundError:
		pass
	return version
