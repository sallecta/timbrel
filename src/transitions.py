#!/usr/bin/python3

########################################################################
#                                                                      #
# transitions.py                                                       #
#                                                                      #
# Copyright (C) 2015 PJ Singh <psingh.cubic@gmail.com>                 #
#                                                                      #
########################################################################

########################################################################
#                                                                      #
# This file is part of Timbrel - Custom Ubuntu ISO Creator.              #
#                                                                      #
# Timbrel is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# (at your option) any later version.                                  #
#                                                                      #
# Timbrel is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public License    #
# along with Timbrel. If not, see <http://www.gnu.org/licenses/>.        #
#                                                                      #
########################################################################
from dbg import *
dbg = dbg_class(nd=1).dbg

import vars
import ui
import handlers
import model
import utils
import validate

import configparser
import glob
import os
import re
import time

MAXIMUM_ISO_SIZE_GIB = 8000.0
MAXIMUM_ISO_SIZE_BYTES = MAXIMUM_ISO_SIZE_GIB * 1073741824.0



# General steps for each transiton.
#
# Note: Deactivating the current page is done by showing the spinner.
#       Activating a new page is done by hiding the spinner.
#
# [1] Perform functions on the current page, and deactivate the current page.
# [2] Prepare and display the new page.
# [3] Perform functions on the new page, and activate the new page.

########################################################################
# TransitionThread - Transition
########################################################################


# TODO: Update arguments to include button labels.
#       reset_buttons(is_quit_visible=False, is_back_visible=False,
#           is_next_visible=False, quit_button_label='Quit',
#           back_button_label='Back', next_button_label='Next')
def transition(
		old_page_name,
		new_page_name,
		quit_visible,
		back_visible,
		next_visible):
	dbg('Performing requested transition action')
	dbg('Transition from', old_page_name)
	dbg('Transition to', new_page_name)

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons()

	# [2] Prepare and display the new page.

	ui.show_page(old_page_name, new_page_name)
	ui.reset_buttons(quit_visible, back_visible, next_visible)

	# [3] Perform functions on the new page, and activate the new page.

	ui.hide_spinner()


########################################################################
# TransitionThread - Transition "Next" Functions
########################################################################

# File Choosers
# Project Directory Page
# New Project Page
# Existing Project Page
# Delete Project Page
# Unsquashfs Page
# Terminal Page
# Copy Files Page
# Options Page
# Repackage ISO Page
# Finish Page

#
# Filechoosers
#


def from__project_directory_page__project_directory_filechooser__to__project_directory_page(
		thread):

	dialog = vars.app.builder.get_object(
		'project_directory_page__project_directory_filechooser')
	vars.prj.path.root = dialog.get_filename()
	ui.update_entry(
		'project_directory_page__project_directory_entry',
		vars.prj.path.root)

	ui.set_sensitive('window', True)


def from__original_iso_filepath_filechooser__to__new_project_page(thread):
	model.set_propagate(False)
	dialog = vars.app.builder.get_object(
		'new_project_page__original_iso_filepath_filechooser')
	# Original
	vars.prj.path.o_iso=dialog.get_filename()
	model.set_original_iso_filename(
		re.sub(r'.*/(.*$)',
			   r'\1',
			   vars.prj.path.o_iso))
	model.set_original_iso_directory(dialog.get_current_folder())
	if not utils.is_mounted(vars.prj.path.o_iso,vars.prj.path.o_iso_m):
		utils.mount_iso_image(vars.prj.path.o_iso,vars.prj.path.o_iso_m,thread)
	else:
		dbg(
			'The original ISO image is already mounted at',
			vars.prj.path.o_iso_m)

	if utils.is_mounted(vars.prj.path.o_iso,vars.prj.path.o_iso_m):
		# Original
		model.set_original_iso_volume_id(
			utils.get_iso_image_volume_id(vars.prj.path.o_iso,thread)
			)
		model.set_original_iso_release_name(
			utils.get_iso_image_release_name(
				vars.prj.path.o_iso_m,
				thread))
		model.set_original_iso_disk_name(
			utils.get_iso_image_disk_name(
				vars.prj.path.o_iso_m,
				thread))
		model.set_casper_relative_directory(
			utils.get_casper_relative_directory(
				vars.prj.path.o_iso_m))

		# Custom
		model.set_custom_iso_version_number(
			utils.create_custom_iso_version_number())
		model.set_custom_iso_filename(
			utils.create_custom_iso_filename(
				model.original_iso_filename,
				model.custom_iso_version_number))
		model.set_custom_iso_dir(vars.prj.path.root)
		model.set_custom_iso_filepath(
			os.path.join(
				model.custom_iso_dir,
				model.custom_iso_filename))
		model.set_custom_iso_volume_id(
			utils.create_custom_iso_volume_id(
				model.original_iso_volume_id,
				model.custom_iso_version_number))
		model.set_custom_iso_release_name(
			utils.create_custom_iso_release_name(
				model.original_iso_release_name))
		model.set_custom_iso_disk_name(
			utils.create_custom_iso_disk_name(
				model.custom_iso_volume_id,
				model.custom_iso_release_name))
		model.set_custom_iso_md5_filename(
			utils.create_custom_iso_md5_filename(
				model.custom_iso_filename))
		model.set_custom_iso_md5_filepath(
			os.path.join(
				model.custom_iso_dir,
				model.custom_iso_md5_filename))

		# Status
		model.set_is_success_copy_original_iso_files(False)
		model.set_is_success_extract_squashfs(False)

	# [2] Prepare and display the new page.

	# Display original values.
	ui.update_entry(
		'new_project_page__original_iso_filename_entry',
		model.original_iso_filename)
	ui.update_entry(
		'new_project_page__original_iso_directory_entry',
		model.original_iso_directory)
	ui.update_entry(
		'new_project_page__original_iso_volume_id_entry',
		model.original_iso_volume_id)
	ui.update_entry(
		'new_project_page__original_iso_release_name_entry',
		model.original_iso_release_name)
	ui.update_entry(
		'new_project_page__original_iso_disk_name_entry',
		model.original_iso_disk_name)

	# Display custom values.
	ui.update_entry(
		'new_project_page__custom_iso_version_number_entry',
		model.custom_iso_version_number)
	ui.update_entry(
		'new_project_page__custom_iso_filename_entry',
		model.custom_iso_filename)
	ui.update_entry(
		'new_project_page__custom_iso_dir_entry',
		model.custom_iso_dir)
	ui.update_entry(
		'new_project_page__custom_iso_volume_id_entry',
		model.custom_iso_volume_id)
	ui.update_entry(
		'new_project_page__custom_iso_release_name_entry',
		model.custom_iso_release_name)
	ui.update_entry(
		'new_project_page__custom_iso_disk_name_entry',
		model.custom_iso_disk_name)

	# [3] Perform functions on the new page, and activate the new page.

	validate.new_project_page()

	ui.set_visible(
		'new_project_page__original_iso_filepath_filechooser',
		False)

	ui.set_sensitive('window', True)

	model.set_propagate(True)


def from__original_iso_filepath_filechooser__to__existing_project_page(thread):
	model.set_propagate(False)
	dialog = vars.app.builder.get_object(
		'existing_project_page__original_iso_filepath_filechooser')
	# Original
	vars.prj.path.o_iso=dialog.get_filename()
	model.set_original_iso_filename(
		re.sub(r'.*/(.*$)',
			   r'\1',
			   vars.prj.path.o_iso))
	model.set_original_iso_directory(dialog.get_current_folder())
	if not utils.is_mounted(vars.prj.path.o_iso,vars.prj.path.o_iso_m):
		utils.mount_iso_image(vars.prj.path.o_iso,vars.prj.path.o_iso_m,thread)
	else:
		dbg('The original ISO is already mounted at',vars.prj.path.o_iso_m)
	if utils.is_mounted(vars.prj.path.o_iso,vars.prj.path.o_iso_m):
		# Original
		model.set_original_iso_volume_id(
			utils.get_iso_image_volume_id(vars.prj.path.o_iso,thread))
		model.set_original_iso_release_name(
			utils.get_iso_image_release_name(
				vars.prj.path.o_iso_m,
				thread))
		model.set_original_iso_disk_name(
			utils.get_iso_image_disk_name(
				vars.prj.path.o_iso_m,
				thread))
		model.set_casper_relative_directory(
			utils.get_casper_relative_directory(
				vars.prj.path.o_iso_m))

		# Custom
		configuration = configparser.ConfigParser()
		configuration.optionxform = str
		configuration.read(vars.prj.path.cfg)
		

		model.set_custom_iso_version_number(
			configuration.get('Custom','iso_version_number'))
		model.set_custom_iso_filename(
			configuration.get('Custom','iso_filename'))
		model.set_custom_iso_dir(
			configuration.get('Custom','iso_dir'))
		model.set_custom_iso_filepath(
			os.path.join(
				model.custom_iso_dir,
				model.custom_iso_filename))
		model.set_custom_iso_volume_id(
			configuration.get('Custom','iso_volume_id'))
		model.set_custom_iso_release_name(
			configuration.get('Custom','iso_release_name'))
		model.set_custom_iso_disk_name(
			configuration.get('Custom','iso_disk_name'))
		model.set_custom_iso_md5_filename(
			configuration.get('Custom','iso_md5_filename'))
		model.set_custom_iso_md5_filepath(
			os.path.join(
				model.custom_iso_dir,
				model.custom_iso_md5_filename))

		# Status
		stored_original_iso_filepath = os.path.join(
			configuration.get('Original','iso_directory'),
			configuration.get('Original','iso_filename'))
		if stored_original_iso_filepath != vars.prj.path.o_iso:
			# Original iso files were not successfully coppied.
			model.set_is_success_copy_original_iso_files(False)
		else:
			model.set_is_success_copy_original_iso_files(
				configuration.getboolean(
					'Status',
					'is_success_copy_original_iso_files',
					fallback=True))

	# [2] Prepare and display the new page.

	# Display original values.
	ui.update_entry(
		'existing_project_page__original_iso_filename_entry',
		model.original_iso_filename)
	ui.update_entry(
		'existing_project_page__original_iso_directory_entry',
		model.original_iso_directory)
	ui.update_entry(
		'existing_project_page__original_iso_volume_id_entry',
		model.original_iso_volume_id)
	ui.update_entry(
		'existing_project_page__original_iso_release_name_entry',
		model.original_iso_release_name)
	ui.update_entry(
		'existing_project_page__original_iso_disk_name_entry',
		model.original_iso_disk_name)

	# Display custom values.
	ui.update_entry(
		'existing_project_page__custom_iso_version_number_entry',
		model.custom_iso_version_number)
	ui.update_entry(
		'existing_project_page__custom_iso_filename_entry',
		model.custom_iso_filename)
	ui.update_entry(
		'existing_project_page__custom_iso_dir_entry',
		model.custom_iso_dir)
	ui.update_entry(
		'existing_project_page__custom_iso_volume_id_entry',
		model.custom_iso_volume_id)
	ui.update_entry(
		'existing_project_page__custom_iso_release_name_entry',
		model.custom_iso_release_name)
	ui.update_entry(
		'existing_project_page__custom_iso_disk_name_entry',
		model.custom_iso_disk_name)

	# [3] Perform functions on the new page, and activate the new page.

	validate.existing_project_page()

	ui.set_visible(
		'existing_project_page__original_iso_filepath_filechooser',
		False)

	ui.set_sensitive('window', True)

	model.set_propagate(True)


def from__custom_iso_dir_filechooser__to__new_project_page(
		thread):

	dialog = vars.app.builder.get_object(
		'new_project_page__custom_iso_dir_filechooser')
	custom_iso_dir = dialog.get_filename()
	# custom_iso_dir = dialog.get_current_folder()
	ui.update_entry(
		'new_project_page__custom_iso_dir_entry',
		custom_iso_dir)

	ui.set_visible(
		'new_project_page__custom_iso_dir_filechooser',
		False)

	ui.set_sensitive('window', True)


def from__custom_iso_dir_filechooser__to__existing_project_page(
		thread):

	dialog = vars.app.builder.get_object(
		'existing_project_page__custom_iso_dir_filechooser')
	custom_iso_dir = dialog.get_filename()
	# custom_iso_dir = dialog.get_current_folder()
	ui.update_entry(
		'existing_project_page__custom_iso_dir_entry',
		custom_iso_dir)

	ui.set_visible(
		'existing_project_page__custom_iso_dir_filechooser',
		False)

	ui.set_sensitive('window', True)


#
# Project Directory Page
#


def from__project_directory_page__to__new_project_page(thread):
	model.set_propagate(False)
	ui.show_spinner()
	ui.reset_buttons()

	# Original
	model.set_original_iso_filename('')
	model.set_original_iso_directory('')
	model.set_original_iso_volume_id('')
	model.set_original_iso_release_name('')
	model.set_original_iso_disk_name('')

	# Custom
	model.set_custom_iso_version_number('')
	model.set_custom_iso_filename('')
	model.set_custom_iso_dir('')
	model.set_custom_iso_filepath('')
	model.set_custom_iso_volume_id('')
	model.set_custom_iso_release_name('')
	model.set_custom_iso_disk_name('')
	model.set_custom_iso_md5_filename('')
	model.set_custom_iso_md5_filepath('')

	# Status
	model.set_is_success_copy_original_iso_files(False)
	model.set_is_success_extract_squashfs(False)

	# Display original values.
	ui.update_entry(
		'new_project_page__original_iso_filename_entry',
		model.original_iso_filename)
	ui.update_entry(
		'new_project_page__original_iso_directory_entry',
		model.original_iso_directory)
	ui.update_entry(
		'new_project_page__original_iso_volume_id_entry',
		model.original_iso_volume_id)
	ui.update_entry(
		'new_project_page__original_iso_release_name_entry',
		model.original_iso_release_name)
	ui.update_entry(
		'new_project_page__original_iso_disk_name_entry',
		model.original_iso_disk_name)

	# Display custom values.
	ui.update_entry(
		'new_project_page__custom_iso_version_number_entry',
		model.custom_iso_version_number)
	ui.update_entry(
		'new_project_page__custom_iso_filename_entry',
		model.custom_iso_filename)
	ui.update_entry(
		'new_project_page__custom_iso_dir_entry',
		model.custom_iso_dir)
	ui.update_entry(
		'new_project_page__custom_iso_volume_id_entry',
		model.custom_iso_volume_id)
	ui.update_entry(
		'new_project_page__custom_iso_release_name_entry',
		model.custom_iso_release_name)
	ui.update_entry(
		'new_project_page__custom_iso_disk_name_entry',
		model.custom_iso_disk_name)

	# Activate radio button 2 as the default option (continue customizing the
	# existing project) when the existing project page is displyed.
	# The handler on_toggled__existing_project_page__radiobutton() is called
	# whenever the radiobutton is toggled; however the function will not
	# execute, because model.propagate is False.
	ui.activate_radiobutton('existing_project_page__radiobutton_2', True)

	# Transition to the New Project page.
	ui.show_page('project_directory_page', 'new_project_page')
	ui.reset_buttons(True, True, False)

	# [3] Perform functions on the new page, and activate the new page.

	validate.new_project_page()

	ui.hide_spinner()

	model.set_propagate(True)


def from__project_directory_page__to__existing_project_page(thread):
	model.set_propagate(False)
	ui.show_spinner()
	ui.reset_buttons()
	# Create the configuration.
	configuration = configparser.ConfigParser()
	configuration.optionxform = str
	configuration.read(vars.prj.path.cfg)
	# Original
	model.set_original_iso_filename(
		configuration.get('Original','iso_filename'))
	model.set_original_iso_directory(
		configuration.get('Original','iso_directory'))
	vars.prj.path.o_iso=os.path.join(
			model.original_iso_directory,
			model.original_iso_filename)
	is_mounted = utils.is_mounted(vars.prj.path.o_iso,vars.prj.path.o_iso_m)
	if not is_mounted:
		utils.mount_iso_image(vars.prj.path.o_iso,vars.prj.path.o_iso_m,thread)
	model.set_original_iso_volume_id(configuration.get('Original','iso_volume_id'))
	model.set_original_iso_release_name(configuration.get('Original','iso_release_name'))
	model.set_original_iso_disk_name(configuration.get('Original','iso_disk_name'))
	model.set_casper_relative_directory(utils.get_casper_relative_directory(vars.prj.path.o_iso_m))
	# Custom
	vars.prj.use_copied=configuration.getboolean('Custom','use_copied',fallback=vars.prj.use_copied)
	dbg('vars.prj.use_copied',vars.prj.use_copied)
	model.set_custom_iso_version_number(
		configuration.get('Custom','iso_version_number'))
	model.set_custom_iso_filename(
		configuration.get('Custom','iso_filename'))
	model.set_custom_iso_dir(
		configuration.get('Custom','iso_dir'))
	model.set_custom_iso_filepath(
		os.path.join(
			model.custom_iso_dir,
			model.custom_iso_filename))
	model.set_custom_iso_volume_id(
		configuration.get('Custom','iso_volume_id'))
	model.set_custom_iso_release_name(
		configuration.get('Custom','iso_release_name'))
	model.set_custom_iso_disk_name(
		configuration.get('Custom','iso_disk_name'))
	model.set_custom_iso_md5_filename(
		configuration.get('Custom','iso_md5_filename'))
	model.set_custom_iso_md5_filepath(
		os.path.join(
			model.custom_iso_dir,
			model.custom_iso_md5_filename))
	# Status
	model.set_is_success_copy_original_iso_files(
		configuration.getboolean(
			'Status',
			'is_success_copy_original_iso_files',
			fallback=True))
	model.set_is_success_extract_squashfs(
		configuration.getboolean(
			'Status',
			'is_success_extract_squashfs',
			fallback=True))
	# Options
	# BootConfigs
	if configuration.has_section("BootConfigs" ):
		bootconfigs = configuration.items( "BootConfigs" )
		for key, relpath in bootconfigs:
			fpath = os.path.join(vars.prj.path.c_iso,relpath)
			if os.path.isfile(fpath):
				vars.prj.bootconfigs.append(fpath)
				dbg('BootConfigs added',relpath)
			else:
				dbg('BootConfigs ignored missing',relpath,nd=2)
	if len(vars.prj.bootconfigs)<1:
		vars.prj.bootconfigs=vars.prj.bootconfigs_factory
		dbg('BootConfigs is bad, using factory values',vars.prj.bootconfigs,nd=2)
	# end BootConfigs

# Display original values.
	ui.update_entry(
		'existing_project_page__original_iso_filename_entry',
		model.original_iso_filename)
	ui.update_entry(
		'existing_project_page__original_iso_directory_entry',
		model.original_iso_directory)
	ui.update_entry(
		'existing_project_page__original_iso_volume_id_entry',
		model.original_iso_volume_id)
	ui.update_entry(
		'existing_project_page__original_iso_release_name_entry',
		model.original_iso_release_name)
	ui.update_entry(
		'existing_project_page__original_iso_disk_name_entry',
		model.original_iso_disk_name)
# Display custom values.
	ui.update_entry(
		'existing_project_page__custom_iso_version_number_entry',
		model.custom_iso_version_number)
	ui.update_entry(
		'existing_project_page__custom_iso_filename_entry',
		model.custom_iso_filename)
	ui.update_entry(
		'existing_project_page__custom_iso_dir_entry',
		model.custom_iso_dir)
	ui.update_entry(
		'existing_project_page__custom_iso_volume_id_entry',
		model.custom_iso_volume_id)
	ui.update_entry(
		'existing_project_page__custom_iso_release_name_entry',
		model.custom_iso_release_name)
	ui.update_entry(
		'existing_project_page__custom_iso_disk_name_entry',
		model.custom_iso_disk_name)
	ui.activate_radiobutton('existing_project_page__radiobutton_1', True)
	ui.show_page('project_directory_page', 'existing_project_page')
	ui.reset_buttons(True, True, False)
	validate.existing_project_page()
	ui.hide_spinner()
	model.set_propagate(True)


#
# Shared Functions:
# - New Project Page
# - Existing Project Page
# - Unsquashfs Page
# - Terminal Page
#


def _from__project_page__to__unsquashfs_page(
		old_page_name,
		thread):

	# TODO: These functions do not exist. Revise comment.
	# This is the same as from__new_project_page__to__unsquashfs_page(thread)
	# This is the same as from__existing_project_page__to__unsquashfs_page(thread)

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons()

	# Save the configuration (since it may have changed).
	# Changes are are not persisted on the previous page, so the configuration
	# may be saved after the user clicks Next on the previous page.
	utils.save_configuration()

	# [2] Prepare and display the new page.

	# ui.hide_spinner()

	# Setup the Copy Original ISO Files section.
	ui.update_label(
		'unsquashfs_page__copy_original_iso_files_label',
		'Copying ISO files from the original disk image:')
	ui.update_label(
		'unsquashfs_page__copy_original_iso_files_original_disk_image_label',
		'%s' % vars.prj.path.o_iso)
	ui.update_progressbar_percent(
		'unsquashfs_page__copy_original_iso_files_progressbar',
		0)
	ui.update_progressbar_text(
		'unsquashfs_page__copy_original_iso_files_progressbar',
		'')
	ui.set_label_error(
		'unsquashfs_page__copy_original_iso_files_result_label',
		False)
	ui.update_label(
		'unsquashfs_page__copy_original_iso_files_result_label',
		'')
	ui.update_status(
		'unsquashfs_page__copy_original_iso_files',
		ui.BULLET)
	ui.set_visible(
		'unsquashfs_page__copy_original_iso_files_section',
		True)

	# Setup the Unsquashfs section.
	ui.update_label(
		'unsquashfs_page__unsquashfs_original_disk_image_label',
		'%s' % vars.prj.path.o_iso)
	ui.update_progressbar_percent(
		'unsquashfs_page__unsquashfs_progressbar',
		0)
	ui.update_progressbar_text(
		'unsquashfs_page__unsquashfs_progressbar',
		'')
	ui.set_label_error('unsquashfs_page__unsquashfs_result_label', False)
	ui.update_label('unsquashfs_page__unsquashfs_result_label', '')
	ui.update_status('unsquashfs_page__unsquashfs', ui.BULLET)
	ui.set_visible('unsquashfs_page__unsquashfs_section', True)

	# Transition to the Unsquashfs page.
	ui.show_page(old_page_name, 'unsquashfs_page')
	ui.reset_buttons(True, True, False)

	# [3] Perform functions on the new page, and activate the new page.

	ui.hide_spinner()

	# Perform actions in the Copy Original ISO Files section.

	ui.update_status(
		'unsquashfs_page__copy_original_iso_files',
		ui.PROCESSING)
	time.sleep(0.25)

	if model.is_success_copy_original_iso_files:
		ui.update_progressbar_percent(
			'unsquashfs_page__copy_original_iso_files_progressbar',
			100)
		ui.set_label_error(
			'unsquashfs_page__copy_original_iso_files_result_label',
			False)
		ui.update_label(
			'unsquashfs_page__copy_original_iso_files_result_label',
			'Using previously copied original ISO files.')
		ui.update_status(
			'unsquashfs_page__copy_original_iso_files',
			ui.OK)
	else:
		# Copy original ISO files.
		is_success = utils.copy_original_iso_files(thread)
		model.set_is_success_copy_original_iso_files(is_success)

		if model.is_success_copy_original_iso_files:
			ui.set_label_error(
				'unsquashfs_page__copy_original_iso_files_result_label',
				False)
			ui.update_label(
				'unsquashfs_page__copy_original_iso_files_result_label',
				'All original ISO files have been copied.')
			ui.update_status(
				'unsquashfs_page__copy_original_iso_files',
				ui.OK)
		else:
			ui.set_label_error(
				'unsquashfs_page__copy_original_iso_files_result_label',
				True)
			ui.update_label(
				'unsquashfs_page__copy_original_iso_files_result_label',
				'Unable to copy original ISO files.')
			ui.update_status(
				'unsquashfs_page__copy_original_iso_files',
				ui.ERROR)

	time.sleep(0.50)

	# Perform actions in the Unsquashfs section.

	ui.update_status('unsquashfs_page__unsquashfs', ui.PROCESSING)
	time.sleep(0.25)

	if model.is_success_extract_squashfs:
		ui.update_progressbar_percent(
			'unsquashfs_page__unsquashfs_progressbar',
			100)
		ui.set_label_error(
			'unsquashfs_page__unsquashfs_result_label',
			False)
		ui.update_label(
			'unsquashfs_page__unsquashfs_result_label',
			'Using previously extracted compressed Linux file system.')
		ui.update_status('unsquashfs_page__unsquashfs', ui.OK)
	else:
		# Clear the terminal because the history will no longer be valid.
		terminal = vars.app.builder.get_object('terminal_page__terminal')
		terminal.reset(True, True)

		# Extract filesystem.squashfs.
		is_success = utils.unsquashfs(thread)
		model.set_is_success_extract_squashfs(is_success)

		if model.is_success_extract_squashfs:
			# Display the Unsquashfs section.
			ui.set_label_error(
				'unsquashfs_page__unsquashfs_result_label',
				False)
			ui.update_label(
				'unsquashfs_page__unsquashfs_result_label',
				'The compressed Linux file system has been extracted.')
			ui.update_status('unsquashfs_page__unsquashfs', ui.OK)
		else:
			ui.set_label_error(
				'unsquashfs_page__unsquashfs_result_label',
				True)
			ui.update_label(
				'unsquashfs_page__unsquashfs_result_label',
				'Unable to extract the compressed Linux file system.')
			ui.update_status('unsquashfs_page__unsquashfs', ui.ERROR)

	time.sleep(1.00)

	# Save the configuration (since it may have changed).
	# Since this page persists changes (such as copying original iso files or
	# extracting squashfs) the configuration must be persisted on this page as
	# well.
	utils.save_configuration()

	is_page_complete = (
		model.is_success_copy_original_iso_files
		and model.is_success_extract_squashfs)

	# ui.reset_buttons(True, True, is_page_complete)

	return is_page_complete


def to__options_page(aprev_page, athread):
	ui.show_spinner()
	ui.reset_buttons()
	installed_packages_list = utils.create_installed_packages_list(athread)
	if aprev_page == 'terminal_page':
		ui.set_label_error('terminal_page__exit_terminal_label', False)
		ui.update_label(
			'terminal_page__exit_terminal_label',
			'Exiting from the chroot environment')
		ui.set_visible('terminal_page__exit_terminal_label', True)
		if model.handler_id:
			terminal = vars.app.builder.get_object('terminal_page__terminal')
			terminal.disconnect(model.handler_id)
			model.set_handler_id(None)
		utils.exit_chroot_environment(athread)
		is_chroot = utils.check_chroot(athread)
		if not is_chroot:
			ui.set_sensitive('terminal_page__terminal', False)
			ui.set_label_error(
				'terminal_page__exit_terminal_label',
				False)
			ui.update_label(
				'terminal_page__exit_terminal_label',
				'Successfully exited from the chroot environment.')
			ui.set_visible('terminal_page__exit_terminal_label', True)
		else:
			ui.set_sensitive('terminal_page__terminal', True)
			ui.set_label_error('terminal_page__exit_terminal_label', True)
			ui.update_label(
				'terminal_page__exit_terminal_label',
				'Unable to exit from the chroot environment.')
			ui.set_visible('terminal_page__exit_terminal_label', True)
		time.sleep(1.00)
	# Manifest tab
	utils.create_filesystem_manifest_file(installed_packages_list)
	package_count = len(installed_packages_list)
	filename = 'filesystem.manifest-remove'
	is_exists = utils.is_exists_filesystem_manifest_remove(filename)
	removable_packages_list_1 = utils.get_removable_packages_list(
		filename) if is_exists else []
	# Create list of removable packages for a minimal install.
	filename = 'filesystem.manifest-minimal-remove'
	is_exists = utils.is_exists_filesystem_manifest_remove(filename)
	removable_packages_list_2 = utils.get_removable_packages_list(filename) if is_exists else []
	package_details_list = utils.create_package_details_list(
		installed_packages_list,
		removable_packages_list_1,
		removable_packages_list_2)

	if removable_packages_list_2:
		ui.set_column_visible(
			'options_page__package_manifest_tab__remove_2_treeviewcolumn',
			True)
	else:
		ui.set_column_visible(
			'options_page__package_manifest_tab__remove_2_treeviewcolumn',
			False)

	ui.update_liststore(
		'options_page__package_manifest_tab__liststore',
		package_details_list)

	model.undo_index = 0
	model.undo_list = []
	ui.set_sensitive(
		'options_page__package_manifest_tab__revert_button',
		False)
	ui.set_sensitive(
		'options_page__package_manifest_tab__undo_button',
		False)
	ui.set_sensitive(
		'options_page__package_manifest_tab__redo_button',
		False)

	#Linux kernels tab
	directory_1 = os.path.join(vars.prj.path.c_squash, 'boot')
	directory_2 = os.path.join(
		vars.prj.path.o_iso_m,
		model.casper_relative_directory)
	kernel_details_list = utils.create_kernel_details_list(
		directory_1,
		directory_2)
	import gi
	gi.require_version('Gtk', '3.0')
	from gi.repository import Gtk
	column = vars.app.builder.get_object(
		'options_page__linux_kernels_tab__treeviewcolumn_2')
	area = column.get_area()
	area.set_orientation(Gtk.Orientation.VERTICAL)
	ui.update_liststore(
		'options_page__linux_kernels_tab__liststore',
		kernel_details_list)
	# Preseed tab
	model.delete_list = []
	search_filepath = os.path.join(
		vars.prj.path.c_iso,
		'preseed',
		'*')
	filepaths = glob.glob(search_filepath)
	filepaths.sort()
	ui.tab_update('page_options__tab_preseed', filepaths)
	if filepaths:
		ui.set_sensitive(
			'page_options__tab_preseed_sidebar__create_button',
			True)
		ui.set_sensitive(
			'page_options__tab_preseed_sidebar__delete_button',
			True)
		ui.set_visible('page_options__tab_preseed', True)
		ui.set_visible('options_page__preseed_tab__create_grid', False)
		ui.set_visible('options_page__preseed_tab__delete_grid', False)
	else:
		ui.set_sensitive(
			'page_options__tab_preseed_sidebar__create_button',
			False)
		ui.set_sensitive(
			'page_options__tab_preseed_sidebar__delete_button',
			False)
		ui.set_visible('page_options__tab_preseed', False)
		ui.update_entry(
			'options_page__preseed_tab__create_grid__entry',
			'')
		ui.update_label(
			'options_page__preseed_tab__create_grid__error_label',
			'')
		ui.set_visible('options_page__preseed_tab__create_grid', True)
		ui.set_visible('options_page__preseed_tab__delete_grid', False)

	# Get the selected kernel.
	for selected_index, kernel_details in enumerate(kernel_details_list):
		if kernel_details[7]: break
	else: selected_index = 0
	dbg('The selected kernel is index number', selected_index)
	search_text_1 = r'/vmlinuz\S*'
	replacement_text_1 = '/%s' % kernel_details_list[selected_index][2]
	search_text_2 = r'/initrd\S*'
	replacement_text_2 = '/%s' % kernel_details_list[selected_index][4]
	
	#Boot configuration tab
	filepaths = []
	for bootcfg in vars.prj.bootconfigs:
		filepath = os.path.join(
			vars.prj.path.c_iso,
			bootcfg)
		filepaths.append(filepath)
	tabname='page_options__tab_boot_config'
	dbg('Updating',tabname,'with',filepaths)
	ui.tab_update(
		tabname,
		filepaths,
		(search_text_1,
		 replacement_text_1),
		(search_text_2,
		 replacement_text_2))

	# Transition to the Manage Options page.
	ui.show_page(aprev_page, 'options_page')
	ui.reset_buttons(True, True, True, next_button_label='Generate')
	ui.hide_spinner()


#
# New Project Page
#


def from__new_project_page__to__terminal_page(athread):

	if _from__project_page__to__unsquashfs_page('new_project_page',athread):
		_from__unsquashfs_page__to__terminal_page(athread)


#
# Existing Project Page
#


def from__existing_project_page__to__existing_project_page(thread):

	if vars.app.builder.get_object(
			'existing_project_page__radiobutton_1').get_active():
		_from__existing_project_page__to__existing_project_page__radiobutton_1(
			thread)
	elif vars.app.builder.get_object(
			'existing_project_page__radiobutton_2').get_active():
		_from__existing_project_page__to__existing_project_page__radiobutton_2(
			thread)
	elif vars.app.builder.get_object(
			'existing_project_page__radiobutton_3').get_active():
		_from__existing_project_page__to__existing_project_page__radiobutton_3(
			thread)


def _from__existing_project_page__to__existing_project_page__radiobutton_1(
		thread):

	ui.set_sensitive(
		'existing_project_page__original_iso_filepath_filechooser__open_button',
		True)
	ui.reset_buttons(True, True, False)
	validate.existing_project_page()


def _from__existing_project_page__to__existing_project_page__radiobutton_2(
		thread):

	ui.set_sensitive(
		'existing_project_page__original_iso_filepath_filechooser__open_button',
		True)
	ui.reset_buttons(True, True, False)
	validate.existing_project_page()


def _from__existing_project_page__to__existing_project_page__radiobutton_3(thread):
	model.set_propagate(False)
	ui.show_spinner()
	ui.reset_buttons()

	configuration = configparser.ConfigParser()
	configuration.optionxform = str
	configuration.read(vars.prj.path.cfg)

	# Original
	model.set_original_iso_filename(
		configuration.get('Original','iso_filename'))
	model.set_original_iso_directory(
		configuration.get('Original','iso_directory'))
	vars.prj.path.o_iso=os.path.join(
			model.original_iso_directory,
			model.original_iso_filename)
	if not utils.is_mounted(vars.prj.path.o_iso,vars.prj.path.o_iso_m):
		utils.mount_iso_image(vars.prj.path.o_iso,vars.prj.path.o_iso_m,thread)
	else:
		dbg('The original ISO is already mounted at',vars.prj.path.o_iso_m)
	if utils.is_mounted(vars.prj.path.o_iso,vars.prj.path.o_iso_m):
		# Original
		model.set_original_iso_volume_id(
			configuration.get('Original','iso_volume_id'))
		model.set_original_iso_release_name(
			configuration.get('Original','iso_release_name'))
		model.set_original_iso_disk_name(
			configuration.get('Original','iso_disk_name'))
		model.set_casper_relative_directory(
			utils.get_casper_relative_directory(
				vars.prj.path.o_iso_m))
	# Custom
	model.set_custom_iso_version_number(
		configuration.get('Custom','iso_version_number'))
	model.set_custom_iso_filename(
		configuration.get('Custom','iso_filename'))
	model.set_custom_iso_dir(
		configuration.get('Custom','iso_dir'))
	model.set_custom_iso_filepath(
		os.path.join(
			model.custom_iso_dir,
			model.custom_iso_filename))
	model.set_custom_iso_volume_id(
		configuration.get('Custom','iso_volume_id'))
	model.set_custom_iso_release_name(
		configuration.get('Custom','iso_release_name'))
	model.set_custom_iso_disk_name(
		configuration.get('Custom','iso_disk_name'))
	model.set_custom_iso_md5_filename(
		configuration.get('Custom','iso_md5_filename'))
	model.set_custom_iso_md5_filepath(
		os.path.join(
			model.custom_iso_dir,
			model.custom_iso_md5_filename))

	# Status
	model.set_is_success_copy_original_iso_files(
		configuration.getboolean(
			'Status',
			'is_success_copy_original_iso_files',
			fallback=True))

	# [2] Prepare and display the new page.

	# Display original values.
	ui.update_entry(
		'existing_project_page__original_iso_filename_entry',
		model.original_iso_filename)
	ui.update_entry(
		'existing_project_page__original_iso_directory_entry',
		model.original_iso_directory)
	ui.update_entry(
		'existing_project_page__original_iso_volume_id_entry',
		model.original_iso_volume_id)
	ui.update_entry(
		'existing_project_page__original_iso_release_name_entry',
		model.original_iso_release_name)
	ui.update_entry(
		'existing_project_page__original_iso_disk_name_entry',
		model.original_iso_disk_name)

	ui.set_sensitive(
		'existing_project_page__original_iso_filepath_filechooser__open_button',
		False)

	# Display custom values.
	ui.update_entry(
		'existing_project_page__custom_iso_version_number_entry',
		model.custom_iso_version_number)
	ui.update_entry(
		'existing_project_page__custom_iso_filename_entry',
		model.custom_iso_filename)
	ui.update_entry(
		'existing_project_page__custom_iso_dir_entry',
		model.custom_iso_dir)
	ui.update_entry(
		'existing_project_page__custom_iso_volume_id_entry',
		model.custom_iso_volume_id)
	ui.update_entry(
		'existing_project_page__custom_iso_release_name_entry',
		model.custom_iso_release_name)
	ui.update_entry(
		'existing_project_page__custom_iso_disk_name_entry',
		model.custom_iso_disk_name)

	# ui.set_sensitive('existing_project_page__custom_iso_dir_filechooser__open_button', False)

	# Transition to the Existing Project page.
	# ui.show_page('project_directory_page', 'existing_project_page')
	ui.reset_buttons(True, True, False)

	# [3] Perform functions on the new page, and activate the new page.

	validate.existing_project_page_for_delete()

	ui.hide_spinner()

	model.set_propagate(True)


def from__existing_project_page__to__options_page(thread):

	if _from__project_page__to__unsquashfs_page(
			'existing_project_page',
			thread):
		_from__unsquashfs_page__to__options_page(thread)


def from__existing_project_page__to__terminal_page(thread):

	if _from__project_page__to__unsquashfs_page(
			'existing_project_page',
			thread):
		_from__unsquashfs_page__to__terminal_page(thread)


def from__existing_project_page__to__delete_project_page(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons()

	# [2] Prepare and display the new page.

	if os.path.exists(vars.prj.path.cfg):
		ui.update_status(
			'delete_project_page__configuration_file',
			ui.BULLET)
		ui.set_visible(
			'delete_project_page__configuration_file_section',
			True)
	else:
		ui.set_visible(
			'delete_project_page__configuration_file_section',
			False)

	if os.path.exists(vars.prj.path.o_iso_m):
		ui.update_status(
			'delete_project_page__original_iso_mount_point',
			ui.BULLET)
		ui.set_visible(
			'delete_project_page__original_iso_mount_point_section',
			True)
	else:
		ui.set_visible(
			'delete_project_page__custom_squashfs_directory_section',
			False)

	if os.path.exists(vars.prj.path.c_squash):
		ui.update_status(
			'delete_project_page__custom_squashfs_directory',
			ui.BULLET)
		ui.set_visible(
			'delete_project_page__custom_squashfs_directory_section',
			True)
	else:
		ui.set_visible(
			'delete_project_page__custom_squashfs_directory_section',
			False)

	if os.path.exists(vars.prj.path.c_iso):
		ui.update_status(
			'delete_project_page__custom_live_iso_directory',
			ui.BULLET)
		ui.set_visible(
			'delete_project_page__custom_live_iso_directory_section',
			True)
	else:
		ui.set_visible(
			'delete_project_page__custom_live_iso_directory_section',
			False)

	# Only delete the custom ISO image if it is directly under the project directory.
	filepath = os.path.join(
		vars.prj.path.root,
		model.custom_iso_filename)
	if os.path.exists(filepath):
		ui.update_status(
			'delete_project_page__custom_iso_filename',
			ui.BULLET)
		ui.update_label(
			'delete_project_page__custom_iso_filename_filepath',
			filepath)
		ui.set_visible(
			'delete_project_page__custom_iso_filename_section',
			True)
	else:
		ui.set_visible(
			'delete_project_page__custom_iso_filename_section',
			False)

	# Only delete the custom ISO image MD5 file if it is directly under the project directory.
	filepath = os.path.join(
		vars.prj.path.root,
		model.custom_iso_md5_filename)
	if os.path.exists(filepath):
		ui.update_status(
			'delete_project_page__custom_iso_md5_filename',
			ui.BULLET)
		ui.update_label(
			'delete_project_page__custom_iso_md5_filename_filepath',
			filepath)
		ui.set_visible(
			'delete_project_page__custom_iso_md5_filename_section',
			True)
	else:
		ui.set_visible(
			'delete_project_page__custom_iso_md5_filename_section',
			False)

	# Transition to the Delete Project page.
	ui.show_page('existing_project_page', 'delete_project_page')
	ui.reset_buttons(
		True,
		True,
		True,
		back_button_label='Cancel',
		next_button_label='Delete')

	# [3] Perform functions on the new page, and activate the new page.
	ui.hide_spinner()


#
# Delete Project Page
#


def from__delete_project_page__to__new_project_page(thread):
	model.set_propagate(False)
	ui.reset_buttons(True, False, False)
	if os.path.exists(vars.prj.path.cfg):
		ui.update_status(
			'delete_project_page__configuration_file',
			ui.PROCESSING)
		time.sleep(0.25)
		dbg(
			'About to delete configuration file',
			vars.prj.path.cfg)
		utils.delete_file(vars.prj.path.cfg, thread)
		ui.update_status(
			'delete_project_page__configuration_file',
			ui.OK)
		time.sleep(0.25)

	else:

		ui.update_status(
			'delete_project_page__configuration_file',
			ui.BULLET)
		ui.set_visible(
			'delete_project_page__configuration_file_section',
			False)

	if os.path.exists(vars.prj.path.o_iso_m):

		ui.update_status(
			'delete_project_page__original_iso_mount_point',
			ui.PROCESSING)
		time.sleep(0.25)

		# Unmount the original ISO image and delete the mount point.
		dbg(
			'About to delete original ISO image mount point',
			vars.prj.path.o_iso_m)
		utils.delete_iso_mount(
			vars.prj.path.o_iso_m,
			thread)
		ui.update_status(
			'delete_project_page__original_iso_mount_point',
			ui.OK)
		time.sleep(0.25)

	else:

		ui.update_status(
			'delete_project_page__original_iso_mount_point',
			ui.BULLET)
		ui.set_visible(
			'delete_project_page__original_iso_mount_point_section',
			False)

	if os.path.exists(vars.prj.path.c_squash):

		ui.update_status(
			'delete_project_page__custom_squashfs_directory',
			ui.PROCESSING)
		time.sleep(0.25)

		# Delete directory, squashfs-root.
		dbg(
			'About to delete custom squashfs directory',
			vars.prj.path.c_squash)
		utils.delete_file(vars.prj.path.c_squash, thread)
		ui.update_status(
			'delete_project_page__custom_squashfs_directory',
			ui.OK)
		time.sleep(0.25)

	else:

		ui.update_status(
			'delete_project_page__custom_squashfs_directory',
			ui.BULLET)
		ui.set_visible(
			'delete_project_page__original_iso_mount_point_section',
			False)

	if os.path.exists(vars.prj.path.c_iso):
		ui.update_status(
			'delete_project_page__custom_live_iso_directory',
			ui.PROCESSING)
		time.sleep(0.25)

		# Delete directory, custom-live-iso.
		dbg(
			'About to delete custom live ISO directory',
			vars.prj.path.c_iso)
		utils.delete_file(vars.prj.path.c_iso, thread)
		ui.update_status(
			'delete_project_page__custom_live_iso_directory',
			ui.OK)
		time.sleep(0.25)

	else:

		ui.update_status(
			'delete_project_page__custom_live_iso_directory',
			ui.BULLET)
		ui.set_visible(
			'delete_project_page__custom_live_iso_directory_section',
			False)

	# Only delete the custom ISO image if it is directly under the project directory.
	filepath = os.path.join(
		vars.prj.path.root,
		model.custom_iso_filename)
	if os.path.exists(filepath):

		ui.update_status(
			'delete_project_page__custom_iso_filename',
			ui.PROCESSING)
		time.sleep(0.25)

		# Delete file, *.iso.
		dbg('About to delete custom ISO image file', filepath)
		utils.delete_file(filepath, thread)
		ui.update_status(
			'delete_project_page__custom_iso_filename',
			ui.OK)
		time.sleep(0.25)

	else:

		ui.update_status(
			'delete_project_page__custom_iso_filename',
			ui.BULLET)
		ui.set_visible(
			'delete_project_page__custom_iso_filename_section',
			False)

	# Only delete the MD5 file if it is directly under the project directory.
	filepath = os.path.join(
		vars.prj.path.root,
		model.custom_iso_md5_filename)
	if os.path.exists(filepath):

		ui.update_status(
			'delete_project_page__custom_iso_md5_filename',
			ui.PROCESSING)
		time.sleep(0.25)

		# Delete file, *.md5.
		dbg('About to delete custom ISO image MD5 file', filepath)
		utils.delete_file(filepath, thread)
		ui.update_status(
			'delete_project_page__custom_iso_md5_filename',
			ui.OK)
		time.sleep(0.25)

	else:

		ui.update_status(
			'delete_project_page__custom_iso_md5_filename',
			ui.BULLET)
		ui.set_visible(
			'delete_project_page__custom_iso_md5_filename_section',
			False)

	# Reset all global variables before transitioning to new project page.
	ui.show_spinner()
	ui.reset_buttons()

	# Original
	model.set_original_iso_filename('')
	model.set_original_iso_directory('')
	model.set_original_iso_volume_id('')
	model.set_original_iso_release_name('')
	model.set_original_iso_disk_name('')

	# Custom
	model.set_custom_iso_version_number('')
	model.set_custom_iso_filename('')
	model.set_custom_iso_dir('')
	model.set_custom_iso_filepath('')  # Aggregated value; not displayed.
	model.set_custom_iso_volume_id('')
	model.set_custom_iso_release_name('')
	model.set_custom_iso_disk_name('')
	model.set_custom_iso_md5_filename(
		'')  # Aggregated value; not displayed.
	model.set_custom_iso_md5_filepath(
		'')  # Aggregated value; not displayed.

	# Status
	model.set_is_success_copy_original_iso_files(False)
	model.set_is_success_extract_squashfs(False)

	# [2] Prepare and display the new page.

	# Display original values.
	ui.update_entry(
		'new_project_page__original_iso_filename_entry',
		model.original_iso_filename)
	ui.update_entry(
		'new_project_page__original_iso_directory_entry',
		model.original_iso_directory)
	ui.update_entry(
		'new_project_page__original_iso_volume_id_entry',
		model.original_iso_volume_id)
	ui.update_entry(
		'new_project_page__original_iso_release_name_entry',
		model.original_iso_release_name)
	ui.update_entry(
		'new_project_page__original_iso_disk_name_entry',
		model.original_iso_disk_name)

	# Display custom values.
	ui.update_entry(
		'new_project_page__custom_iso_version_number_entry',
		model.custom_iso_version_number)
	ui.update_entry(
		'new_project_page__custom_iso_filename_entry',
		model.custom_iso_filename)
	ui.update_entry(
		'new_project_page__custom_iso_dir_entry',
		model.custom_iso_dir)
	ui.update_entry(
		'new_project_page__custom_iso_volume_id_entry',
		model.custom_iso_volume_id)
	ui.update_entry(
		'new_project_page__custom_iso_release_name_entry',
		model.custom_iso_release_name)
	ui.update_entry(
		'new_project_page__custom_iso_disk_name_entry',
		model.custom_iso_disk_name)

	# Activate radio button 2 as the default option (continue customizing the
	# existing project) when the existing project page is displayed.
	# The handler on_toggled__existing_project_page__radiobutton() is called
	# whenever the radiobutton is toggled; however the function will not
	# execute, because model.propagate is False.
	ui.activate_radiobutton('existing_project_page__radiobutton_2', True)

	# Transition to the New Project page.
	ui.show_page('delete_project_page', 'new_project_page')
	ui.reset_buttons(True, True, False)

	# [3] Perform functions on the new page, and activate the new page.

	validate.new_project_page()

	ui.hide_spinner()

	model.set_propagate(True)


#
# Unsquashfs Page
#


def _from__unsquashfs_page__to__terminal_page(thread):
	ui.show_spinner()
	ui.reset_buttons()
	utils.save_configuration()
	ui.show_page('unsquashfs_page', 'terminal_page')
	ui.set_label_error('terminal_page__exit_terminal_label', False)
	ui.update_label(
		'terminal_page__exit_terminal_label',
		'Entering the chroot environment')
	ui.set_visible('terminal_page__exit_terminal_label', True)
	utils.prepare_chroot_environment(thread)
	utils.create_chroot_terminal(thread)
	is_chroot = utils.check_chroot(thread)
	time.sleep(1.00)
	if is_chroot:
		# Connect terminal exit handler.
		terminal = vars.app.builder.get_object('terminal_page__terminal')
		handler_id = terminal.connect(
			'child-exited',
			handlers.on_child_exited__terminal_page)
		model.set_handler_id(handler_id)
		# Enable input to the terminal.
		ui.set_sensitive('terminal_page__terminal', True)
		ui.set_label_error('terminal_page__exit_terminal_label', False)
		ui.update_label(
			'terminal_page__exit_terminal_label',
			'You are in the chroot environment.')
		ui.set_visible('terminal_page__exit_terminal_label', True)
	else:
		# Disable input to the terminal.
		ui.set_sensitive('terminal_page__terminal', False)
		ui.set_label_error('terminal_page__exit_terminal_label', True)
		ui.update_label(
			'terminal_page__exit_terminal_label',
			'You are not in the chroot environment. Terminal input has been disabled.'
		)
		ui.set_visible('terminal_page__exit_terminal_label', True)

	ui.hide_spinner()
	utils.initialize_chroot_environment(thread)
	ui.reset_buttons(True, True, is_chroot)


def _from__unsquashfs_page__to__options_page(thread):
	to__options_page(
		'unsquashfs_page',
		thread)


#
# Terminal Page
#


def from__terminal_page__to__options_page(thread):
	to__options_page('terminal_page', thread)


def from__terminal_page__to__copy_files_page(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons()

	# Create a file details list of files to be copied.
	file_details_list = utils.create_file_details_list()

	# [2] Prepare and display the new page.

	ui.set_sensitive('copy_files_page__scrolled_window', False)
	ui.reset_buttons(True, True, False)

	total_files = len(model.uris)
	current_directory = utils.get_current_directory()
	relative_directory = os.path.join(
		'/',
		os.path.relpath(current_directory,
						vars.prj.path.c_squash))

	if total_files == 1:
		label = 'Copy one file to %s' % relative_directory
	else:
		label = 'Copy %s files to %s' % (total_files, relative_directory)
	ui.update_label('copy_files_page__progress_label', label)

	ui.update_progressbar_text(
		'copy_files_page__copy_files_progressbar',
		None)
	ui.update_progressbar_percent(
		'copy_files_page__copy_files_progressbar',
		0)
	ui.update_liststore(
		'copy_files_page__file_details__liststore',
		file_details_list)

	# Transition to the Copy Files page.
	ui.show_page('terminal_page', 'copy_files_page')
	ui.set_sensitive('copy_files_page__scrolled_window', True)

	# [3] Perform functions on the new page, and activate the new page.

	ui.hide_spinner()
	ui.reset_buttons(
		True,
		True,
		True,
		back_button_label='Cancel',
		next_button_label='Copy')


#
# Copy Files Page
#

# TODO: Consider converting to (page, function) instead of (page, page)
# For example,...
# (1) from__copy_files_page__to__terminal_page_copy_files()
#     - Copy files first, then automatically transition to terminal page.
# (2) from__copy_files_page__to__terminal_page_cancel_copy_files()
#     - Do not Copy files; automatically transition to terminal page.


def from__copy_files_page__to__terminal_page_copy_files(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.reset_buttons(True, True, False)

	# Copy the files.
	utils.copy_files(thread)

	ui.reset_buttons()

	# Pause to let the user read the screen before automatic transition.
	time.sleep(1.0)
	ui.show_spinner()

	# [2] Prepare and display the new page.

	# ui.update_label(
	#     'terminal_page__exit_terminal_label',
	#     'You have exited from the chroot environment. Click the Back button to re-enter.')
	# ui.set_visible('terminal_page__exit_terminal_label', False)

	# Transition to the Copy Files page.
	ui.show_page('copy_files_page', 'terminal_page')
	ui.reset_buttons(True, True, True)

	# [3] Perform functions on the new page, and activate the new page.

	ui.hide_spinner()


#
# Options Page
#


def from__options_page__to__repackage_iso_page(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons()

	# Update filesystem.manifest-remove file.
	# filename = 'filesystem.manifest-remove'
	# is_exists = utils.is_exists_filesystem_manifest_remove(filename)
	# removable_packages_list = utils.create_typical_removable_packages_list(
	# )
	# Save the filesystem.manifest-remove file if there are packages to remove
	# or if the file already exists.
	# if is_exists or removable_packages_list:
	#     # This function will create the file if it does not exist.
	#     utils.create_filesystem_manifest_remove_file(
	#         filename,
	#         removable_packages_list)

	# Update filesystem.manifest-remove file.
	# Always save the filesystem.manifest-remove file, even if there are no
	# packages to remove. If the does not exist an empty file will be created.
	# This function will create the file if it does not exist.
	filename = 'filesystem.manifest-remove'
	removable_packages_list = utils.create_typical_removable_packages_list(
	)
	utils.create_filesystem_manifest_remove_file(
		filename,
		removable_packages_list)

	# Update filesystem.manifest-minimal-remove file.
	filename = 'filesystem.manifest-minimal-remove'
	is_exists = utils.is_exists_filesystem_manifest_remove(filename)
	removable_packages_list = utils.create_minimal_removable_packages_list(
	)
	# Save the filesystem.manifest-minimal-remove file if there are packages to
	# remove or if the file already exists. If the file does not exist, there
	# will not be any packages to remove. (In the future, if we may show the
	# minimal remove column even when the file does not exist).
	if is_exists or removable_packages_list:
		# This function will create the file if it does not exist.
		utils.create_filesystem_manifest_remove_file(
			filename,
			removable_packages_list)

	# Save preseed files.
	# TODO: Remove this line when 14.04 is no longer supported.
	# Bypass this functionality for Ubuntu 14.04.
	if vars.app.builder.get_object('page_options__tab_preseed'):
		dbg('Save preseed files')
		utils.save_stack_buffers('page_options__tab_preseed')

	# Delete preseed files.
	if model.delete_list:
		dbg('Delete preseed files')
		for filepath in model.delete_list:
			try:
				dbg('Delete file', filepath)
				os.remove(filepath)
			except OSError as exception:
				dbg('Error deleting file', exception)
		model.delete_list = []

	# Save ISO boot configurations.
	# TODO: Remove this line when 14.04 is no longer supported.
	# Bypass this functionality for Ubuntu 14.04.
	if vars.app.builder.get_object('page_options__tab_boot_config'):
		dbg('Save ISO boot configurations')
		utils.save_stack_buffers(
			'page_options__tab_boot_config')
	else:
		utils.update_and_save_boot_configurations()

	# [2] Prepare and display the new page.

	ui.update_progressbar_percent(
		'repackage_iso_page__copy_boot_files_progressbar',
		0)
	ui.update_progressbar_text(
		'repackage_iso_page__copy_boot_files_progressbar',
		'')
	ui.update_progressbar_percent(
		'repackage_iso_page__create_squashfs_progressbar',
		0)
	ui.update_progressbar_text(
		'repackage_iso_page__create_squashfs_progressbar',
		'')
	ui.update_progressbar_percent(
		'repackage_iso_page__create_iso_image_progressbar',
		0)
	ui.update_progressbar_text(
		'repackage_iso_page__create_iso_image_progressbar',
		'')

	ui.update_label(
		'repackage_iso_page__update_filesystem_size_result_label',
		'')
	ui.update_label(
		'repackage_iso_page__update_disk_name_result_label',
		'')
	ui.update_progressbar_percent(
		'repackage_iso_page__update_checksums_progressbar',
		0)
	ui.update_progressbar_text(
		'repackage_iso_page__update_checksums_progressbar',
		'')
	ui.set_label_error(
		'repackage_iso_page__check_iso_size_result_label',
		False)
	ui.update_label(
		'repackage_iso_page__check_iso_size_result_label',
		'')
	ui.update_label(
		'repackage_iso_page__calculate_iso_image_md5_sum_result_label',
		'')
	ui.update_label(
		'repackage_iso_page__calculate_iso_image_md5_filename_result_label',
		'')
	ui.update_status(
		'repackage_iso_page__copy_boot_files',
		ui.BULLET)
	ui.update_status(
		'repackage_iso_page__create_squashfs',
		ui.BULLET)
	ui.update_status(
		'repackage_iso_page__update_filesystem_size',
		ui.BULLET)
	ui.update_status(
		'repackage_iso_page__update_disk_name',
		ui.BULLET)
	ui.update_status(
		'repackage_iso_page__update_checksums',
		ui.BULLET)
	ui.update_status('repackage_iso_page__check_iso_size', ui.BULLET)
	ui.update_status(
		'repackage_iso_page__create_iso_image',
		ui.BULLET)
	ui.update_status(
		'repackage_iso_page__calculate_iso_image_md5_sum',
		ui.BULLET)

	# Transition to the Repackage ISO page.
	ui.show_page('options_page', 'repackage_iso_page')
	ui.reset_buttons(True, True, False, next_button_label='Finish')

	# [3] Perform functions on the new page, and activate the new page.

	ui.hide_spinner()

	# Copy boot files.
	ui.update_status(
		'repackage_iso_page__copy_boot_files',
		ui.PROCESSING)
	time.sleep(0.25)
	
	if not vars.prj.use_copied:
		dbg('Running utils.copy_vmlinuz_and_initrd_files')
		utils.copy_vmlinuz_and_initrd_files(thread)
	else:
		dbg('Skipping utils.copy_vmlinuz_and_initrd_files')
	
	ui.update_status('repackage_iso_page__copy_boot_files', ui.OK)
	time.sleep(1.00)

	# Create squashfs.
	ui.update_status(
		'repackage_iso_page__create_squashfs',
		ui.PROCESSING)
	time.sleep(0.25)
	utils.squashfs_mk(thread)
	ui.update_status('repackage_iso_page__create_squashfs', ui.OK)
	time.sleep(1.00)

	# Update filesystem size.
	ui.update_status(
		'repackage_iso_page__update_filesystem_size',
		ui.PROCESSING)
	time.sleep(0.25)
	filesystem_size = utils.update_filesystem_size(thread)
	dbg(
		'The file system size is',
		'%.2f GiB (%s bytes)' %
		((filesystem_size / 1073741824.0),
		 filesystem_size))
	ui.update_label(
		'repackage_iso_page__update_filesystem_size_result_label',
		'The file system size is %.2f GiB (%s bytes).' %
		((filesystem_size / 1073741824.0),
		 filesystem_size))
	ui.update_status(
		'repackage_iso_page__update_filesystem_size',
		ui.OK)
	time.sleep(1.00)

	# Update disk name.
	ui.update_status(
		'repackage_iso_page__update_disk_name',
		ui.PROCESSING)
	time.sleep(0.25)
	utils.update_disk_name()
	utils.update_disk_info()
	dbg('Updated the disk name', model.custom_iso_disk_name)
	ui.update_label(
		'repackage_iso_page__update_disk_name_result_label',
		'The disk name is %s.' % model.custom_iso_disk_name)
	ui.update_status('repackage_iso_page__update_disk_name', ui.OK)
	time.sleep(1.00)

	# Update MD5 sums.
	ui.update_status(
		'repackage_iso_page__update_checksums',
		ui.PROCESSING)
	time.sleep(0.25)
	checksums_filepath = os.path.join(
		vars.prj.path.c_iso,
		'md5sum.txt')
	exclude_paths = [
		os.path.join(vars.prj.path.c_iso,
					 'isolinux'),
		checksums_filepath
	]
	count = utils.update_md5_checksums(
		checksums_filepath,
		vars.prj.path.c_iso,
		exclude_paths)
	# count = utils.count_lines(checksums_filepath, thread)
	dbg('Number of checksums calculated', count)
	ui.update_progressbar_text(
		'repackage_iso_page__update_checksums_progressbar',
		'Calculated checksums for %i files' % count)
	ui.update_status('repackage_iso_page__update_checksums', ui.OK)
	time.sleep(1.00)

	# Check ISO size and create ISO image.
	ui.update_status(
		'repackage_iso_page__check_iso_size',
		ui.PROCESSING)
	time.sleep(0.25)
	directory_size_bytes = utils.get_directory_size(
		vars.prj.path.c_iso)
	directory_size_gib = directory_size_bytes / 1073741824.0
	dbg(
		'The total size of all files on the disk is',
		'%.2f GiB (%i bytes)' % (directory_size_gib,
								 directory_size_bytes))
	dbg(
		'The maximum size limit for all files on the disk is',
		'%.2f GiB (%i bytes)' % (MAXIMUM_ISO_SIZE_GIB,
								 MAXIMUM_ISO_SIZE_BYTES))
	# TODO: Add or improve log statments below.
	if directory_size_bytes > MAXIMUM_ISO_SIZE_BYTES:
		# Show directory size error message.
		ui.update_status(
			'repackage_iso_page__check_iso_size',
			ui.ERROR)
		ui.set_label_error(
			'repackage_iso_page__check_iso_size_result_label',
			True)
		ui.update_label(
			'repackage_iso_page__check_iso_size_result_label',
			'The total size of all files is %.2f GiB (%i bytes).' %
			(directory_size_gib,
			 directory_size_bytes) + os.linesep +
			'This is larger than the %.2f GiB (%i bytes) limit.' %
			(MAXIMUM_ISO_SIZE_GIB,
			 MAXIMUM_ISO_SIZE_BYTES) + os.linesep +
			'Click the Back button, and reduce the size of the Linux file system.'
		)
		dbg(ui.ERROR, 'Disk size exceeds maximum')
	else:
		# Show directory size.
		ui.update_status('repackage_iso_page__check_iso_size', ui.OK)
		ui.set_label_error(
			'repackage_iso_page__check_iso_size_result_label',
			False)
		ui.update_label(
			'repackage_iso_page__check_iso_size_result_label',
			'The total size of all files is %.2f GiB (%i bytes).' %
			(directory_size_gib,
			 directory_size_bytes))

		# Create ISO image.
		ui.update_status(
			'repackage_iso_page__create_iso_image',
			ui.PROCESSING)
		time.sleep(0.25)
		utils.create_iso_image(thread)
		ui.update_status(
			'repackage_iso_page__create_iso_image',
			ui.OK)
		time.sleep(1.00)

		# Calculate ISO image MD5 checksum.
		ui.update_status(
			'repackage_iso_page__calculate_iso_image_md5_sum',
			ui.PROCESSING)
		time.sleep(0.25)
		utils.calculate_md5_hash_for_iso()
		ui.update_label(
			'repackage_iso_page__calculate_iso_image_md5_sum_result_label',
			'The MD5 checksum is %s.' % model.custom_iso_md5_sum)
		ui.update_label(
			'repackage_iso_page__calculate_iso_image_md5_filename_result_label',
			'The MD5 checksum file is %s.' %
			model.custom_iso_md5_filename)
		ui.update_status(
			'repackage_iso_page__calculate_iso_image_md5_sum',
			ui.OK)
		time.sleep(0.25)

		ui.reset_buttons(True, True, True, next_button_label='Finish')


#
# Repackage ISO Page
#


def from__repackage_iso_page__to__finish_page(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons(False, False, False, next_button_label='Close')

	# [2] Prepare and display the new page.

	# Display custom values.
	ui.update_entry(
		'finish_page__custom_iso_version_number_entry',
		model.custom_iso_version_number)
	ui.update_entry(
		'finish_page__custom_iso_filename_entry',
		model.custom_iso_filename)
	ui.update_entry(
		'finish_page__custom_iso_dir_entry',
		model.custom_iso_dir)
	ui.update_entry(
		'finish_page__custom_iso_volume_id_entry',
		model.custom_iso_volume_id)
	ui.update_entry(
		'finish_page__custom_iso_release_name_entry',
		model.custom_iso_release_name)
	ui.update_entry(
		'finish_page__custom_iso_disk_name_entry',
		model.custom_iso_disk_name)
	ui.update_entry(
		'finish_page__custom_iso_md5_entry',
		model.custom_iso_md5_sum)
	ui.update_entry(
		'finish_page__custom_iso_md5_filename_entry',
		model.custom_iso_md5_filename)

	# Transition to the Finish page.
	ui.show_page('repackage_iso_page', 'finish_page')
	ui.reset_buttons(False, False, True, next_button_label='Close')

	# [3] Perform functions on the new page, and activate the new page.

	ui.hide_spinner()


########################################################################
# TransitionThread - Transition "Back" Functions
########################################################################


def from__new_project_page__to__project_directory_page(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons()

	# Unmount the original ISO image and delete the mount point.
	dbg(
		'Unmount and delete the original ISO image mount point',
		vars.prj.path.o_iso_m)
	utils.delete_iso_mount(vars.prj.path.o_iso_m, thread)


	# Transition to the Project Directory page.
	ui.show_page('new_project_page', 'project_directory_page')
	ui.reset_buttons(True, False, True)

	# [3] Perform functions on the new page, and activate the new page.

	ui.hide_spinner()


def from__existing_project_page__to__project_directory_page(
		thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons()

	# Unmount the original ISO image and delete the mount point.
	dbg(
		'Unmount and delete the original ISO image mount point',
		vars.prj.path.o_iso_m)
	utils.delete_iso_mount(vars.prj.path.o_iso_m, thread)


	# Transition to the Project Directory page.
	ui.show_page('existing_project_page', 'project_directory_page')
	ui.reset_buttons(True, False, True)

	# [3] Perform functions on the new page, and activate the new page.

	ui.hide_spinner()


def from__delete_project_page__to__existing_project_page(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons()

	ui.set_sensitive(
		'existing_project_page__original_iso_filepath_filechooser__open_button',
		False)

	validate.existing_project_page_for_delete()

	# [2] Prepare and display the new page.

	# Transition to the Existing Project page.
	ui.show_page('delete_project_page', 'existing_project_page')
	ui.reset_buttons(True, True, True)

	# [3] Perform functions on the new page, and activate the new page.

	ui.hide_spinner()


def from__unsquashfs_page__to__existing_project_page(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	model.set_propagate(False)

	ui.show_spinner()
	ui.reset_buttons()

	# [2] Prepare and display the new page.

	# Display original values.
	ui.update_entry(
		'existing_project_page__original_iso_filename_entry',
		model.original_iso_filename)
	ui.update_entry(
		'existing_project_page__original_iso_directory_entry',
		model.original_iso_directory)
	ui.update_entry(
		'existing_project_page__original_iso_volume_id_entry',
		model.original_iso_volume_id)
	ui.update_entry(
		'existing_project_page__original_iso_release_name_entry',
		model.original_iso_release_name)
	ui.update_entry(
		'existing_project_page__original_iso_disk_name_entry',
		model.original_iso_disk_name)

	# Display custom values.
	ui.update_entry(
		'existing_project_page__custom_iso_version_number_entry',
		model.custom_iso_version_number)
	ui.update_entry(
		'existing_project_page__custom_iso_filename_entry',
		model.custom_iso_filename)
	ui.update_entry(
		'existing_project_page__custom_iso_dir_entry',
		model.custom_iso_dir)
	ui.update_entry(
		'existing_project_page__custom_iso_volume_id_entry',
		model.custom_iso_volume_id)
	ui.update_entry(
		'existing_project_page__custom_iso_release_name_entry',
		model.custom_iso_release_name)
	ui.update_entry(
		'existing_project_page__custom_iso_disk_name_entry',
		model.custom_iso_disk_name)

	# Transition to the Existing Project page.
	ui.show_page('unsquashfs_page', 'existing_project_page')
	ui.reset_buttons(True, True, True)

	# [3] Perform functions on the new page, and activate the new page.

	validate.existing_project_page()

	ui.hide_spinner()

	model.set_propagate(True)


def from__terminal_page__to__existing_project_page(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	model.set_propagate(False)

	ui.show_spinner()
	ui.reset_buttons()

	# Exit the chroot environment.

	ui.set_label_error('terminal_page__exit_terminal_label', False)
	ui.update_label(
		'terminal_page__exit_terminal_label',
		'Exiting from the chroot environment')
	ui.set_visible('terminal_page__exit_terminal_label', True)

	# Disconnect terminal exit handler.
	if model.handler_id:
		terminal = vars.app.builder.get_object('terminal_page__terminal')
		terminal.disconnect(model.handler_id)
		model.set_handler_id(None)

	utils.exit_chroot_environment(thread)

	is_chroot = utils.check_chroot(thread)

	if not is_chroot:
		# Disable input to the terminal.
		ui.set_sensitive('terminal_page__terminal', False)
		ui.set_label_error('terminal_page__exit_terminal_label', False)
		ui.update_label(
			'terminal_page__exit_terminal_label',
			'Successfully exited from the chroot environment.')
		ui.set_visible('terminal_page__exit_terminal_label', True)
	else:
		# Enable input to the terminal.
		ui.set_sensitive('terminal_page__terminal', True)
		ui.set_label_error('terminal_page__exit_terminal_label', True)
		ui.update_label(
			'terminal_page__exit_terminal_label',
			'Unable to exit from the chroot environment.')
		ui.set_visible('terminal_page__exit_terminal_label', True)

	time.sleep(1.00)

	# [2] Prepare and display the new page.

	# Display original values.
	ui.update_entry(
		'existing_project_page__original_iso_filename_entry',
		model.original_iso_filename)
	ui.update_entry(
		'existing_project_page__original_iso_directory_entry',
		model.original_iso_directory)
	ui.update_entry(
		'existing_project_page__original_iso_volume_id_entry',
		model.original_iso_volume_id)
	ui.update_entry(
		'existing_project_page__original_iso_release_name_entry',
		model.original_iso_release_name)
	ui.update_entry(
		'existing_project_page__original_iso_disk_name_entry',
		model.original_iso_disk_name)

	# Display custom values.
	ui.update_entry(
		'existing_project_page__custom_iso_version_number_entry',
		model.custom_iso_version_number)
	ui.update_entry(
		'existing_project_page__custom_iso_filename_entry',
		model.custom_iso_filename)
	ui.update_entry(
		'existing_project_page__custom_iso_dir_entry',
		model.custom_iso_dir)
	ui.update_entry(
		'existing_project_page__custom_iso_volume_id_entry',
		model.custom_iso_volume_id)
	ui.update_entry(
		'existing_project_page__custom_iso_release_name_entry',
		model.custom_iso_release_name)
	ui.update_entry(
		'existing_project_page__custom_iso_disk_name_entry',
		model.custom_iso_disk_name)

	# Transition to the Existing Project page.
	ui.show_page('terminal_page', 'existing_project_page')
	ui.reset_buttons(True, True, True)

	# [3] Perform functions on the new page, and activate the new page.

	validate.existing_project_page()

	ui.hide_spinner()

	model.set_propagate(True)


def from__copy_files_page__to__terminal_page_cancel_copy_files(
		thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	# [2] Prepare and display the new page.

	transition('copy_files_page', 'terminal_page', True, True, True)

	# [3] Perform functions on the new page, and activate the new page.


def from__options_page__to__terminal_page(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons()

	# [2] Prepare and display the new page.

	# ui.set_label_error('terminal_page__exit_terminal_label', False)
	# ui.update_label(
	#     'terminal_page__exit_terminal_label',
	#     '...')
	# ui.set_visible('terminal_page__exit_terminal_label', True)

	# Transition to the Terminal page.
	ui.show_page('options_page', 'terminal_page')

	# [3] Perform functions on the new page, and activate the new page.

	# Enter the chroot environment.

	ui.set_label_error('terminal_page__exit_terminal_label', False)
	ui.update_label(
		'terminal_page__exit_terminal_label',
		'Entering the chroot environment')
	ui.set_visible('terminal_page__exit_terminal_label', True)

	utils.prepare_chroot_environment(thread)
	utils.create_chroot_terminal(thread)

	is_chroot = utils.check_chroot(thread)

	time.sleep(1.00)

	if is_chroot:
		# Connect terminal exit handler.
		terminal = vars.app.builder.get_object('terminal_page__terminal')
		handler_id = terminal.connect(
			'child-exited',
			handlers.on_child_exited__terminal_page)
		model.set_handler_id(handler_id)
		# Enable input to the terminal.
		ui.set_sensitive('terminal_page__terminal', True)
		ui.set_label_error('terminal_page__exit_terminal_label', False)
		ui.update_label(
			'terminal_page__exit_terminal_label',
			'You are in the chroot environment.')
		ui.set_visible('terminal_page__exit_terminal_label', True)
	else:
		# Disable input to the terminal.
		ui.set_sensitive('terminal_page__terminal', False)
		ui.set_label_error('terminal_page__exit_terminal_label', True)
		ui.update_label(
			'terminal_page__exit_terminal_label',
			'You are not in the chroot environment. Terminal input has been disabled.'
		)
		ui.set_visible('terminal_page__exit_terminal_label', True)

	ui.hide_spinner()
	utils.initialize_chroot_environment(thread)
	ui.reset_buttons(True, True, is_chroot)


def from__options_page__to__existing_project_page(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	# [2] Prepare and display the new page.

	transition('options_page', 'existing_project_page', True, True, True)

	# [3] Perform functions on the new page, and activate the new page.

	validate.existing_project_page()


def from__repackage_iso_page__to__options_page(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons()

	# [2] Prepare and display the new page.

	# Transition to the Manage Options.
	ui.show_page('repackage_iso_page', 'options_page')
	ui.reset_buttons(True, True, True, next_button_label='Generate')

	# [3] Perform functions on the new page, and activate the new page.

	ui.hide_spinner()


########################################################################
# TransitionThread - Transition "Exit" Functions
########################################################################


def from__project_directory_page__to__exit(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons()

	ui.main_quit()
	ui.hide_spinner()

	# [2] Prepare and display the new page.

	# [3] Perform functions on the new page, and activate the new page.

	dbg('Done')


def from__new_project_page__to__exit(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons()

	# Unmount the original ISO image and delete the mount point.
	dbg(
		'Unmount and delete the original ISO image mount point',
		vars.prj.path.o_iso_m)
	utils.delete_iso_mount(vars.prj.path.o_iso_m, thread)

	ui.main_quit()
	ui.hide_spinner()

	# [2] Prepare and display the new page.

	# [3] Perform functions on the new page, and activate the new page.

	dbg('Done')


def from__existing_project_page__to__exit(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons()

	# Unmount the original ISO image and delete the mount point.
	dbg(
		'Unmount and delete the original ISO image mount point',
		vars.prj.path.o_iso_m)
	utils.delete_iso_mount(vars.prj.path.o_iso_m, thread)

	ui.main_quit()
	ui.hide_spinner()

	# [2] Prepare and display the new page.

	# [3] Perform functions on the new page, and activate the new page.

	dbg('Done')


def from__delete_project_page__to__exit(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons()

	# Unmount the original ISO image and delete the mount point.
	dbg(
		'Unmount and delete the original ISO image mount point',
		vars.prj.path.o_iso_m)
	utils.delete_iso_mount(vars.prj.path.o_iso_m, thread)

	ui.main_quit()
	ui.hide_spinner()

	# [2] Prepare and display the new page.

	# [3] Perform functions on the new page, and activate the new page.

	dbg('Done')


def from__unsquashfs_page__to__exit(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons()

	# Unmount the original ISO image and delete the mount point.
	dbg(
		'Unmount and delete the original ISO image mount point',
		vars.prj.path.o_iso_m)
	utils.delete_iso_mount(vars.prj.path.o_iso_m, thread)

	ui.main_quit()
	ui.hide_spinner()

	# [2] Prepare and display the new page.

	# [3] Perform functions on the new page, and activate the new page.

	dbg('Done')


def from__terminal_page__to__exit(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons()

	# Unmount and delete directory (original-iso-mount).
	dbg(
		'Unmount and delete the original ISO image mount point',
		vars.prj.path.o_iso_m)
	utils.delete_iso_mount(vars.prj.path.o_iso_m, thread)

	# Exit the chroot environment.

	ui.set_label_error('terminal_page__exit_terminal_label', False)
	ui.update_label(
		'terminal_page__exit_terminal_label',
		'Exiting from the chroot environment')
	ui.set_visible('terminal_page__exit_terminal_label', True)

	# Disconnect terminal exit handler.
	if model.handler_id:
		terminal = vars.app.builder.get_object('terminal_page__terminal')
		terminal.disconnect(model.handler_id)
		model.set_handler_id(None)

	utils.exit_chroot_environment(thread)

	is_chroot = utils.check_chroot(thread)

	if not is_chroot:
		# Disable input to the terminal.
		ui.set_sensitive('terminal_page__terminal', False)
		ui.set_label_error('terminal_page__exit_terminal_label', False)
		ui.update_label(
			'terminal_page__exit_terminal_label',
			'Successfully exited from the chroot environment.')
		ui.set_visible('terminal_page__exit_terminal_label', True)
	else:
		# Enable input to the terminal.
		ui.set_sensitive('terminal_page__terminal', True)
		ui.set_label_error('terminal_page__exit_terminal_label', True)
		ui.update_label(
			'terminal_page__exit_terminal_label',
			'Unable to exit from the chroot environment.')
		ui.set_visible('terminal_page__exit_terminal_label', True)

	time.sleep(1.00)
	ui.main_quit()
	ui.hide_spinner()
	dbg('Done')


def from__terminal_page__to__terminal_page(thread):
	ui.show_spinner()
	ui.reset_buttons(True, True, False)
	ui.set_label_error('terminal_page__exit_terminal_label', False)
	ui.update_label(
		'terminal_page__exit_terminal_label',
		'Exiting from the chroot environment')
	ui.set_visible('terminal_page__exit_terminal_label', True)
	# Disconnect terminal exit handler.
	if model.handler_id:
		terminal = vars.app.builder.get_object('terminal_page__terminal')
		terminal.disconnect(model.handler_id)
		model.set_handler_id(None)
	utils.exit_chroot_environment(thread)
	is_chroot = utils.check_chroot(thread)
	if not is_chroot:
		# Disable input to the terminal.
		ui.set_sensitive('terminal_page__terminal', False)
		ui.set_label_error('terminal_page__exit_terminal_label', False)
		ui.update_label(
			'terminal_page__exit_terminal_label',
			'Successfully exited from the chroot environment.')
		ui.set_visible('terminal_page__exit_terminal_label', True)
	else:
		# Enable input to the terminal.
		ui.set_sensitive('terminal_page__terminal', True)
		ui.set_label_error('terminal_page__exit_terminal_label', True)
		ui.update_label(
			'terminal_page__exit_terminal_label',
			'Unable to exit from the chroot environment.')
		ui.set_visible('terminal_page__exit_terminal_label', True)

	time.sleep(0.25)

	# Enter the chroot environment.

	ui.set_label_error('terminal_page__exit_terminal_label', False)
	ui.update_label(
		'terminal_page__exit_terminal_label',
		'Reentering the chroot environment')
	ui.set_visible('terminal_page__exit_terminal_label', True)

	utils.prepare_chroot_environment(thread)
	utils.create_chroot_terminal(thread)

	is_chroot = utils.check_chroot(thread)

	time.sleep(1.00)

	if is_chroot:
		# Connect terminal exit handler.
		terminal = vars.app.builder.get_object('terminal_page__terminal')
		handler_id = terminal.connect(
			'child-exited',
			handlers.on_child_exited__terminal_page)
		model.set_handler_id(handler_id)
		# Enable input to the terminal.
		ui.set_sensitive('terminal_page__terminal', True)
		ui.set_label_error('terminal_page__exit_terminal_label', False)
		ui.update_label(
			'terminal_page__exit_terminal_label',
			'You are in the chroot environment.')
		ui.set_visible('terminal_page__exit_terminal_label', True)
	else:
		# Disable input to the terminal.
		ui.set_sensitive('terminal_page__terminal', False)
		ui.set_label_error('terminal_page__exit_terminal_label', True)
		ui.update_label(
			'terminal_page__exit_terminal_label',
			'You are not in the chroot environment. Terminal input has been disabled.'
		)
		ui.set_visible('terminal_page__exit_terminal_label', True)

	ui.hide_spinner()
	utils.initialize_chroot_environment(thread)
	ui.reset_buttons(True, True, is_chroot)


def from__copy_files_page__to__exit(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	# This is the same as exiting the terminal page.

	ui.show_spinner()
	ui.reset_buttons()

	# Unmount and delete directory (original-iso-mount).
	dbg(
		'Unmount and delete the original ISO image mount point',
		vars.prj.path.o_iso_m)
	utils.delete_iso_mount(vars.prj.path.o_iso_m, thread)

	# Exit the chroot environment.

	ui.set_label_error('terminal_page__exit_terminal_label', False)
	ui.update_label(
		'terminal_page__exit_terminal_label',
		'Exiting from the chroot environment')
	ui.set_visible('terminal_page__exit_terminal_label', True)

	# Disconnect terminal exit handler.
	if model.handler_id:
		terminal = vars.app.builder.get_object('terminal_page__terminal')
		terminal.disconnect(model.handler_id)
		model.set_handler_id(None)

	utils.exit_chroot_environment(thread)

	is_chroot = utils.check_chroot(thread)

	### TODO: This is not needed because we are not on the terminal page.
	if not is_chroot:
		# Disable input to the terminal.
		ui.set_sensitive('terminal_page__terminal', False)
		ui.set_label_error('terminal_page__exit_terminal_label', False)
		ui.update_label(
			'terminal_page__exit_terminal_label',
			'Successfully exited from the chroot environment.')
		ui.set_visible('terminal_page__exit_terminal_label', True)
	else:
		# Enable input to the terminal.
		ui.set_sensitive('terminal_page__terminal', True)
		ui.set_label_error('terminal_page__exit_terminal_label', True)
		ui.update_label(
			'terminal_page__exit_terminal_label',
			'Unable to exit from the chroot environment.')
		ui.set_visible('terminal_page__exit_terminal_label', True)

	ui.main_quit()
	ui.hide_spinner()

	# [2] Prepare and display the new page.

	# [3] Perform functions on the new page, and activate the new page.

	dbg('Done')


def from__options_page__to__exit(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons()

	# Unmount and delete directory (original-iso-mount).
	dbg(
		'Unmount and delete the original ISO image mount point',
		vars.prj.path.o_iso_m)
	utils.delete_iso_mount(vars.prj.path.o_iso_m, thread)

	ui.main_quit()
	ui.hide_spinner()

	# [2] Prepare and display the new page.

	# [3] Perform functions on the new page, and activate the new page.

	dbg('Done')


def from__repackage_iso_page__to__exit(thread):
	ui.show_spinner()
	ui.reset_buttons()
	dbg(
		'Unmount and delete the original ISO image mount point',
		vars.prj.path.o_iso_m)
	utils.delete_iso_mount(vars.prj.path.o_iso_m, thread)
	ui.main_quit()
	ui.hide_spinner()
	dbg('Done')


def from__finish_page__to__exit(thread):

	# [1] Perform functions on the current page, and deactivate the current page.

	ui.show_spinner()
	ui.reset_buttons(False, False, False, next_button_label='Close')

	# Unmount and delete directory (original-iso-mount).
	dbg(
		'Unmount and delete the original ISO image mount point',
		vars.prj.path.o_iso_m)
	utils.delete_iso_mount(vars.prj.path.o_iso_m, thread)

	# Delete project files, if requested.
	checkbutton = vars.app.builder.get_object(
		'finish_page__delete_project_files_checkbutton')
	is_active = checkbutton.get_active()
	dbg(
		'The delete project files checkbutton is checked?',
		is_active)
	if is_active:
		# Delete directory: custom-live-iso
		dbg(
			'Delete the custom live ISO directory',
			vars.prj.path.c_iso)
		utils.delete_file(vars.prj.path.c_iso, thread)

		# Delete file: timbrel.conf
		dbg(
			'Delete the configuration file',
			vars.prj.path.cfg)
		utils.delete_file(vars.prj.path.cfg, thread)

		# Delete directory: squashfs-root
		dbg(
			'Delete the custom squashfs directory',
			vars.prj.path.c_squash)
		utils.delete_file(vars.prj.path.c_squash, thread)


	ui.main_quit()
	ui.hide_spinner()

	# [2] Prepare and display the new page.

	# [3] Perform functions on the new page, and activate the new page.

	dbg('Done')
