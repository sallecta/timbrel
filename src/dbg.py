#!/usr/bin/python3
from inspect import getframeinfo, stack
import os

class dbg_class:
	def __init__(self, nd=0):
		self.nd = nd
	def dbg(self,*args,sp=' ',nd=None):
		caller = getframeinfo(stack()[1][0])
		#inspect.stack()[1].function
		fname=os.path.basename(caller.filename)
		func=caller.function
		lno=caller.lineno
		msg=None
		if nd==None:
			nd = self.nd
		
		nd='    ' * nd
		ndx=0
		for arg in args:
			if ndx==0:
				msg="%s" % (arg)
			else:
				msg="%s%s%s" % (msg,sp,arg)
			ndx=ndx+1
		if func and func!='<module>':
			print("%s%s[%d][%s]: %s" % (nd,fname, lno, func, msg)) 
		else:
			print("%s%s[%d]: %s" % (nd,fname, lno, msg)) 
