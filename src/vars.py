import os
from calm import calm

class prj(metaclass=calm):
	pass
	
class app(metaclass=calm):
	pass

app.path=os.path.dirname(os.path.realpath(__file__))
app.version='unknown'


#-----#

prj.path=calm.obj()
prj.path.root=None
prj.path.o_iso=None
prj.path.o_iso_m=None
prj.path.c_squash=None
prj.path.c_iso=None
prj.path.cfg=None

prj.file=calm.obj()
prj.file.cfg='timbrel.conf'

prj.dir=calm.obj()
prj.dir.c_iso='custom-live-iso'
prj.dir.c_squash='squashfs-root'
#prj.dir.o_iso=''
prj.dir.o_iso_m='original-iso-mount'

prj.bootconfigs_factory = ['boot/grub/grub.cfg','boot/grub/loopback.cfg','isolinux/txt.cfg','isolinux/isolinux.cfg']
prj.bootconfigs = []

prj.use_copied=False
