#!/usr/bin/python3

########################################################################
#                                                                      #
# transition.py                                                        #
#                                                                      #
# Copyright (C) 2015 PJ Singh <psingh.cubic@gmail.com>                 #
#                                                                      #
########################################################################

########################################################################
#                                                                      #
# This file is part of Timbrel - Custom Ubuntu ISO Creator.              #
#                                                                      #
# Timbrel is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# (at your option) any later version.                                  #
#                                                                      #
# Timbrel is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public License    #
# along with Timbrel. If not, see <http://www.gnu.org/licenses/>.        #
#                                                                      #
########################################################################

from dbg import *
dbg = dbg_class(nd=1).dbg

import ui
import transitions

import ctypes
import sys
from threading import Thread
import time
import traceback

########################################################################
# InterruptException Class
########################################################################


class InterruptException(Exception):
	pass



class TransitionThread(Thread):
	process = None
	
	def __init__(self, page_name, new_page_name, previous_thread=None):
		if previous_thread:
			dbg('Created new thread')
			dbg('Current page', page_name)
			dbg('New page', new_page_name)
			dbg('Previous thread id', previous_thread.ident)
		else:
			dbg('Created new thread')
			dbg('Current page', page_name)
			dbg('New page', new_page_name)
		# Set instance variables
		self.page_name = page_name
		self.new_page_name = new_page_name
		self.previous_thread = previous_thread
		self.process = None
		Thread.__init__(self)
	
	def run(self):
		thread_id = self.ident
		dbg('Running thread with id', thread_id)
		self.interrupt_previous_thread()
		try:
			self.action()
		except InterruptException as exception:
			dbg(
				'InterruptException encountered in thread',
				self.ident)
			dbg('Ignoring exception')
			dbg('The tracekback is', traceback.format_exc())
		except Exception as exception:
			dbg('Exception encountered in thread', self.ident)
			dbg('Ignore exception?', 'No')
			# dbg('The exception is', exception)
			dbg('The tracekback is', traceback.format_exc())
		dbg('Finished running thread with id', thread_id)
	
	def get_process_id(self):
		if self.process:
			return self.process.pid
		else:
			return -1
	
	def get_process(self):
		if self.process:
			dbg('Returning process with id %i.' % self.process.pid)
			return self.process
		else:
			dbg('Process does not exists, return None.')
			return None
	
	def set_process(self, new_process):
		self.process = new_process
		dbg('Set a new process for thread id', self.ident)
		dbg('The new process id is', self.process.pid)
	
	def interrupt_previous_thread(self):
		sys.stdout.flush()
		if self.previous_thread and self.previous_thread.is_alive():
			ui.show_spinner()
			ui.reset_buttons()
			time.sleep(0.125)
			if self.previous_thread and self.previous_thread.is_alive():
				previous_thread_id = self.previous_thread.ident
				dbg('Interrupting previous thread',previous_thread_id)
				ctypes.pythonapi.PyThreadState_SetAsyncExc(
					ctypes.c_long(previous_thread_id),
					ctypes.py_object(InterruptException))
				self.terminate_process(self.previous_thread)
				self.previous_thread.join()
				# time.sleep(0.50)
				dbg('Done',nd=2)
			ui.hide_spinner()
		else:
			dbg('No previous thread to interrupt')
	
	def terminate_process(self, previous_thread):
		process = previous_thread.process
		if process:
			process_id = previous_thread.get_process_id()
			dbg('Terminating process', process_id)
			is_alive = process.isalive()
			dbg('Is process %s alive?' % process_id,is_alive)
			is_terminated = False
			if is_alive:
				try:
					dbg('Trying to terminate process',process_id)
					is_terminated = process.terminate(True)
				except Exception as exception:
					dbg('Exception in thread',self.ident)
					dbg(' process',process_id)
					# dbg('The exception is', exception)
					dbg('The tracekback is',traceback.format_exc())
					try:
						dbg('Trying to wait for process',process_id)
						# If the process is no longer alive, consider it terminated.
						process.wait()
						is_terminated = not process.isalive()
					except Exception as exception:
						dbg('Exception in thread',self.ident)
						dbg('Exception waiting for process',process_id)
						# dbg('The exception is', exception)
						dbg('The tracekback is',traceback.format_exc())
					else:
						dbg('Finished trying to wait for process',process_id)
				else:
					dbg('Finished trying to terminate process',process_id)
			else:
				is_terminated = True
			dbg('Process %s terminated?' % process_id,is_terminated)
		else:
			dbg('Process is not running')
	
	def action(self):
		dbg('Transition from', self.page_name, 'to', self.new_page_name)
		if self.page_name == 'project_directory_page__project_directory_filechooser':
			if self.new_page_name == 'project_directory_page':
				transitions.from__project_directory_page__project_directory_filechooser__to__project_directory_page(
					self)
			else:
				dbg('Error. No action defined for transition to',self.new_page_name)
		elif self.page_name == 'original_iso_filepath_filechooser':
			if self.new_page_name == 'new_project_page':
				transitions.from__original_iso_filepath_filechooser__to__new_project_page(self)
			elif self.new_page_name == 'existing_project_page':
				transitions.from__original_iso_filepath_filechooser__to__existing_project_page(self)
			else:
				dbg('Error. No action defined for transition to',self.new_page_name)
		elif self.page_name == 'custom_iso_dir_filechooser':
			if self.new_page_name == 'new_project_page':
				transitions.from__custom_iso_dir_filechooser__to__new_project_page(self)
			elif self.new_page_name == 'existing_project_page':
				transitions.from__custom_iso_dir_filechooser__to__existing_project_page(self)
			else:
				dbg('Error. No action defined for transition to',self.new_page_name)
		elif self.page_name == 'project_directory_page':
			if self.new_page_name == 'new_project_page':
				transitions.from__project_directory_page__to__new_project_page(self)
			elif self.new_page_name == 'existing_project_page':
				transitions.from__project_directory_page__to__existing_project_page(self)
			elif self.new_page_name == 'exit':
				transitions.from__project_directory_page__to__exit(self)
			else:
				dbg('Error. No action defined for transition to',self.new_page_name)
		elif self.page_name == 'new_project_page':
			if self.new_page_name == 'terminal_page':
				transitions.from__new_project_page__to__terminal_page(self)
			elif self.new_page_name == 'project_directory_page':
				transitions.from__new_project_page__to__project_directory_page(self)
			elif self.new_page_name == 'exit':
				transitions.from__new_project_page__to__exit(self)
			else:
				dbg('Error. No action defined for transition to',self.new_page_name)
		elif self.page_name == 'existing_project_page':
			if self.new_page_name == 'existing_project_page':
				transitions.from__existing_project_page__to__existing_project_page(self)
			elif self.new_page_name == 'options_page':
				transitions.from__existing_project_page__to__options_page(self)
			elif self.new_page_name == 'terminal_page':
				transitions.from__existing_project_page__to__terminal_page(self)
			elif self.new_page_name == 'delete_project_page':
				transitions.from__existing_project_page__to__delete_project_page(self)
			elif self.new_page_name == 'project_directory_page':
				transitions.from__existing_project_page__to__project_directory_page(self)
			elif self.new_page_name == 'exit':
				transitions.from__existing_project_page__to__exit(self)
			else:
				dbg('Error. No action defined for transition to',self.new_page_name)
		elif self.page_name == 'delete_project_page':
			if self.new_page_name == 'new_project_page':
				transitions.from__delete_project_page__to__new_project_page(self)
			elif self.new_page_name == 'existing_project_page':
				transitions.from__delete_project_page__to__existing_project_page(self)
			elif self.new_page_name == 'exit':
				transitions.from__delete_project_page__to__exit(self)
		elif self.page_name == 'unsquashfs_page':
			if self.new_page_name == 'existing_project_page':
				transitions.from__unsquashfs_page__to__existing_project_page(self)
			elif self.new_page_name == 'exit':
				transitions.from__unsquashfs_page__to__exit(self)
			else:
				dbg('Error. No action defined for transition to',self.new_page_name)
		elif self.page_name == 'terminal_page':
			if self.new_page_name == 'options_page':
				transitions.from__terminal_page__to__options_page(self)
			elif self.new_page_name == 'copy_files_page':
				transitions.from__terminal_page__to__copy_files_page(self)
			elif self.new_page_name == 'existing_project_page':
				transitions.from__terminal_page__to__existing_project_page(self)
			elif self.new_page_name == 'terminal_page':
				transitions.from__terminal_page__to__terminal_page(self)
			elif self.new_page_name == 'exit':
				transitions.from__terminal_page__to__exit(self)
			else:
				dbg('Error. No action defined for transition to',self.new_page_name)
		elif self.page_name == 'copy_files_page':
			if self.new_page_name == 'terminal_page_copy_files':
				transitions.from__copy_files_page__to__terminal_page_copy_files(self)
			elif self.new_page_name == 'terminal_page_cancel_copy_files':
				transitions.from__copy_files_page__to__terminal_page_cancel_copy_files(self)
			elif self.new_page_name == 'exit':
				transitions.from__copy_files_page__to__exit(self)
			else:
				dbg('Error. No action defined for transition to',self.new_page_name)
		elif self.page_name == 'options_page':
			if self.new_page_name == 'existing_project_page':
				transitions.from__options_page__to__existing_project_page(self)
			elif self.new_page_name == 'repackage_iso_page':
				transitions.from__options_page__to__repackage_iso_page(self)
			elif self.new_page_name == 'terminal_page':
				transitions.from__options_page__to__terminal_page(self)
			elif self.new_page_name == 'exit':
				transitions.from__options_page__to__exit(self)
			else:
				dbg('Error. No action defined for transition to',self.new_page_name)
		elif self.page_name == 'repackage_iso_page':
			if self.new_page_name == 'finish_page':
				transitions.from__repackage_iso_page__to__finish_page(self)
			elif self.new_page_name == 'options_page':
				transitions.from__repackage_iso_page__to__options_page(self)
			elif self.new_page_name == 'exit':
				transitions.from__repackage_iso_page__to__exit(self)
			else:
				dbg('Error. No action defined for transition to',self.new_page_name)
		elif self.page_name == 'finish_page':
			if self.new_page_name == 'exit':
				transitions.from__finish_page__to__exit(self)
			else:
				dbg('Error. No action defined for transition to',self.new_page_name)
		else:
			dbg('Error. No action defined for transition','From %s to %s' % (self.page_name,
			   self.new_page_name))
