#!/usr/bin/python3

########################################################################
#                                                                      #
# utils.py                                                         #
#                                                                      #
# Copyright (C) 2015 PJ Singh <psingh.cubic@gmail.com>                 #
#                                                                      #
########################################################################

########################################################################
#                                                                      #
# This file is part of Timbrel - Custom Ubuntu ISO Creator.              #
#                                                                      #
# Timbrel is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# (at your option) any later version.                                  #
#                                                                      #
# Timbrel is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public License    #
# along with Timbrel. If not, see <http://www.gnu.org/licenses/>.        #
#                                                                      #
########################################################################

from dbg import *
dbg = dbg_class(nd=1).dbg

import vars

import subprocess

import ui
import model

import configparser
import datetime
import gi
gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')
gi.require_version('GtkSource', vars.app.GtkSourceVersion)

from gi.repository import Gdk
from gi.repository import Gio
from gi.repository import GLib
from gi.repository import Gtk
from gi.repository import GtkSource
from gi.repository import Pango
try:
	gi.require_version('Vte', '2.91')
	dbg('Using Vte version', '2.91',nd=1)
except ValueError:
	gi.require_version('Vte', '2.90')
	dbg('Using Vte version', '2.90',nd=1)
from gi.repository.Vte import Terminal, PtyFlags
import glob
import hashlib
import os
import pexpect
import platform
import re
from shutil import copyfile, move
import signal
import stat
import string
import time
import traceback
from urllib.parse import urlparse, unquote
# import uuid

# https://en.wikipedia.org/wiki/ANSI_escape_code
# https://stackoverflow.com/questions/4842424/list-of-ansi-color-escape-sequences
BOLD_RED = '\033[1;31m'
BOLD_GREEN = '\033[1;32m'
BOLD_BLUE = '\033[1;34m'
BOLD_YELLOW = '\033[1;33;1m'
NORMAL = '\033[0m'

def version():
	version = "incorrectVersion"
	try:
		f = open(vars.app.path + "/version.txt", "r")
		version = f.read(5)
	except FileNotFoundError:
		pass
	return version


########################################################################
# Execute Functions
########################################################################

def exec_sync(acmd, thread=None, workingdir=None, silent=True):
	athread=thread
	aworkingdir=workingdir
	asilent=silent
	if not asilent:
		dbg(acmd)
	result = None
	error = 1
	try:
		# For Pexpect Pexpect 3.x
		process = pexpect.spawnu(acmd, timeout=300, cwd=aworkingdir)
		# For Pexpect 4.0
		# process = pexpect.spawn(acmd, timeout=300, cwd=aworkingdir, encoding='UTF-8')
		if athread:
			athread.set_process(process)
			if not asilent:
				dbg('The process id is', athread.get_process_id(),nd=2)
		result = process.read()
		result = result.strip() if result else None
		error = process.exitstatus
	except pexpect.ExceptionPexpect as exception:
			if not asilent:
				dbg('Exception while executing', acmd,nd=2)
				dbg('The tracekback is', traceback.format_exc(),nd=2)
	if not asilent:
		dbg(result,nd=2)
	return result, error


def exec_async(command, thread=None, working_directory=None):
	dbg(command)

	try:
		# For Pexpect Pexpect 3.x
		process = pexpect.spawnu(command, timeout=300, cwd=working_directory)
		# For Pexpect 4.0
		# process = pexpect.spawn(command, timeout=300, cwd=working_directory, encoding='UTF-8')
		if thread:
			thread.set_process(process)
			dbg('The process id is', thread.get_process_id())
	except pexpect.ExceptionPexpect as exception:
		dbg('Exception while executing', command)
		dbg('The tracekback is', traceback.format_exc())


########################################################################
# File Functions
########################################################################


def replace_text_in_file(filepath, search_text, replacement_text):
	dbg('Replace text in file', filepath)
	dbg('Search text', search_text)
	dbg('Replacement text', replacement_text)

	if not filepath:
		dbg('Cannot replace text', 'File not specified')
		return

	if not os.path.exists(filepath):
		dbg('File does not exist: ' + filepath)
		return

	if not search_text:
		dbg('Cannot replace text', 'Search text not specified')
		return

	if not replacement_text:
		dbg('Replacement text not specified')
		return

	with open(filepath, 'r+') as file:
		file_contents = file.read()
		file_contents = re.sub(search_text, replacement_text, file_contents)
		file.seek(0)
		file.truncate()
		file.write(file_contents)


def delete_file(filepath, thread):
	dbg('Delete file', filepath)
	if os.path.exists(filepath):
		command = 'rm -rf "%s"' % filepath
		result, error = exec_sync(command, thread)
		if error:
			dbg( 'deleting file ' + filepath, result)
	else:
		dbg('Path does not exist', filepath)


def get_directory_size(start_path):
	dbg('Calculate directory size')
	dbg('The directory is', start_path)

	total_size = 0
	for dirpath, dirnames, filenames in os.walk(start_path):
		for filename in filenames:
			filepath = os.path.join(dirpath, filename)
			total_size += os.path.getsize(filepath)

	dbg(total_size,nd=2)
	return total_size


def get_directory_for_file(filename, start_path):
	dbg(filename, start_path)

	directory = ''
	for dirpath, dirnames, filenames in os.walk(start_path):
		if filename in filenames: directory = dirpath

	if directory:
		dbg('success')
	else:
		dbg(' file is not in directory')

	dbg(directory,nd=2)
	return directory


########################################################################
# Miscelaneous Functions
########################################################################


def set_user_and_group(user_id, group_id):
	dbg(user_id, group_id)
	os.setgid(group_id)
	os.setuid(user_id)


def get_current_directory():
	current_directory = os.readlink('/proc/%i/cwd' % model.terminal_pid)
	dbg(current_directory,nd=2)
	return current_directory


def get_package_version(aname):
	command = 'dpkg-query --showformat="${Version}" --show "%s"' % aname
	result, error = exec_sync(command)
	dbg(result,nd=2)
	return result


########################################################################
# Configuration File Functions
########################################################################

def save_configuration():
	# Create the configuraton.
	configuration = configparser.ConfigParser(allow_no_value=True)
	configuration.optionxform = str
	# General
	configuration.add_section('Timbrel')
	configuration.set('Timbrel', 'Version', vars.app.version)
	# Original
	configuration.add_section('Original')
	configuration.set('Original','iso_filename',model.original_iso_filename)
	configuration.set('Original','iso_directory',model.original_iso_directory)
	configuration.set('Original','iso_volume_id',model.original_iso_volume_id)
	configuration.set('Original','iso_release_name',model.original_iso_release_name)
	configuration.set('Original','iso_disk_name',model.original_iso_disk_name)
	# Custom
	configuration.add_section('Custom')
	configuration.set('Custom','iso_version_number',model.custom_iso_version_number)
	configuration.set('Custom','iso_filename',model.custom_iso_filename)
	configuration.set('Custom','iso_dir',model.custom_iso_dir)
	configuration.set('Custom','iso_volume_id',	model.custom_iso_volume_id)
	configuration.set('Custom','iso_release_name',model.custom_iso_release_name)
	configuration.set('Custom','iso_disk_name',model.custom_iso_disk_name)
	configuration.set('Custom','iso_md5_filename',model.custom_iso_md5_filename)
	configuration.set('Custom','use_copied',str(vars.prj.use_copied))
	# Status
	configuration.add_section('Status')
	configuration.set('Status','is_success_copy_original_iso_files',
		str(model.is_success_copy_original_iso_files))
	configuration.set('Status','is_success_extract_squashfs',
		str(model.is_success_extract_squashfs))
	# BootConfigs
	configuration.add_section('BootConfigs')
	ndx=1
	checklist=[]
	for relpath in vars.prj.bootconfigs:
		# relpath=os.path.relpath(relpath,vars.prj.path.c_iso)
		path=os.path.join(vars.prj.path.c_iso,relpath)
		if os.path.isfile(path):
			if relpath in checklist:
				dbg('BootConfigs skipping dublicate',relpath)
				continue
			else:
				dbg('BootConfigs saving',relpath)
				configuration.set('BootConfigs',str(ndx),relpath)
				checklist.append(relpath)
			ndx=ndx+1
		else:
			dbg('BootConfigs skipping',relpath,'relpath',relpath)
			continue
	vars.prj.bootconfigs=checklist
	with open(vars.prj.path.cfg, 'w') as configuration_file:
		configuration.write(configuration_file)
	os.chown(vars.prj.path.cfg, model.user_id, model.group_id)


def save_configuration_value(section, key, value):
	# Create the configuration.
	configuration = configparser.ConfigParser(allow_no_value=True)
	configuration.optionxform = str

	# Set the section, key, and value.
	configuration.add_section(section)

	if type(value) is str:
		configuration.set(section, key, value)
	elif type(value) is bool:
		configuration.set(section, key, str(value))
	elif type(value) is tuple:
		configuration.set(section, key, ','.join(value))
	elif type(value) is list:
		configuration.set(section, key, ','.join(value))
	else:
		configuration.set(section, key, value)
	# Write the configuration file.
	with open(vars.prj.path.cfg, 'w') as configuration_file:
		configuration.write(configuration_file)


########################################################################
# ISO Mount Functions
########################################################################


def is_mounted(aisopath, amountpoint):
	dbg('ISO image', aisopath)
	dbg('Mount point', amountpoint)
	command = 'mount'
	result, error = exec_sync(command,silent=True)
	mounted = False
	if not error:
		mount_information = re.search(
			r'%s\s*on\s*%s' %
			(re.escape(aisopath),
			 re.escape(amountpoint)),
			result)
		mounted = bool(mount_information)
	dbg(mounted,nd=2)
	return mounted


def mount_iso_image(aisopath, aisomountpoint, athread):
	aisopath = os.path.realpath(aisopath)
	aisomountpoint = os.path.realpath(aisomountpoint)
	dbg('ISO image', aisopath)
	dbg('Mount point', aisomountpoint)
	if os.path.exists(aisomountpoint):
		dbg('Unmount existing mount point if it is mounted', aisomountpoint)
		command = 'umount "%s"' % aisomountpoint
		result, error = exec_sync(command, athread)
		if error:
			dbg('Unable to unmount', result)
	else:
		dbg('Create the mount point if it does not exist', aisomountpoint)
		command = 'mkdir "%s"' % aisomountpoint
		result, error = exec_sync(command, athread)
		if error:
			dbg('Error',result,nd=2)
	if os.path.exists(aisopath):
		dbg('Mount', aisopath)
		command = 'mount --options loop "%s" "%s"' % (
			aisopath,
			aisomountpoint)
		result, error = exec_sync(command, athread)
		if error:
			dbg('Error',result,nd=2)


def delete_iso_mount(iso_image_mount_point, thread):
	dbg(iso_image_mount_point)

	iso_image_mount_point = os.path.realpath(iso_image_mount_point)

	if os.path.exists(iso_image_mount_point):

		# Unmount existing mount point, just in case it is already
		# mounted.

		dbg('Unmount if it is mounted', iso_image_mount_point)
		command = 'umount "%s"' % iso_image_mount_point
		result, error = exec_sync(command, thread)
		if error:
			dbg( 'Unable to unmount', result)
		time.sleep(1.00)

		# Delete the mount point.
		dbg(iso_image_mount_point)
		command = 'rm -rf "%s"' % iso_image_mount_point
		result, error = exec_sync(command, thread)
		if error:
			dbg( 'Unable to delete', result)
		time.sleep(1.00)

	else:

		dbg('The mount point does not exist', 'Do nothing')


########################################################################
# Get Iso Image Information
########################################################################


def get_iso_image_volume_id(iso_image_filepath, thread):
	dbg('Get ISO image volume id')
	# Get the original ISO image volume id.
	iso_image_filepath = os.path.realpath(iso_image_filepath)
	command = 'isoinfo -d -i "%s"' % iso_image_filepath
	result, error = exec_sync(command, thread)
	iso_image_volume_id = 'Unknown iso image volume id'
	if not error:
		iso_image_volume_id = re.sub(
			r'.*Volume id:\s+(.*[^\n]).*Volume\s+set\s+id.*',
			r'\1',
			result,
			0,
			re.DOTALL)
	dbg(iso_image_volume_id,nd=2)
	return iso_image_volume_id


def get_iso_image_release_name(iso_image_mount_point, thread):
	dbg('Get ISO image release name')
	iso_image_mount_point = os.path.realpath(iso_image_mount_point)
	# Read the original ISO image README.diskdefines file.
	command = 'cat "%s"' % os.path.join(
		iso_image_mount_point,
		'README.diskdefines')
	result, error = exec_sync(command, thread)
	# Get the original ISO image release name.
	iso_image_release_name = 'Unknown iso image release name'
	if not error:
		iso_image_release_name_infromation = re.search(
			r'DISKNAME.*"(.*)"',
			result)
		if iso_image_release_name_infromation:
			iso_image_release_name = iso_image_release_name_infromation.group(
				1)
	dbg(iso_image_release_name,nd=2)
	return iso_image_release_name

def get_iso_image_disk_name(iso_image_mount_point, thread):
	dbg('Get ISO image disk name')
	iso_image_mount_point = os.path.realpath(iso_image_mount_point)
	# Read the original ISO image README.diskdefines file.
	command = 'cat "%s"' % os.path.join(
		iso_image_mount_point,
		'README.diskdefines')
	result, error = exec_sync(command, thread)
	# Get the original ISO image disk name.
	iso_image_disk_name = 'Unknown iso image disk name'
	if not error:
		iso_image_disk_name_information = re.search(r'DISKNAME *(.*)', result)
		if iso_image_disk_name_information:
			iso_image_disk_name = iso_image_disk_name_information.group(1)
	dbg(iso_image_disk_name,nd=2)
	return iso_image_disk_name

def get_casper_relative_directory(iso_image_mount_point, thread):
	dbg('Get ISO image disk name')
	iso_image_mount_point = os.path.realpath(iso_image_mount_point)
	# Read the original ISO image README.diskdefines file.
	command = 'cat "%s"' % os.path.join(
		iso_image_mount_point,
		'README.diskdefines')
	result, error = exec_sync(command, thread)
	# Get the original ISO image disk name.
	iso_image_disk_name = 'Unknown iso image disk name'
	if not error:
		iso_image_disk_name_information = re.search(r'DISKNAME *(.*)', result)
		if iso_image_disk_name_information:
			iso_image_disk_name = iso_image_disk_name_information.group(1)
	dbg(iso_image_disk_name,nd=2)
	return iso_image_disk_name


def get_casper_relative_directory(iso_image_mount_point):
	dbg('Get casper relative directory')
	casper_directory = get_directory_for_file(
		'filesystem.squashfs',
		iso_image_mount_point)
	casper_relative_directory = os.path.relpath(
		casper_directory,
		iso_image_mount_point)
	dbg(casper_relative_directory,nd=2)
	return casper_relative_directory


########################################################################
# Creators
########################################################################


def create_custom_iso_version_number():
	dbg('Create custom ISO version number')
	result=datetime.datetime.now().strftime('%Y.%m.%d')
	dbg(result,nd=2)
	return result

def get_path(adir):
	dbg('Getting full path')
	result=os.path.join(vars.prj.path.root, adir)
	dbg(result,nd=2)
	return result

def create_custom_iso_filename(original_iso_filename, custom_iso_version_number):
	dbg('Create custom ISO image filename')
	dbg(
		'The original ISO image filename is',
		original_iso_filename)
	dbg(
		'The custom ISO image version number is',
		custom_iso_version_number)

	if original_iso_filename:

		# original_iso_filename = re.sub('\.iso$', '',
		#                                      original_iso_filename)
		original_iso_filename = original_iso_filename[:-4]

		# original_iso_filename ◀ (text_a)(version)(text_b)
		search = r'(^.*)(\d{4}\.\d{2}\.\d{2})(.*$)'
		match = re.search(search, original_iso_filename)
		if match:
			# Version exists in original_iso_filename.
			text_a = match.group(1)
			version = match.group(2)
			text_b = match.group(3)
			dbg('text a', text_a)
			dbg('version', version)
			dbg('text b', text_b)
			# text_a ◀ (text_c)(release)(point_release)(text_d)
			search = r'(^.*?)(\d{2}\.\d{1,2})(\.\d{1,2}){0,1}(.*$)'
			match = re.search(search, text_a)
			if match:
				# Release exists in text_a.
				text_c = match.group(1)
				release = match.group(2)
				point_release = match.group(3)
				text_d = match.group(4)
				dbg('text c', text_c)
				dbg('release', release)
				dbg('point release', point_release)
				dbg('text d', text_d)
				if not point_release:
					point_release = '.0'
					dbg('new point_release', point_release)
					# text_a = text_c + release + point_release + text_d
					text_a = '%s%s%s%s' % (
						text_c,
						release,
						point_release,
						text_d)
					dbg('new text a', text_a)
			else:
				# text_b ◀ (text_c)(release)(point_release)(text_d)
				match = re.search(search, text_b)
				if match:
					# Release exists in text_b.
					text_c = match.group(1)
					release = match.group(2)
					point_release = match.group(3)
					text_d = match.group(4)
					dbg('text c', text_c)
					dbg('release', release)
					dbg('point release', point_release)
					dbg('text d', text_d)
					if not point_release:
						point_release = '.0'
						dbg('new point_release', point_release)
						# text_b = text_c + release + point_release + text_d
						text_b = '%s%s%s%s' % (
							text_c,
							release,
							point_release,
							text_d)
						dbg('new text b', text_b)
			# custom_iso_filename = text_a + custom_iso_version_number + text_b + '.iso'
			custom_iso_filename = '%s%s%s.iso' % (
				text_a,
				custom_iso_version_number,
				text_b)
		else:
			# original_volume_id ◀ (text_a)(release)(point_release)(text_b)
			search = r'(^.*?)(\d{2}\.\d{1,2})(\.\d{1,2}){0,1}(.*$)'
			match = re.search(search, original_iso_filename)
			if match:
				# Release exists in original_iso_filename.
				text_a = match.group(1)
				release = match.group(2)
				point_release = match.group(3)
				text_b = match.group(4)
				dbg('text a', text_a)
				dbg('release', release)
				dbg('point release', point_release)
				dbg('text b', text_b)
				if not point_release:
					point_release = '.0'
					dbg('new point_release', point_release)
				# custom_iso_filename = text_a + release + point_release + '-' + custom_iso_version_number + text_b + '.iso'
				custom_iso_filename = '%s%s%s-%s%s.iso' % (
					text_a,
					release,
					point_release,
					custom_iso_version_number,
					text_b)
			else:
				# custom_iso_filename = original_iso_filename + '-' + custom_iso_version_number + '.iso'
				custom_iso_filename = '%s-%s.iso' % (
					original_iso_filename,
					custom_iso_version_number)

		dbg(
			'The custom iso image filename is',
			custom_iso_filename)

	else:

		custom_iso_filename = None
		dbg(
			'The custom iso image filename is',
			custom_iso_filename)

	dbg(custom_iso_filename,nd=2)
	return custom_iso_filename


def create_custom_iso_volume_id(original_iso_volume_id, custom_iso_version_number):
	dbg('Create custom ISO image volume id')
	dbg(
		'The original ISO image volume id is',
		original_iso_volume_id)
	dbg(
		'The custom ISO image version number is',
		custom_iso_version_number)

	if original_iso_volume_id:

		# original_iso_volume_id ◀ (text_a)(version)(text_b)
		search = r'(^.*)(\d{4}\.\d{2}\.\d{2})(.*$)'
		match = re.search(search, original_iso_volume_id)
		if match:
			# Version exists in original_iso_volume_id.
			text_a = match.group(1)
			version = match.group(2)
			text_b = match.group(3)
			dbg('text a', text_a)
			dbg('version', version)
			dbg('text b', text_b)
			# text_a ◀ (text_c)(release)(point_release)(text_d)
			search = r'(^.*?)(\d{2}\.\d{1,2})(\.\d{1,2}){0,1}(.*$)'
			match = re.search(search, text_a)
			if match:
				# Release exists in text_a.
				text_c = match.group(1)
				release = match.group(2)
				point_release = match.group(3)
				text_d = match.group(4)
				dbg('text c', text_c)
				dbg('release', release)
				dbg('point release', point_release)
				dbg('text d', text_d)
				if not point_release:
					point_release = '.0'
					dbg('new point_release', point_release)
					# text_a = text_c + release + point_release + text_d
					text_a = '%s%s%s%s' % (
						text_c,
						release,
						point_release,
						text_d)
					dbg('new text a', text_a)
			else:
				# text_b ◀ (text_c)(release)(point_release)(text_d)
				match = re.search(search, text_b)
				if match:
					# Release exists in text_b.
					text_c = match.group(1)
					release = match.group(2)
					point_release = match.group(3)
					text_d = match.group(4)
					dbg('text c', text_c)
					dbg('release', release)
					dbg('point release', point_release)
					dbg('text d', text_d)
					if not point_release:
						point_release = '.0'
						dbg('new point_release', point_release)
						# text_b = text_c + release + point_release + text_d
						text_b = '%s%s%s%s' % (
							text_c,
							release,
							point_release,
							text_d)
						dbg('new text b', text_b)
			# custom_iso_volume_id = text_a + custom_iso_version_number + text_b
			custom_iso_volume_id = '%s%s%s' % (
				text_a,
				custom_iso_version_number,
				text_b)
		else:
			# original_volume_id ◀ (text_a)(release)(point_release)(text_b)
			search = r'(^.*?)(\d{2}\.\d{1,2})(\.\d{1,2}){0,1}(.*$)'
			match = re.search(search, original_iso_volume_id)
			if match:
				# Release exists in original_iso_volume_id.
				text_a = match.group(1)
				release = match.group(2)
				point_release = match.group(3)
				text_b = match.group(4)
				dbg('text a', text_a)
				dbg('release', release)
				dbg('point release', point_release)
				dbg('text b', text_b)
				if not point_release:
					point_release = '.0'
					dbg('new point_release', point_release)
				# custom_iso_volume_id = text_a + release + point_release + ' ' + custom_iso_version_number + text_b
				custom_iso_volume_id = '%s%s%s %s%s' % (
					text_a,
					release,
					point_release,
					custom_iso_version_number,
					text_b)
			else:
				# custom_iso_volume_id = original_iso_volume_id + ' ' + custom_iso_version_number
				custom_iso_volume_id = '%s %s' % (
					original_iso_volume_id,
					custom_iso_version_number)

		dbg(
			'The custom iso image volume id is',
			custom_iso_volume_id)

	else:

		custom_iso_volume_id = None
		dbg(
			'The custom iso image volume id is',
			custom_iso_volume_id)

	dbg(custom_iso_volume_id,nd=2)
	return custom_iso_volume_id


def create_custom_iso_release_name(original_iso_release_name):
	dbg('Create custom ISO image release name')

	try:
		custom_iso_release_name = 'Custom %s' % re.sub(
			r'^Custom\s*',
			'',
			original_iso_release_name)
	except Exception as exception:
		dbg(
			'Encountered exception while creating custom ISO image release name',
			exception)
		custom_iso_release_name = ''

	dbg(custom_iso_release_name,nd=2)
	return custom_iso_release_name


def create_custom_iso_disk_name(custom_iso_volume_id, custom_iso_release_name):
	dbg('Create custom ISO image disk name')

	if custom_iso_volume_id and custom_iso_release_name:
		custom_iso_disk_name = '%s "%s"' % (
			custom_iso_volume_id,
			custom_iso_release_name)
	elif custom_iso_volume_id:
		custom_iso_disk_name = custom_iso_volume_id
	elif custom_iso_release_name:
		custom_iso_disk_name = custom_iso_release_name
	else:
		custom_iso_disk_name = ''

	dbg(custom_iso_disk_name,nd=2)
	return custom_iso_disk_name


def create_custom_iso_md5_filename_ORIGINAL(custom_iso_filename):
	dbg('Create custom ISO image md5 filename')

	try:
		custom_iso_md5_filename = re.sub(
			r'\.iso$|\.$|$',
			'.md5',
			custom_iso_filename)
	except Exception as exception:
		dbg(
			'Encountered exception while creating custom ISO image md5 filename',
			exception)
		custom_iso_md5_filename = 'custom.md5'

	dbg(custom_iso_md5_filename,nd=2)
	return custom_iso_md5_filename


def create_custom_iso_md5_filename(custom_iso_filename):
	dbg('Create custom ISO image md5 filename')
	try:
		# filename_root = os.path.splitext(custom_iso_filename)[0]
		# filename_root = re.search(r'(.*?)\.iso*', custom_iso_filename).group(1)
		# filename_root = re.search(r'(.*?)(?:(?:\.iso)*)$', custom_iso_filename).group(1)
		custom_iso_md5_filename = '%s.md5' % custom_iso_filename[:
																			 -4]
	except Exception as exception:
		dbg(
			'Encountered exception while creating custom ISO image md5 filename',
			exception)
		custom_iso_md5_filename = 'custom.md5'
	dbg(custom_iso_md5_filename,nd=2)
	return custom_iso_md5_filename


########################################################################
# Copy Files Functions
########################################################################


def create_file_details_list():
	dbg('List files to copy')

	file_details_list = []

	for file_number, uri in enumerate(model.uris):
		filepath = unquote(urlparse(uri).path)
		file_details_list.append([0, filepath])

	dbg(file_details_list,nd=2)
	return file_details_list


def copy_files(thread):
	dbg('Copy file(s)')

	total_files = len(model.uris)
	current_directory = get_current_directory()
	relative_directory = os.path.join(
		'/',
		os.path.relpath(current_directory,vars.prj.path.c_squash))

	dbg('The current directory is', current_directory)
	dbg(
		'The custom squashfs directory is',
		vars.prj.path.c_squash)
	dbg(
		'The real custom squashfs directory is',
		vars.prj.path.c_squash)
	dbg('The relative current directory is', relative_directory)

	for file_number, uri in enumerate(model.uris):

		filepath = unquote(urlparse(uri).path)

		if total_files == 1:
			label = 'Copying one file to %s' % relative_directory
		else:
			label = 'Copying file %s of %s to %s' % (
				file_number + 1,
				total_files,
				relative_directory)

		ui.update_label('copy_files_page__progress_label', label)
		ui.scroll_to_tree_view_row(
			'copy_files_page__treeview',
			file_number)

		copy_file(
			filepath,
			file_number,
			current_directory,
			total_files,
			thread)

		time.sleep(0.10)


def copy_file(filepath, file_number, directory, total_files, thread):
	dbg(
		'Copy file',
		'Number %s of %s' % (file_number + 1,
							 total_files))
	dbg('The file is', filepath)
	dbg('The target directory is', directory)

	command = 'rsync --archive --info=progress2 "%s" "%s"' % (
		filepath,
		directory)

	current_time = datetime.datetime.now()
	# formatted_time = '{:%Y-%m-%d %I:%M:%S.%f %p}'.format(current_time)
	formatted_time = '{:%H:%M:%S.%f}'.format(current_time)

	exec_async(command, thread)
	dbg('The start time is', formatted_time)

	progress_initial_global = int(round(100 * file_number / total_files, 0))

	progress_initial = 0
	progress_target = 100
	progress_display = progress_initial - 1
	progress_current = progress_initial

	pulse = max(0.01 / total_files, .001)
	pause = max(1 / total_files, .01)

	while (thread.process.exitstatus is
		   None) or (progress_display < progress_target
					 and not thread.process.exitstatus):

		try:
			line = thread.process.read_nonblocking(100, 0.05)
			result = re.search(r'([0-9]{1,3})%', str(line))
			if result:
				progress_current = progress_initial + int(result.group(1))
		except pexpect.TIMEOUT:
			pass
		except pexpect.EOF:
			if progress_current < progress_target:
				progress_current = progress_target
			time.sleep(pulse)

		if progress_current > progress_display:
			progress_display += 1
			if progress_display % 10 == 0:
				dbg('Completed', '%i%%' % progress_display)
			if progress_display == 0:
				ui.update_progressbar_text(
					'copy_files_page__copy_files_progressbar',
					None)
				ui.update_liststore_progressbar_percent(
					'copy_files_page__file_details__liststore',
					file_number,
					0)
			ui.update_progressbar_percent(
				'copy_files_page__copy_files_progressbar',
				progress_initial_global + progress_display / total_files)
			ui.update_liststore_progressbar_percent(
				'copy_files_page__file_details__liststore',
				file_number,
				progress_display)

	current_time = datetime.datetime.now()
	# formatted_time = '{:%Y-%m-%d %I:%M:%S.%f %p}'.format(current_time)
	formatted_time = '{:%H:%M:%S.%f}'.format(current_time)
	dbg('The end time is', formatted_time)

	time.sleep(pause)


########################################################################
# Enter Chroot Environment Functions
########################################################################


def copy_original_iso_files(thread):
	source_path = vars.prj.path.o_iso_m
	dbg('The source path is', source_path)
	target_path = vars.prj.path.c_iso
	dbg('The target path is', target_path)
	filename = 'filesystem.manifest-remove'
	is_exists_filesystem_manifest_typical_remove = is_exists_filesystem_manifest_remove(
		filename)
	dbg(
		'Include %s?' % filename,
		not is_exists_filesystem_manifest_typical_remove)

	filename = 'filesystem.manifest-minimal-remove'
	is_exists_filesystem_manifest_minimal_remove = is_exists_filesystem_manifest_remove(
		filename)
	dbg(
		'Include %s?' % filename,
		not is_exists_filesystem_manifest_minimal_remove)

	if not is_exists_filesystem_manifest_typical_remove and not is_exists_filesystem_manifest_minimal_remove:
		# Copy all files from the original iso.
		# Exclude or copy the following files as indicated.
		# Do not copy: /md5sum.txt
		# Do not copy: /casper/filesystem.manifest
		# ****** Copy: /casper/filesystem.manifest-remove
		# ****** Copy: /casper/filesystem.manifest-minimal-remove
		# Do not copy: /casper/filesystem.size
		# Do not copy: /casper/filesystem.squashfs
		# Do not copy: /casper/filesystem.squashfs.gpg
		# Do not copy: /casper/initrd.lz
		# Do not copy: /casper/vmlinuz.efi

		command = (
			'rsync'
			' --delete'
			' --archive'
			' --exclude="md5sum.txt"'
			' --exclude="/%s/filesystem.squashfs"'
			' --exclude="/%s/filesystem.squashfs.gpg"'
			' --progress "%s" "%s"' % (
				model.casper_relative_directory,
				model.casper_relative_directory,
				source_path,
				target_path))
	elif is_exists_filesystem_manifest_typical_remove and not is_exists_filesystem_manifest_minimal_remove:
		# Copy all files from the original iso.
		# Exclude or copy the following files as indicated.
		# Do not copy: /md5sum.txt
		# Do not copy: /casper/filesystem.manifest
		# Do not Copy: /casper/filesystem.manifest-remove
		# ****** Copy: /casper/filesystem.manifest-minimal-remove
		# Do not copy: /casper/filesystem.size
		# Do not copy: /casper/filesystem.squashfs
		# Do not copy: /casper/filesystem.squashfs.gpg
		# Do not copy: /casper/initrd.lz
		# Do not copy: /casper/vmlinuz.efi

		command = (
			'rsync'
			' --delete'
			' --archive'
			' --exclude="md5sum.txt"'
			' --exclude="/%s/filesystem.squashfs"'
			' --exclude="/%s/filesystem.squashfs.gpg"'
			' --exclude="/%s/filesystem.manifest-remove"'
			# ' --exclude="/%s/filesystem.manifest-minimal-remove"'
			' --progress "%s" "%s"' % (
				model.casper_relative_directory,
				model.casper_relative_directory,
				model.casper_relative_directory,
				source_path,
				target_path))
	elif not is_exists_filesystem_manifest_typical_remove and is_exists_filesystem_manifest_minimal_remove:
		# Copy all files from the original iso.
		# Exclude or copy the following files as indicated.
		# Do not copy: /md5sum.txt
		# Do not copy: /casper/filesystem.manifest
		# ****** Copy: /casper/filesystem.manifest-remove
		# Do not Copy: /casper/filesystem.manifest-minimal-remove
		# Do not copy: /casper/filesystem.size
		# Do not copy: /casper/filesystem.squashfs
		# Do not copy: /casper/filesystem.squashfs.gpg
		# Do not copy: /casper/initrd.lz
		# Do not copy: /casper/vmlinuz.efi

		command = (
			'rsync'
			' --delete'
			' --archive'
			' --exclude="md5sum.txt"'
			' --exclude="/%s/filesystem.squashfs"'
			' --exclude="/%s/filesystem.squashfs.gpg"'
			# ' --exclude="/%s/filesystem.manifest-remove"'
			' --exclude="/%s/filesystem.manifest-minimal-remove"'
			' --progress "%s" "%s"' % (
				model.casper_relative_directory,
				model.casper_relative_directory,
				source_path,
				target_path))
	else:
		command = (
			'rsync'
			' --delete'
			' --archive'
			' --exclude="md5sum.txt"'
			' --exclude="/%s/filesystem.squashfs"'
			' --exclude="/%s/filesystem.squashfs.gpg"'
			' --progress "%s" "%s"' % (
				model.casper_relative_directory,
				model.casper_relative_directory,
				source_path,
				target_path))

	current_time = datetime.datetime.now()
	# formatted_time = '{:%Y-%m-%d %I:%M:%S.%f %p}'.format(current_time)
	formatted_time = '{:%H:%M:%S.%f}'.format(current_time)

	exec_async(command, thread)
	dbg('The start time is', formatted_time)

	progress_initial = 0
	progress_target = 100
	progress_display = progress_initial - 1
	progress_current = progress_initial

	while (thread.process.exitstatus is
		   None) or (progress_display < progress_target
					 and not thread.process.exitstatus):

		try:
			line = thread.process.read_nonblocking(100, 0.05)
			result = re.search(r'([0-9]{1,3})%', str(line))
			if result:
				progress_current = progress_initial + int(result.group(1))
		except pexpect.TIMEOUT:
			pass
		except pexpect.EOF:
			if progress_current < progress_target:
				progress_current = progress_target
			time.sleep(0.05)

		if progress_current > progress_display:
			progress_display += 1
			if progress_display % 10 == 0:
				dbg('Completed', '%i%%' % progress_display)
			if progress_display == 0:
				ui.update_progressbar_text(
					'unsquashfs_page__copy_original_iso_files_progressbar',
					None)
			ui.update_progressbar_percent(
				'unsquashfs_page__copy_original_iso_files_progressbar',
				progress_display)

	current_time = datetime.datetime.now()
	# formatted_time = '{:%Y-%m-%d %I:%M:%S.%f %p}'.format(current_time)
	formatted_time = '{:%H:%M:%S.%f}'.format(current_time)
	dbg('The end time is', formatted_time)

	time.sleep(0.10)

	# TODO: Set return value based on rsync result.
	return True


def unsquashfs(thread):
	target_path = vars.prj.path.c_squash
	dbg('Extracting to', target_path)
	# Delete custom squashfs directory, if it exists.
	delete_file(target_path, thread)
	source_path = os.path.join(
		vars.prj.path.o_iso_m,
		model.casper_relative_directory,
		'filesystem.squashfs')
	dbg('The source path is', source_path)
	command = 'unsquashfs -force -dest "%s" "%s"' % (target_path, source_path)
	current_time = datetime.datetime.now()
	formatted_time = '{:%H:%M:%S.%f}'.format(current_time)
	exec_async(command, thread)
	dbg('The start time is', formatted_time)
	progress_initial = 0
	progress_target = 100
	progress_display = progress_initial - 1
	progress_current = progress_initial
	while (thread.process.exitstatus is
		   None) or (progress_display < progress_target
					 and not thread.process.exitstatus):
		try:
			line = thread.process.read_nonblocking(100, 0.05)
			result = re.search(r'([0-9]{1,3})%', str(line))
			if result:
				progress_current = progress_initial + int(result.group(1))
		except pexpect.TIMEOUT:
			pass
		except pexpect.EOF:
			if progress_current < progress_target:
				progress_current = progress_target
			time.sleep(0.05)
		if progress_current > progress_display:
			progress_display += 1
			if progress_display % 10 == 0:
				dbg('Completed', '%i%%' % progress_display)
			if progress_display == 0:
				ui.update_progressbar_text(
					'unsquashfs_page__unsquashfs_progressbar',
					None)
			ui.update_progressbar_percent(
				'unsquashfs_page__unsquashfs_progressbar',
				progress_display)

	current_time = datetime.datetime.now()
	formatted_time = '{:%H:%M:%S.%f}'.format(current_time)
	dbg('The end time is', formatted_time)

	time.sleep(0.10)

	return True



# Vte Terminal Information
# https://lazka.github.io/pgi-docs/#Vte-2.91/classes/Terminal.html
# https://help.ubuntu.com/community/BasicChroot
def prepare_chroot_environment(thread):
	command = 'mount --bind /dev "%s"' % os.path.join(
		vars.prj.path.c_squash,
		'dev')
	result, error = exec_sync(command, thread)
	if error: dbg('Error mounting /dev', result)

	command = 'mount --bind /run "%s"' % os.path.join(
		vars.prj.path.c_squash,
		'run')
	result, error = exec_sync(command, thread)
	if error: dbg('Error mounting /run', result)
	command = 'mount --types proc proc "%s"' % os.path.join(vars.prj.path.c_squash,'proc')
	result, error = exec_sync(command, thread)
	if error: dbg('Error mounting /proc', result)
	command = 'mount --types sysfs sys "%s"' % os.path.join(vars.prj.path.c_squash,'sys')
	result, error = exec_sync(command, thread)
	if error: dbg('Error mounting /sys', result)
	command = 'mount --types devpts pts "%s"' % os.path.join(vars.prj.path.c_squash,'dev','pts')
	result, error = exec_sync(command, thread)
	if error: dbg('Error mounting /dev/pts', result)
	try:
		dbg('Change terminal prompt colors (modify .bashrc)')
		# Make a backup of .bashrc
		source_filepath = os.path.join(
			vars.prj.path.c_squash,
			'root',
			'.bashrc')
		target_filepath = os.path.join(
			vars.prj.path.c_squash,
			'root',
			'.bashrc.bak')
		dbg('Backup', source_filepath)
		dbg('To', target_filepath)
		copyfile(source_filepath, target_filepath)
		# Change terminal prompt colors.
		filepath = os.path.join(vars.prj.path.c_squash, 'root', '.bashrc')
		replace_text_in_file(
			filepath,
			'#force_color_prompt=yes',
			'force_color_prompt=yes')
		replace_text_in_file(filepath, '01;32', '00;35')
		replace_text_in_file(filepath, '01;34', '00;36')
		replace_text_in_file(filepath, r']\\\$', r'] \$')
	except Exception as exception:
		dbg(
			'Ignoring exception while changing terminal prompt colors (modifying .bashrc)',
			exception)

def enter_chroot_environment(*args):
	try:
		dbg()
		dbg(BOLD_YELLOW + 'Enter chroot environment.' + NORMAL)
		dbg()
		os.chroot(vars.prj.path.c_squash)
	except Exception as exception:
		dbg(BOLD_RED + 'Error entering chroot environment' + NORMAL)


# Vte Terminal Information
# https://lazka.github.io/pgi-docs/#Vte-2.91/classes/Terminal.html
# https://web.archive.org/web/20170311221231/https://lazka.github.io/pgi-docs/Vte-2.91/classes/Terminal.html#Vte.Terminal.feed_child
def create_chroot_terminal(thread):
	dbg('Create chroot environment')
	dbg(
		'The chroot environment directory is',
		vars.prj.path.c_squash)
	terminal = vars.app.builder.get_object('terminal_page__terminal')
	terminal.reset(True, False)
	settings = Gio.Settings('org.gnome.desktop.interface')
	font_name = settings.get_string('monospace-font-name')
	font = Pango.FontDescription(font_name)
	terminal.set_font(font)
	# terminal.set_scrollback_lines(-1)

	flags = Gtk.DestDefaults.MOTION | Gtk.DestDefaults.HIGHLIGHT | Gtk.DestDefaults.DROP
	# TODO: Change: Gtk.TargetFlags
	#       See: https://lazka.github.io/pgi-docs/Gtk-3.0/structs/TargetEntry.html#methods
	targets = [
		Gtk.TargetEntry.new('text/uri-list',
							0,
							80),
		Gtk.TargetEntry.new('text/plain',
							0,
							80)
	]
	actions = Gdk.DragAction.COPY
	terminal.drag_dest_set(flags, targets, actions)

	pty_flags = PtyFlags.DEFAULT
	working_directory = os.path.join(vars.prj.path.c_squash, 'root')
	terminal_argument_vector = ['/bin/bash', '--login']
	environment_variables = [
		'HOME=/root',
		'DISPLAY=%s' % (os.environ['DISPLAY'])
	]
	# spawn_flags = GLib.SpawnFlags.DO_NOT_REAP_CHILD
	spawn_flags = GLib.SpawnFlags.DEFAULT
	terminal_setup_data = thread

	# https://lazka.github.io/pgi-docs/#Vte-2.90/classes/Terminal.html
	# https://lazka.github.io/pgi-docs/#Vte-2.91/classes/Terminal.html
	# https://developer.gnome.org/vte/unstable/VteTerminal.html#vte-terminal-spawn-async

	# TODO: spawn_sync is deprecated, use spawn_async

	try:

		terminal.spawn_sync

	except AttributeError:

		# Ubuntu 14.04 uses libvte-2.90
		# For Vte 2.90 use fork_command_full()...
		# terminal_pid = int(terminal.fork_command_full(pty_flags, dbg('Creating a terminal widget using function', 'fork_command_full(); libvte-2.90+; Ubuntu 14.04+')
		terminal_pid = int(
			terminal.fork_command_full(
				pty_flags,
				working_directory,
				terminal_argument_vector,
				environment_variables,
				spawn_flags,
				enter_chroot_environment,
				terminal_setup_data)[1])
		model.set_terminal_pid(terminal_pid)

		# # Clear the terminal.
		# # Work-around for Bug #1550003
		# # In Ubuntu 14.04, the first few lines of text in in the Terminal are not visible.
		# # See https://bugs.launchpad.net/cubic/+bug/1779015
		# text = 'clear'
		# send_command_to_terminal(text)

	else:

		# Ubuntu 15.04 uses libvte-2.91
		# For Vte 2.91 use spawn_sync()...
		# terminal_pid = int(terminal.spawn_sync(pty_flags, dbg('Creating a terminal widget using function', 'spawn_sync(); libvte-2.91+; Ubuntu 15.04+')
		terminal_pid = int(
			terminal.spawn_sync(
				pty_flags,
				working_directory,
				terminal_argument_vector,
				environment_variables,
				spawn_flags,
				enter_chroot_environment,
				terminal_setup_data)[1])
		model.set_terminal_pid(terminal_pid)

		# # Clear the terminal.
		# # Work-around for Bug in Ubuntu 15.04+, where the bottom of the terminal appears shaded.
		# # See https://bugs.launchpad.net/cubic/+bug/1779015
		# text = 'clear'
		# send_command_to_terminal(text)

	# Enable input to the terminal.
	# terminal.set_input_enabled(True)
	ui.set_sensitive('terminal_page__terminal', True)


def check_chroot(thread):
	dbg('Check chroot environment')
	dbg('The terminal pid is', model.terminal_pid)
	dbg(
		'The custom squashfs directory is',
		vars.prj.path.c_squash)
	terminal_root_directory = None
	try:
		terminal_root_directory = os.readlink(
			'/proc/%s/root' % model.terminal_pid)
	except FileNotFoundError as exception:
		pass
	dbg(
		'The terminal\'s root directory is',
		terminal_root_directory)
	is_chroot = (terminal_root_directory == vars.prj.path.c_squash)
	dbg(is_chroot,nd=2)
	return is_chroot


def initialize_chroot_environment(thread):
	is_chroot = check_chroot(thread)

	# Note:
	#
	# The message "mesg: ttyname failed: No such device" apears just before
	# the status is printed to the chroot terminal. This is probably bug in
	# the .profile file.
	#
	# The cause of the problem is that the line "mesg n || true" in .profile
	# runs every time bash is executed, even when it is run from a session
	# without a tty device.
	#
	# Solution 1:
	# In file... ".profile"
	# Replace... mesg n || true
	# With...... if [[ $( tty ) != "not a tty" ]]; then mesg n || true; fi

	# Solution 1 Command:
	# $ sed -i 's/^mesg n || true$/if [[ $( tty ) != \"not a tty\" ]]; then mesg n || true; fi/g' .profile
	#
	# Solution 2:
	# In file... ".profile"
	# Replace... mesg n || true
	# With...... (tty > /dev/null) && (mesg n || true)
	#
	# Solution 2 Command:
	# $ sed -i 's/^mesg n || true$/(tty > /dev/null) && (mesg n || true)/g' .profile
	#
	# Reference:
	# https://bugs.launchpad.net/cubic/+bug/1779675/comments/4
	# https://superuser.com/questions/1241548/xubuntu-16-04-ttyname-failed-inappropriate-ioctl-for-device

	if is_chroot:

		# Notify user that we are in chroot.
		text = BOLD_GREEN + 'You are in the chroot environment.' + NORMAL
		send_message_to_terminal(text)

		# Divert initctl.
		text = 'dpkg-divert --local --rename --add /sbin/initctl'
		send_command_to_terminal(text)

		# Enter a new line.
		text = ''
		send_command_to_terminal(text)

	else:

		# Notify user that we are not in chroot.
		text = BOLD_RED + 'WARNING! You are in NOT the chroot environment. Exiting.' + NORMAL
		send_message_to_terminal(text)

		# If not in chroot, exit the terminal. Once the terminal exits,
		# the function handlers.on_child_exited__terminal_page() will be
		# invoked.
		text = 'exit'
		send_command_to_terminal(text)

		# Enter a new line.
		send_command_to_terminal()

	# Restore .bashrc. This file was changed in function
	# prepare_chroot_environment(thread)
	try:
		dbg('Restore terminal prompt colors (restore .bashrc)')
		source_filepath = os.path.join(
			vars.prj.path.c_squash,
			'root',
			'.bashrc.bak')
		target_filepath = os.path.join(
			vars.prj.path.c_squash,
			'root',
			'.bashrc')
		dbg('Restore', target_filepath)
		dbg('From', source_filepath)
		move(source_filepath, target_filepath)
	# except IOError as exception:
	except Exception as exception:
		dbg(
			'Ignoring exception while restoring terminal prompt colors (restoring .bashrc)',
			exception)


# Terminal.feed() may require one or two arguments:
# - feed(text, length)
# - feed(bytes)
# After executing Terminal.feed(), it is necessary to execute
# Terminal.feed_child() to print a new line.
#
# However, Terminal.feed_child() may also require one or two arguments:
# - feed_child(text, length)
# - feed_child(bytes)
#
# The approach used here allows any combination of Terminal.feed(), with one or
# two arguments, and Terminal.feed_child(), with one or two arguments.
def send_message_to_terminal(text=None):
	terminal = vars.app.builder.get_object('terminal_page__terminal')

	try:
		# Using Vte.Terminal 2.90 or 2.91
		# Print the message.
		if text: terminal.feed(text + '\n', -1)
		# Print a new line. (This is necessary).
		send_command_to_terminal()
		dbg('Send text to terminal', text)
	except TypeError:
		# Using Vte.Terminal "new" 2.91
		# Print the message.
		if text: terminal.feed(bytes(text + '\n', encoding='utf-8'))
		# Print a new line. (This is necessary).
		send_command_to_terminal()
		dbg('Send bytes to terminal', text)
	# This seems necessary to avoid a race condition in Vte.Terminal.
	time.sleep(0.1)


# Terminal.feed_child() may require one or two arguments:
# - feed_child(text, length)
# - feed_child(bytes)
# If text is none, a new line is sent to the terminal.
def send_command_to_terminal(text=None):
	terminal = vars.app.builder.get_object('terminal_page__terminal')

	try:
		# Using Vte.Terminal 2.90 or 2.91
		if text: terminal.feed_child(text + '\n', -1)
		else: terminal.feed_child('\n', -1)
		dbg('Send text to terminal', text)
	except TypeError:
		# Using Vte.Terminal "new" 2.91
		if text: terminal.feed_child(bytes(text + '\n', encoding='utf-8'))
		else: terminal.feed_child(bytes('\n', encoding='utf-8'))
		dbg('Send bytes to terminal', text)
	# This seems necessary to avoid a race condition in Vte.Terminal.
	time.sleep(0.1)


# Terminal.feed_child() may require one or two arguments:
# - feed_child(text, length)
# - feed_child(bytes)
# Text is sent to the terminal without appending a new line.
def send_text_to_terminal(text):
	terminal = vars.app.builder.get_object('terminal_page__terminal')

	try:
		# Using Vte.Terminal 2.90 or 2.91
		terminal.feed_child(text, -1)
		dbg('Send text to terminal', text)
	except TypeError:
		# Using Vte.Terminal "new" 2.91
		terminal.feed_child(bytes(text, encoding='utf-8'))
		dbg('Send bytes to terminal', text)
	# This seems necessary to avoid a race condition in Vte.Terminal.
	time.sleep(0.1)


########################################################################
# Exit Chroot Environment Functions
########################################################################


def get_processes_using_directory(directory):
	directory = os.path.realpath(directory)
	dbg('Get processes that are using directory', directory)

	pids = []
	symlinks = glob.glob('/proc/*/root')
	for symlink in symlinks:
		try:
			target = os.readlink(symlink)
			if directory in target:
				pid = int(re.search(r'.*/(\d*)/.*', symlink).group(1))
				pids.append(pid)
		except FileNotFoundError as exception:
			pass

	if pids:
		dbg(
			'%i processes were found that are using directory' % len(pids),
			directory)
		dbg('%s' % pids)
		# for index, pid in enumerate(pids):
		#     dbg('% 2i. %s' % (index+1, pid))
	else:
		dbg(
			'No processes were found that are using directory',
			directory)

	dbg(pids,nd=2)
	return pids


def kill_processes(pids, exclude_pid=None):
	while pids:
		pid = pids.pop()
		if pid != exclude_pid:
			dbg('Killing chroot process with pid', pid)
			os.kill(pid, signal.SIGKILL)
		else:
			dbg(
				'Not killing excluded chroot process with pid',
				pid)


def get_mount_points_in_directory(directory, thread):
	# directory = os.path.realpath(directory)
	dbg('Get mount points in directory', directory)

	mount_points = None
	command = 'mount'
	result, error = exec_sync(command, thread)
	mount_points = None
	if not error:
		mount_points = re.findall(r'.*on\s(%s.*)\stype' % directory, result)
	else:
		# dbg(
		#     'Unable to get mount points in directory %s' % directory, result)
		dbg('Unable to get mount points in', directory)

	if mount_points:
		dbg(
			'%i mount points were found in directory' % len(mount_points),
			directory)
		for index, mount_point in enumerate(mount_points):
			dbg('Mount point % 2i' % (index + 1), mount_point)
	else:
		# dbg(
		#     'No mount points were found in directory %s' % directory, result)
		dbg('No mount points were found in', directory)

	dbg(mount_points,nd=2)
	return mount_points


def unmount_mount_points(mount_points):
	dbg('Unmount mount points')
	while mount_points:
		mount_point = mount_points.pop()
		mount_point = os.path.realpath(mount_point)
		dbg('Unmount', mount_point)
		command = 'umount "%s"' % mount_point
		result, error = exec_sync(command)
		if not error:
			dbg('Successfully unmounted %s' % mount_point, result)
		else:
			dbg('Unable to unmount %s' % mount_point, result)


def exit_chroot_environment(thread):
	# http://askubuntu.com/questions/162319/how-do-i-stop-all-processes-in-a-chroot

	dbg('Exit chroot environment')

	terminal = vars.app.builder.get_object('terminal_page__terminal')

	# Disable input to the terminal.
	# terminal.set_input_enabled(False)
	ui.set_sensitive('terminal_page__terminal', False)

	# Do we need this? Should this be done from outside the chroot environmant?
	# Remove file /sbin/initctl.
	# See https://bugs.launchpad.net/cubic/+bug/1779015
	text = 'rm -f /sbin/initctl'
	try:
		# Using Vte.Terminal 2.90 or 2.91
		terminal.feed_child(text, -1)
		terminal.feed_child('\n', -1)
		dbg('Send text to terminal', text)
	except TypeError:
		# Using Vte.Terminal "new" 2.91
		data = bytes(text, encoding='utf-8')
		terminal.feed_child(data)
		terminal.feed_child(bytes('\n', encoding='utf-8'))
		dbg('Send bytes to terminal', data)
	# This seems necessary to avoid a race condition in Vte.Terminal.
	time.sleep(0.1)

	# Remove initctl diversion.
	# See https://bugs.launchpad.net/cubic/+bug/1779015
	text = 'dpkg-divert --rename --remove /sbin/initctl'
	try:
		# Using Vte.Terminal 2.90 or 2.91
		terminal.feed_child(text, -1)
		terminal.feed_child('\n', -1)
		dbg('Send text to terminal', text)
	except TypeError:
		# Using Vte.Terminal "new" 2.91
		data = bytes(text, encoding='utf-8')
		terminal.feed_child(data)
		terminal.feed_child(bytes('\n', encoding='utf-8'))
		dbg('Send bytes to terminal', data)
	# This seems necessary to avoid a race condition in Vte.Terminal.
	time.sleep(0.1)

	# Exit the terminal.
	# See https://bugs.launchpad.net/cubic/+bug/1779015
	text = 'exit'
	try:
		# Using Vte.Terminal 2.90 or 2.91
		terminal.feed_child(text, -1)
		terminal.feed_child('\n', -1)
		dbg('Send text to terminal', text)
	except TypeError:
		# Using Vte.Terminal "new" 2.91
		data = bytes(text, encoding='utf-8')
		terminal.feed_child(data)
		terminal.feed_child(bytes('\n', encoding='utf-8'))
		dbg('Send bytes to terminal', data)
	# This seems necessary to avoid a race condition in Vte.Terminal.
	time.sleep(0.1)

	#
	# Attempt to remove all chroot mounts.
	#

	# Attempt 1
	chroot_mount_points = get_mount_points_in_directory(
		vars.prj.path.c_squash,
		thread)
	if chroot_mount_points: unmount_mount_points(chroot_mount_points)

	# Attempt 2
	chroot_mount_points = get_mount_points_in_directory(
		vars.prj.path.c_squash,
		thread)
	if chroot_mount_points: unmount_mount_points(chroot_mount_points)

	#
	# Attempt to kill remaining processes and remove rmaining chroot
	# mounts.
	#

	# Attempt 1
	chroot_processes = get_processes_using_directory(vars.prj.path.c_squash)
	if chroot_processes: kill_processes(chroot_processes, model.terminal_pid)
	chroot_mount_points = get_mount_points_in_directory(
		vars.prj.path.c_squash,
		thread)
	if chroot_mount_points: unmount_mount_points(chroot_mount_points)

	# Attempt 2
	chroot_processes = get_processes_using_directory(vars.prj.path.c_squash)
	if chroot_processes: kill_processes(chroot_processes, model.terminal_pid)
	chroot_mount_points = get_mount_points_in_directory(
		vars.prj.path.c_squash,
		thread)
	if chroot_mount_points: unmount_mount_points(chroot_mount_points)

	#
	# Check if all chroot processes were killed.
	#
	chroot_processes = get_processes_using_directory(vars.prj.path.c_squash)
	if chroot_processes:
		dbg('Error killing all chroot processes')
		for index, process in enumerate(chroot_processes):
			dbg('Process % 2i.' % (index + 1), process)

	#
	# Check if all chroot mount points were unmounted.
	#
	chroot_mount_points = get_mount_points_in_directory(
		vars.prj.path.c_squash,
		thread)
	if chroot_mount_points:
		dbg('Error unmounting all chroot mount points')
		for index, mount_point in enumerate(chroot_mount_points):
			dbg('Mount point % 2i' % (index + 1), mount_point)

########################################################################
# Manage Linux Kernels Functions
########################################################################

# ------------------------------------------------------------------------------
# Kernel Versions
# ------------------------------------------------------------------------------


def create_kernel_details_list(*directories):
	dbg('Create kernel details list')

	# Create a consolidated kernel details list.
	kernel_details_list = []
	for directory in directories:
		directory = os.path.realpath(directory)
		update_kernel_details_list_for_vmlinuz(directory, kernel_details_list)
		update_kernel_details_list_for_initrd(directory, kernel_details_list)

	# The resulting kernel_details is:
	# 0: version_integers
	# 1: version_name
	# 2: vmlinuz_filename
	# 3: new_vmlinuz_filename
	# 4: initrd_filename
	# 5: new_initrd_filename
	# 6: directory
	# 7: note
	# 8: is_selected
	# 9: is_remove

	# For debugging.
	# print_kernel_details_list(kernel_details_list)

	# total = len(kernel_details_list)
	# for index, kernel_details in enumerate(kernel_details_list):
	#     dbg('version', kernel_details[0])
	#     dbg('version', kernel_details[1])
	#     dbg('Index', '%s of %s' % (index, total - 1))
	#     dbg('Vmlinuz filename', kernel_details[2])
	#     dbg('New vmlinuz filename', kernel_details[3])
	#     dbg('Initrd filename', kernel_details[4])
	#     dbg('New initrd filename', kernel_details[5])
	#     dbg('Directory', kernel_details[6])
	#     dbg('Note', kernel_details[7])
	#     dbg('Is selected', kernel_details[8])
	#     # dbg('Is remove', kernel_details[9]

	# Remove kernels that do not have both vmlinuz filename and initrd filename.
	kernel_details_list = [
		kernel_details for kernel_details in kernel_details_list
		if kernel_details[2] and kernel_details[4]
	]

	# For debugging.
	# print_kernel_details_list(kernel_details_list)

	# total = len(kernel_details_list)
	# for index, kernel_details in enumerate(kernel_details_list):
	#     dbg('version', kernel_details[0])
	#     dbg('version', kernel_details[1])
	#     dbg('Index', '%s of %s' % (index, total - 1))
	#     dbg('Vmlinuz filename', kernel_details[2])
	#     dbg('New vmlinuz filename', kernel_details[3])
	#     dbg('Initrd filename', kernel_details[4])
	#     dbg('New initrd filename', kernel_details[5])
	#     dbg('Directory', kernel_details[6])
	#     dbg('Note', kernel_details[7])
	#     dbg('Is selected', kernel_details[8])
	#     # dbg('Is remove', kernel_details[9]

	# Reverse sort the kernel details list by kernel version number (1st column).
	kernel_details_list.sort(
		key=lambda list: ['' if value is None else value for value in list],
		reverse=True)

	# Set the new vmlinuz filename.
	# Set the new initrd filename.
	for kernel_details in kernel_details_list:
		directory = kernel_details[6]
		vmlinuz_filename = kernel_details[2]
		vmlinuz_filepath = os.path.join(directory, vmlinuz_filename)
		new_vmlinuz_filename = calculate_vmlinuz_filename(vmlinuz_filepath)
		kernel_details[3] = new_vmlinuz_filename
		initrd_filename = kernel_details[4]
		initrd_filepath = os.path.join(directory, initrd_filename)
		new_initrd_filename = calculate_initrd_filename(initrd_filepath)
		kernel_details[5] = new_initrd_filename

	# Set the selected index as the index of the most recent kernel.
	selected_index = 0

	# Set the notes, and update the selected index if necessary.
	current_kernel_release_name = get_current_kernel_release_name()
	current_kernel_version_name = get_current_kernel_version_name()
	original_iso_directory = os.path.join(
		vars.prj.path.o_iso_m,
		model.casper_relative_directory)
	for index, kernel_details in enumerate(kernel_details_list):
		note = ''
		version_name = kernel_details[1]
		if current_kernel_version_name == version_name:
			if note: note += ' '  # os.linesep
			note += 'This is the kernel version you are currently running.'
		if index == 0:
			if note: note += ' '  # os.linesep
			note += 'This is the newest kernel version that may be used to bootstrap the customized live ISO image.'
		directory = kernel_details[6]
		if directory == original_iso_directory:
			if note: note += ' '  # os.linesep
			note += 'This kernel is used to bootstrap the original live ISO image.'
			if len(kernel_details_list) > 1:
				if note: note += ' '  # os.linesep
				note += 'Select this kernel if you encounter issues such as BusyBox when using other kernel versions.'
			# if is_server_image()
			#     # if note: note += ' ' # os.linesep
			#     # note += 'Since you are customizing a server image, select this option if you encounter issues using other kernel versions.'
			#     # Set the selected index for the the original live ISO image kernel.
			#     selected_index = index
		new_vmlinuz_filename = kernel_details[3]
		new_initrd_filename = kernel_details[5]
		if note: note += ' '  # os.linesep
		note += 'Reference these files as <tt>%s</tt> and <tt>%s</tt> in the ISO boot configurations.' % (
			new_vmlinuz_filename,
			new_initrd_filename)
		kernel_details[7] = note

	# For debugging.
	print_kernel_details_list(kernel_details_list)

	# Set the selected kernel based on the selected index.
	kernel_details_list[selected_index][8] = True

	# Remove the 1st column because it is a tuple and cannot be rendered.
	# The resulting kernel_details is:
	# 0: version_name
	# 1: vmlinuz_filename
	# 2: new_vmlinuz_filename
	# 3: initrd_filename
	# 4: new_initrd_filename
	# 5: directory
	# 6: note
	# 7: is_selected
	# 8: is_remove
	[kernel_details.pop(0) for kernel_details in kernel_details_list]

	# Log the resuting list of kernel versions.
	total = len(kernel_details_list)
	for index, kernel_details in enumerate(kernel_details_list):
		dbg('version', kernel_details[0])
		dbg('Index', '%s of %s' % (index, total - 1))
		dbg('Vmlinuz filename', kernel_details[1])
		dbg('New vmlinuz filename', kernel_details[2])
		dbg('Initrd filename', kernel_details[3])
		dbg('New initrd filename', kernel_details[4])
		dbg('Directory', kernel_details[5])
		dbg('Note', kernel_details[6])
		dbg('Is selected', kernel_details[7])
		dbg('Is remove', kernel_details[8])

	dbg(kernel_details_list,nd=2)
	return kernel_details_list


# For debugging only.
def print_kernel_details_list(kernel_details_list):
	total = len(kernel_details_list)
	for index, kernel_details in enumerate(kernel_details_list):
		print_kernel_details(kernel_details, index, total)


# For debugging only.
def print_kernel_details(kernel_details, index, total):
	print(
		'| '
		'{:13.13s} | '
		'{:8.8s} | '
		'{:6.6s} | '
		'Vmlinuz: {:15.15s} | '
		'New: {:15.15s} | '
		'Initrd: {:15.15s} | '
		'New: {:15.15s} | '
		'Directory: {:50.50s} | '
		'Note: {:5.5s} | '
		'Selected: {:5.5s} | '
		'Remove: {:5.5s} | '.format(
			str(kernel_details[0]),
			str(kernel_details[1]),
			'%s of %s' % (index,
						  total - 1),
			str(kernel_details[2]),
			str(kernel_details[3]),
			str(kernel_details[4]),
			str(kernel_details[5]),
			str(kernel_details[6]),
			str(kernel_details[7]),
			str(kernel_details[8]),
			str(kernel_details[9])))


def get_current_kernel_version_name():
	version_name = None
	try:
		version_information = re.search(
			r'(\d+\.\d+\.\d+(?:-\d+))',
			platform.release())
		version_name = version_information.group(1)
	except AttributeError as exception:
		pass

	dbg(version_name,nd=2)
	return version_name


def get_current_kernel_release_name():
	dbg(platform.release(),nd=2)
	return platform.release()


def is_server_image():
	# Guess if we are customizing a server image by checking the file
	# name, volume id, or disk name. For example:
	# - original_iso_filename = ubuntu-18.04-live-server-amd64.iso
	# - original_iso_volume_id = Ubuntu-Server 18.04 LTS amd64
	# - original_iso_disk_name = Ubuntu-Server 18.04 LTS "Bionic Beaver" - Release amd64
	result=False
	if re.search('server', model.original_iso_filename, re.IGNORECASE):
		result=True
	if re.search('server', model.original_iso_volume_id, re.IGNORECASE):
		result=True
	if re.search('server', model.original_iso_disk_name, re.IGNORECASE):
		result=True

	dbg(result,nd=2)
	return result


# ------------------------------------------------------------------------------
# Vmlinuz
# ------------------------------------------------------------------------------


def update_kernel_details_list_for_vmlinuz(directory, kernel_details_list):
	dbg('Update kernel details list for vmlinuz')
	filepath_pattern = os.path.join(directory, 'vmlinuz*')
	# Exclude broken symlinks.
	vmlinuz_filepath_list = [
		vmlinuz_filepath for vmlinuz_filepath in glob.glob(filepath_pattern)
		if os.path.exists(vmlinuz_filepath)
	]
	dbg(
		'%i vmlinuz files found in' % len(vmlinuz_filepath_list),
		directory)
	for vmlinuz_filepath in vmlinuz_filepath_list:
		vmlinuz_filename = os.path.basename(vmlinuz_filepath)
		dbg('Get vmlinuz version details')
		version_name = get_vmlinuz_version_name(vmlinuz_filepath)
		if not version_name: version_name = '0.0.0-0'
		dbg('The vmlinuz version is', version_name)
		version_integers = tuple(map(int, re.split('[.-]', version_name)))
		update_kernel_details_for_vmlinuz(
			version_integers,
			version_name,
			vmlinuz_filename,
			directory,
			kernel_details_list)


def update_kernel_details_list_for_vmlinuz_EXPERIMENT(directory, kernel_details_list):
	dbg('Update kernel details list for vmlinuz')
	filepath_pattern = os.path.join(directory, 'vmlinuz*')
	# vmlinuz_filepath_list = glob.glob(filepath_pattern)
	# vmlinuz_filepath_list = [ vmlinuz_filepath for vmlinuz_filepath in vmlinuz_filepath_list if os.path.exists(vmlinuz_filepath) ]
	vmlinuz_filepath_list = [
		vmlinuz_filepath for vmlinuz_filepath in glob.glob(filepath_pattern)
		if os.path.exists(vmlinuz_filepath)
	]
	dbg(
		'%i vmlinuz files found in' % len(vmlinuz_filepath_list),
		directory)
	for vmlinuz_filepath in vmlinuz_filepath_list:
		vmlinuz_filename = os.path.basename(vmlinuz_filepath)
		dbg('Get vmlinuz version details')
		version_name = get_vmlinuz_version_name(vmlinuz_filepath)
		if not version_name: version_name = '0.0.0-0'
		dbg('The vmlinuz version is', version_name)
		version_integers = tuple(map(int, re.split('[.-]', version_name)))
		update_kernel_details_for_vmlinuz(
			version_integers,
			version_name,
			vmlinuz_filename,
			directory,
			kernel_details_list)


def update_kernel_details_list_for_vmlinuz_ORIGINAL(directory, kernel_details_list):
	dbg('Update kernel details list for vmlinuz')
	filepath_pattern = os.path.join(directory, 'vmlinuz*')
	vmlinuz_filepath_list = glob.glob(filepath_pattern)
	dbg(
		'%i vmlinuz files found in' % len(vmlinuz_filepath_list),
		directory)
	for vmlinuz_filepath in vmlinuz_filepath_list:
		vmlinuz_filename = os.path.basename(vmlinuz_filepath)
		dbg('Get vmlinuz version details')
		version_name = get_vmlinuz_version_name(vmlinuz_filepath)
		if not version_name: version_name = '0.0.0-0'
		dbg('The vmlinuz version is', version_name)
		version_integers = tuple(map(int, re.split('[.-]', version_name)))
		update_kernel_details_for_vmlinuz(
			version_integers,
			version_name,
			vmlinuz_filename,
			directory,
			kernel_details_list)


def update_kernel_details_for_vmlinuz(version_integers, version_name, vmlinuz_filename, directory, kernel_details_list):
	dbg('Search kernel details list for matching version')
	dbg('Kernel version', version_name)
	dbg('Kernel version as integers', version_integers)
	dbg('Vmlinuz filename', vmlinuz_filename)
	dbg('Directory', directory)

	# 0: version_integers
	# 1: version_name
	# 2: vmlinuz_filename
	# 3: new_vmlinuz_filename
	# 4: initrd_filename
	# 5: new_initrd_filename
	# 6: directory
	# 7: note
	# 8: is_selected
	# 9: is_remove

	found = False
	for index, kernel_details in enumerate(list(kernel_details_list)):
		if (kernel_details[0] == version_integers
				and kernel_details[1] == version_name
				and kernel_details[6] == directory):
			found = True
			if kernel_details[2] == vmlinuz_filename:
				dbg('Matching kernel version found?', 'Yes')
				total = len(kernel_details_list)
				dbg(
					'Index of match',
					'%i of %i' % (index,
								  total - 1))

				dbg('Skip updating kernel details')

				dbg('Index', '%s of %s' % (index, total - 1))
				dbg('Kernel version', kernel_details[1])
				dbg(
					'Kernel version as integers',
					kernel_details[0])
				dbg('Vmlinuz filename', kernel_details[2])
				dbg('Initrd filename', kernel_details[4])
				dbg('Directory', kernel_details[6])
				break  # TODO: break is not needed here.
			elif not kernel_details[2]:
				dbg('Matching kernel version found?', 'Yes')
				total = len(kernel_details_list)
				dbg(
					'Index of match',
					'%i of %i' % (index,
								  total - 1))

				dbg('Update kernel details')
				kernel_details[2] = vmlinuz_filename

				dbg('Index', '%s of %s' % (index, total - 1))
				dbg('Kernel version', kernel_details[1])
				dbg(
					'Kernel version as integers',
					kernel_details[0])
				dbg('Vmlinuz filename', kernel_details[2])
				dbg('Initrd filename', kernel_details[4])
				dbg('Directory', kernel_details[6])
			else:
				dbg('Matching kernel version found?', 'Yes')
				total = len(kernel_details_list)
				dbg(
					'Index of match',
					'%i of %i' % (index,
								  total - 1))

				dbg('Copy and add kernel details')
				kernel_details = list(kernel_details)
				kernel_details[2] = vmlinuz_filename
				kernel_details_list.append(kernel_details)

				total = len(kernel_details_list)
				dbg('Index', '%s of %s' % (total - 1, total - 1))
				dbg('Kernel version', kernel_details[1])
				dbg(
					'Kernel version as integers',
					kernel_details[0])
				dbg('Vmlinuz filename', kernel_details[2])
				dbg('Initrd filename', kernel_details[4])
				dbg('Directory', kernel_details[6])
	if not found:
		dbg('Matching kernel version found?', 'No')

		dbg('Add new kernel details')

		kernel_details = [None] * 10
		if not version_integers:
			version_integers = (0, 0, 0)
		kernel_details[0] = version_integers
		kernel_details[1] = version_name
		kernel_details[2] = vmlinuz_filename
		kernel_details[3] = None  # new_vmlinuz_filename
		kernel_details[4] = None  # initrd_filename
		kernel_details[5] = None  # new_initrd_filename
		kernel_details[6] = directory
		kernel_details[7] = None  # note
		kernel_details[8] = False  # is_selected
		kernel_details[9] = False  # is_remove
		kernel_details_list.append(kernel_details)

		total = len(kernel_details_list)
		dbg('Index', '%s of %s' % (total - 1, total - 1))
		dbg('Kernel version', kernel_details[1])
		dbg('Kernel version as integers', kernel_details[0])
		dbg('Vmlinuz filename', kernel_details[2])
		dbg('Initrd filename', kernel_details[4])
		dbg('Directory', kernel_details[6])


def get_vmlinuz_version_name(filepath):
	dbg('Get vmlinuz version', filepath)
	version_name = (
		_get_vmlinuz_version_name_from_file_name(filepath)
		or _get_vmlinuz_version_name_from_file_type(filepath)
		or _get_vmlinuz_version_name_from_file_contents(filepath))

	dbg(version_name,nd=2)
	return version_name


def _get_vmlinuz_version_name_from_file_name(filepath):
	dbg('Get vmlinuz version from file name', filepath)
	filename = os.path.basename(filepath)
	version_name = re.search(r'\d[\d\.-]*\d', filename)
	version_name = version_name.group(0) if version_name else None

	dbg(version_name,nd=2)
	return version_name


def _get_vmlinuz_version_name_from_file_type(filepath):
	dbg('Get vmlinuz version from file type', filepath)
	command = 'file "%s"' % filepath
	result, error = exec_sync(command)
	version_name = None
	if not error:
		version_information = re.search(
			r'(\d+\.\d+\.\d+(?:-\d+))',
			str(result))
		if version_information:
			version_name = version_information.group(1)
			dbg('Found version', version_name)

	dbg(version_name,nd=2)
	return version_name


def _get_vmlinuz_version_name_from_file_contents(filepath):
	dbg('Get vmlinuz version from file contents', filepath)
	version_name = None
	with open(filepath, errors='ignore') as file:
		contents = file.read()
	candidate = ''
	for character in contents:
		if character in string.printable:
			candidate += character
		elif len(candidate) > 4:
			try:
				version_information = re.search(
					r'(\d+\.\d+\.\d+(?:-\d+))',
					str(candidate))
				version_name = version_information.group(1)
				dbg('Found version', version_name)
				break
			except:
				candidate = ''
		else:
			candidate = ''

	dbg(version_name,nd=2)
	return version_name


# ------------------------------------------------------------------------------
# Initrd
# ------------------------------------------------------------------------------


def update_kernel_details_list_for_initrd(directory, kernel_details_list):
	dbg('Update kernel details list for initrd')
	filepath_pattern = os.path.join(directory, 'initrd*')
	# Exclude broken symlinks.
	initrd_filepath_list = [
		initrd_filepath for initrd_filepath in glob.glob(filepath_pattern)
		if os.path.exists(initrd_filepath)
	]
	dbg(
		'%i initrd files found in' % len(initrd_filepath_list),
		directory)
	for initrd_filepath in initrd_filepath_list:
		initrd_filename = os.path.basename(initrd_filepath)
		dbg('Get initrd version details')
		version_name = get_initrd_version_name(initrd_filepath)

		# TODO: See if this hack can be improved?
		# As a last resort, grab the first vmlinuz version from this
		# directory, and assume the initrd version is the same. The
		# situation where the initrd version is unknown should only
		# happen in the casper directory of the ISO; this is a critical
		# assumption. In the casper directory, the initrd version should
		# correspond to the vmlinuz version, and it is reasonable to
		# simply use the vmlinuz version, whenever the version of initrd
		# cannot be determined in this directory.
		if not version_name:
			version_name = get_vmlinuz_version_from_kernel_details_list(
				kernel_details_list,
				directory)

		dbg('The initrd version is', version_name)
		version_integers = tuple(map(int, re.split('[.-]', version_name)))
		update_kernel_details_for_initrd(
			version_integers,
			version_name,
			initrd_filename,
			directory,
			kernel_details_list)


def get_vmlinuz_version_from_kernel_details_list( kernel_details_list,  directory):
	# The kernel_details is:
	# 0: version_integers
	# 1: version_name
	# 2: vmlinuz_filename
	# 3: new_vmlinuz_filename
	# 4: initrd_filename
	# 5: new_initrd_filename
	# 6: directory
	# 7: note
	# 8: is_selected
	# 9: is_remove

	version_name = '0.0.0-0'
	for kernel_details in kernel_details_list:
		if directory == kernel_details[6]:
			version_name = kernel_details[1]
			break
	dbg(version_name,nd=2)
	return version_name


def update_kernel_details_list_for_initrd_EXPERIMENT( directory, kernel_details_list):
	dbg('Update kernel details list for initrd')
	filepath_pattern = os.path.join(directory, 'initrd*')
	# initrd_filepath_list = glob.glob(filepath_pattern)
	# initrd_filepath_list = [ initrd_filepath for initrd_filepath in initrd_filepath_list if not os.path.exists(initrd_filepath) ]
	initrd_filepath_list = [
		initrd_filepath for initrd_filepath in glob.glob(filepath_pattern)
		if os.path.exists(initrd_filepath)
	]
	dbg(
		'%i initrd files found in' % len(initrd_filepath_list),
		directory)
	for initrd_filepath in initrd_filepath_list:
		initrd_filename = os.path.basename(initrd_filepath)
		dbg('Get initrd version details')
		version_name = get_initrd_version_name(initrd_filepath)
		if not version_name: version_name = '0.0.0-0'
		dbg('The initrd version is', version_name)
		version_integers = tuple(map(int, re.split('[.-]', version_name)))
		update_kernel_details_for_initrd(
			version_integers,
			version_name,
			initrd_filename,
			directory,
			kernel_details_list)


def update_kernel_details_list_for_initrd_ORIGINAL(  directory, kernel_details_list):
	dbg('Update kernel details list for initrd')
	filepath_pattern = os.path.join(directory, 'initrd*')
	initrd_filepath_list = glob.glob(filepath_pattern)
	dbg(
		'%i initrd files found in' % len(initrd_filepath_list),
		directory)
	for initrd_filepath in initrd_filepath_list:
		initrd_filename = os.path.basename(initrd_filepath)
		dbg('Get initrd version details')
		version_name = get_initrd_version_name(initrd_filepath)
		if not version_name: version_name = '0.0.0-0'
		dbg('The initrd version is', version_name)
		version_integers = tuple(map(int, re.split('[.-]', version_name)))
		update_kernel_details_for_initrd(
			version_integers,
			version_name,
			initrd_filename,
			directory,
			kernel_details_list)


def update_kernel_details_for_initrd( version_integers, version_name, initrd_filename, directory,  kernel_details_list):
	dbg('Search kernel details list for matching version')
	dbg('Kernel version', version_name)
	dbg('Kernel version as integers', version_integers)
	dbg('Initrd filename', initrd_filename)
	dbg('Directory', directory)

	# 0: version_integers
	# 1: version_name
	# 2: vmlinuz_filename
	# 3: new_vmlinuz_filename
	# 4: initrd_filename
	# 5: new_initrd_filename
	# 6: directory
	# 7: note
	# 8: is_selected
	# 9: is_remove

	found = False
	for index, kernel_details in enumerate(list(kernel_details_list)):
		if (kernel_details[0] == version_integers
				and kernel_details[1] == version_name
				and kernel_details[6] == directory):
			found = True
			if kernel_details[4] == initrd_filename:
				dbg('Matching kernel version found?', 'Yes')
				total = len(kernel_details_list)
				dbg(
					'Index of match',
					'%i of %i' % (index,
								  total - 1))

				dbg('Skip updating kernel details')

				dbg('Index', '%s of %s' % (index, total - 1))
				dbg('Kernel version', kernel_details[1])
				dbg(
					'Kernel version as integers',
					kernel_details[0])
				dbg('Vmlinuz filename', kernel_details[2])
				dbg('Initrd filename', kernel_details[4])
				dbg('Directory', kernel_details[6])
				break  # TODO: break is not needed here.
			elif not kernel_details[4]:
				dbg('Matching kernel version found?', 'Yes')
				total = len(kernel_details_list)
				dbg(
					'Index of match',
					'%i of %i' % (index,
								  total - 1))

				dbg('Update kernel details')
				kernel_details[4] = initrd_filename

				dbg('Index', '%s of %s' % (index, total - 1))
				dbg('Kernel version', kernel_details[1])
				dbg(
					'Kernel version as integers',
					kernel_details[0])
				dbg('Vmlinuz filename', kernel_details[2])
				dbg('Initrd filename', kernel_details[4])
				dbg('Directory', kernel_details[6])
			else:
				dbg('Matching kernel version found?', 'Yes')
				total = len(kernel_details_list)
				dbg(
					'Index of match',
					'%i of %i' % (index,
								  total - 1))

				dbg('Copy and add kernel details')
				kernel_details = list(kernel_details)
				kernel_details[4] = initrd_filename
				kernel_details_list.append(kernel_details)

				total = len(kernel_details_list)
				dbg('Index', '%s of %s' % (total - 1, total - 1))
				dbg('Kernel version', kernel_details[1])
				dbg(
					'Kernel version as integers',
					kernel_details[0])
				dbg('Vmlinuz filename', kernel_details[2])
				dbg('Initrd filename', kernel_details[4])
				dbg('Directory', kernel_details[6])
	if not found:
		dbg('Matching kernel version found?', 'No')

		dbg('Add new kernel details')

		kernel_details = [None] * 10
		if not version_integers:
			version_integers = (0, 0, 0)
		kernel_details[0] = version_integers
		kernel_details[1] = version_name
		kernel_details[2] = None  # vmlinuz_filename
		kernel_details[3] = None  # new_vmlinuz_filename
		kernel_details[4] = initrd_filename
		kernel_details[5] = None  # new_initrd_filename
		kernel_details[6] = directory
		kernel_details[7] = None  # note
		kernel_details[8] = False  # is_selected
		kernel_details[9] = False  # is_remove
		kernel_details_list.append(kernel_details)

		total = len(kernel_details_list)
		dbg('Index', '%s of %s' % (total - 1, total - 1))
		dbg('Kernel version', kernel_details[1])
		dbg('Kernel version as integers', kernel_details[0])
		dbg('Vmlinuz filename', kernel_details[2])
		dbg('Initrd filename', kernel_details[4])
		dbg('Directory', kernel_details[6])


def get_initrd_version_name(filepath):
	dbg('Get initrd version', filepath)
	version_name = (
		_get_initrd_version_name_from_file_name(filepath)
		or _get_initrd_version_name_from_file_contents(filepath)
		or _get_initrd_version_name_from_file_type(filepath))

	dbg(version_name,nd=2)
	return version_name


def _get_initrd_version_name_from_file_name(filepath):
	dbg('Get initrd version from file name', filepath)
	filename = os.path.basename(filepath)
	version_name = re.search(r'\d[\d\.-]*\d', filename)
	version_name = version_name.group(0) if version_name else None

	dbg(version_name,nd=2)
	return version_name


def _get_initrd_version_name_from_file_type(filepath):
	dbg('Get initrd version from file type', filepath)
	command = 'file "%s"' % filepath
	result, error = exec_sync(command)
	version_name = None
	if not error:
		version_information = re.search(
			r'(\d+\.\d+\.\d+(?:-\d+))',
			str(result))
		if version_information:
			version_name = version_information.group(1)
			dbg('Found version', version_name)

	dbg(version_name,nd=2)
	return version_name


def _get_initrd_version_name_from_file_contents(filepath):
	dbg('Get initrd version from file contents', filepath)
	version_name = None
	try:
		command = 'lsinitramfs "%s"' % filepath
		process = pexpect.spawnu(command, timeout=60)
		match = False
		while process.exitstatus is None and not match:
			line = process.readline()
			# print('%s' % line, end='')
			match = re.search(r'lib/modules/(\d[\d\.-]*\d)', line)
			if ('cannot' in line or 'error' in line or 'premature' in line):
				dbg(
					'Encountered exception while getting initrd version from file contents',
					line)
		if match:
			process.terminate(True)
			version_name = match.group(1)
	except pexpect.TIMEOUT as exception:
		dbg(
			'Encountered exception while getting initrd version from file contents',
			exception)
	except pexpect.EOF as exception:
		dbg(
			'Encountered exception while getting initrd version from file contents',
			exception)
	except pexpect.ExceptionPexpect as exception:
		dbg(
			'Encountered exception while getting initrd version from file contents',
			exception)

	dbg(version_name,nd=2)
	return version_name


########################################################################
# Create Filesystem Manifest Functions
########################################################################


def is_exists_filesystem_manifest_remove(filename):
	# Check custom live iso directory
	directory = vars.prj.path.c_iso
	filepath = os.path.join(
		directory,
		model.casper_relative_directory,
		filename)

	is_exists = os.path.exists(filepath)
	result=None
	if is_exists:
		dbg(
			'%s found in' % filename,
			os.path.join(directory,
						 model.casper_relative_directory))
		result=True
	else:
		dbg(
			'%s not found in' % filename,
			os.path.join(directory,
						 model.casper_relative_directory))
		result=False
	
	dbg(result,nd=2)
	return result


def create_installed_packages_list(thread):
	command = 'chroot "%s" dpkg-query --show' % vars.prj.path.c_squash
	result, error = exec_sync(command, thread)
	pkglist = result.splitlines()
	dbg('Total number of installed packages', len(pkglist))
	return pkglist


def create_filesystem_manifest_file(installed_packages_list):
	dbg('Create new filesystem manifest file')
	filepath = os.path.join(
		vars.prj.path.c_iso,
		model.casper_relative_directory,
		'filesystem.manifest')
	dbg('Write filesystem manifest to', filepath)
	with open(filepath, 'w') as file:
		for line in installed_packages_list:
			file.write('%s\n' % line)


def get_removable_packages_list(filename):
	# Read filesystem.manifest-remove to get list of packages to remove.
	removable_packages_list = []
	filepath = os.path.join(
		vars.prj.path.c_iso,
		model.casper_relative_directory,
		filename)
	dbg('Read list of packages to remove from', filepath)
	with open(filepath, 'r') as file:
		removable_packages_list = file.read().splitlines()

	dbg(removable_packages_list,nd=2)
	return removable_packages_list


def create_package_details_list(installed_packages_list, removable_packages_list_1, removable_packages_list_2):
	dbg('Create package details list')

	# List installed packages and mark packages that will be removed.

	number_of_packages_to_remove_1 = 0
	number_of_packages_to_retain_1 = 0
	number_of_packages_to_remove_2 = 0
	number_of_packages_to_retain_2 = 0
	package_details_list = []

	for line in installed_packages_list:

		package_details = line.split()
		package_name = package_details[0]
		is_remove_1 = (package_name in removable_packages_list_1) or (
			package_name.rpartition(':')[0] in removable_packages_list_1)
		number_of_packages_to_remove_1 += is_remove_1
		number_of_packages_to_retain_1 += not is_remove_1
		is_remove_2 = (package_name in removable_packages_list_2) or (
			package_name.rpartition(':')[0] in removable_packages_list_2)
		number_of_packages_to_remove_2 += is_remove_2
		number_of_packages_to_retain_2 += not is_remove_2
		package_details.insert(0, is_remove_1)
		package_details.insert(1, is_remove_2 or is_remove_1)
		package_details.insert(2, is_remove_2)
		package_details.insert(3, not is_remove_1)

		package_details_list.append(package_details)

	dbg(
		'Total number of installed packages',
		len(installed_packages_list),nd=2)
	dbg(
		'Number of packages to be removed after a typical install',
		number_of_packages_to_remove_1,nd=2)
	dbg(
		'Number of packages to be retained after a typical install',
		number_of_packages_to_retain_1,nd=2)
	dbg(
		'Number of packages to be removed after a minimal install',
		number_of_packages_to_remove_2,nd=2)
	dbg(
		'Number of packages to be retained after a minimal install',
		number_of_packages_to_retain_2)
	dbg(len(package_details_list),'packages listed',nd=2)
	return package_details_list


def create_typical_removable_packages_list():
	dbg('Create typical removable packages list')

	listore_name = 'options_page__package_manifest_tab__liststore'
	dbg('Get user selections from', listore_name)
	liststore = vars.app.builder.get_object(listore_name)
	removable_packages_list = []
	item = liststore.get_iter_first()
	while item is not None:
		flag = liststore.get_value(item, 0)
		package_name = liststore.get_value(item, 4)
		if flag: removable_packages_list.append(package_name)
		item = liststore.iter_next(item)
	removable_packages_list
	dbg(
		'New number of packages to be removed',
		len(removable_packages_list))

	dbg(removable_packages_list,nd=2)
	return removable_packages_list


def create_minimal_removable_packages_list():
	dbg('Create minimal removable packages list')

	listore_name = 'options_page__package_manifest_tab__liststore'
	dbg('Get user selections from', listore_name)
	liststore = vars.app.builder.get_object(listore_name)
	removable_packages_list = []
	item = liststore.get_iter_first()
	while item is not None:
		flag = liststore.get_value(item, 1) and liststore.get_value(item, 3)
		package_name = liststore.get_value(item, 4)
		if flag: removable_packages_list.append(package_name)
		item = liststore.iter_next(item)
	removable_packages_list
	dbg(
		'New number of packages to be removed',
		len(removable_packages_list))

	dbg(removable_packages_list,nd=2)
	return removable_packages_list


# TODO: This function is not used.
def create_removable_packages_list(listore_name, index):
	dbg('Get removable packages list from user selections')
	dbg('Get user selections from', listore_name)
	liststore = vars.app.builder.get_object(listore_name)
	removable_packages_list = []
	item = liststore.get_iter_first()
	while item is not None:
		flag = liststore.get_value(item, index)
		package_name = liststore.get_value(item, 2)
		if flag: removable_packages_list.append(package_name)
		item = liststore.iter_next(item)
	removable_packages_list
	dbg(
		'New number of packages to be removed',
		len(removable_packages_list))

	dbg(removable_packages_list,nd=2)
	return removable_packages_list


def create_filesystem_manifest_remove_file(filename, removable_packages_list):
	dbg('Create new filesystem manifest remove file')
	filepath = os.path.join(
		vars.prj.path.c_iso,
		model.casper_relative_directory,
		filename)
	dbg('Write filesystem manifest remove file to', filepath)
	with open(filepath, 'w') as file:
		first_line = True
		for packages_name in removable_packages_list:
			if first_line:
				file.write('%s' % packages_name)
				first_line = False
			else:
				file.write('\n%s' % packages_name)


def save_stack_buffers(stack_name):
	stack = vars.app.builder.get_object(stack_name)
	scrolled_windows = stack.get_children()
	for scrolled_window in scrolled_windows:
		filepath = stack.child_get_property(scrolled_window, 'name')
		title = stack.child_get_property(scrolled_window, 'title')
		dbg('Write file', filepath)
		# Get the updated text.
		source_view = scrolled_window.get_child()
		source_buffer = source_view.get_buffer()
		start_iter = source_buffer.get_start_iter()
		end_iter = source_buffer.get_end_iter()
		data = source_buffer.get_text(start_iter, end_iter, True)
		directory = os.path.dirname(filepath)
		os.makedirs(directory, exist_ok=True)
		os.chmod(
			directory,
			stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH | stat.S_IXUSR
			| stat.S_IXGRP | stat.S_IXOTH)
		# Write the file.
		with open(filepath, 'w') as file:
			file.write(data)
		# file.flush()
		os.chmod(filepath, stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH)


# TODO: Remove this function when 14.04 is no longer supported.
# This function is necessary because Gtk 3.10 in Ubuntu 14.04 does not support
# Gtk.Stack or Gtk.StackSidebar.
# This function identifies the selected kernel, then replaces text in the boot
# configurations based on this selection.
def update_and_save_boot_configurations():
	liststore = vars.app.builder.get_object(
		'options_page__linux_kernels_tab__liststore')
	for selected_index, kernel_details in enumerate(liststore):
		if kernel_details[7]: break
	else: selected_index = 0
	dbg('The selected kernel is index number', selected_index)

	# Search and replace text.
	search_text_1 = r'/vmlinuz\S*'
	replacement_text_1 = '/%s' % liststore[selected_index][2]
	search_text_2 = r'/initrd\S*'
	replacement_text_2 = '/%s' % liststore[selected_index][4]

	# Update grub.cfg.
	filepath = os.path.join(
		vars.prj.path.c_iso,
		'boot',
		'grub',
		'grub.cfg')
	replace_text_in_file(filepath, search_text_1, replacement_text_1)
	replace_text_in_file(filepath, search_text_2, replacement_text_2)
	if os.path.exists(filepath):
		os.chmod(filepath, stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH)

	# Update loopback.cfg.
	filepath = os.path.join(
		vars.prj.path.c_iso,
		'boot',
		'grub',
		'loopback.cfg')
	replace_text_in_file(filepath, search_text_1, replacement_text_1)
	replace_text_in_file(filepath, search_text_2, replacement_text_2)
	if os.path.exists(filepath):
		os.chmod(filepath, stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH)
	# Update txt.cfg.
	filepath = os.path.join(
		vars.prj.path.c_iso,
		'isolinux',
		'txt.cfg')
	replace_text_in_file(filepath, search_text_1, replacement_text_1)
	replace_text_in_file(filepath, search_text_2, replacement_text_2)
	if os.path.exists(filepath):
		os.chmod(filepath, stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH)


def copy_vmlinuz_and_initrd_files(thread):
	liststore = vars.app.builder.get_object(
		'options_page__linux_kernels_tab__liststore')
	for selected_index, kernel_details in enumerate(liststore):
		if kernel_details[7]: break
	else: selected_index = 0
	dbg('The selected kernel is index number', selected_index)
	# Get selected directory.
	# source_directory = os.path.realpath(liststore[selected_index][5])
	source_directory = liststore[selected_index][5]
	target_directory = os.path.join(
		vars.prj.path.c_iso,
		model.casper_relative_directory)
	# vmlinuz
	# Delete existing vmlinuz* file(s) in target directory.
	pattern = os.path.join(target_directory, 'vmlinuz*')
	delete_files_with_pattern(pattern)
	# Get selected vmlinuz filepath.
	source_filepath = os.path.join(
		source_directory,
		liststore[selected_index][1])
	# Get target vmlinuz filepath.
	target_filename = liststore[selected_index][2]
	target_filepath = os.path.join(target_directory, target_filename)

	# Copy selected vmlinuz* file to target directory.
	copy_boot_file(source_filepath, 0, target_filepath, 2, thread)
	# $ chmod a=r ./custom-live-iso/casper/vmlinuz{.???}
	# $ chmod a=r ./custom-live-iso/install/vmlinuz{.???} (ex. ubuntu-14.04.5-server-amd64.iso)
	os.chmod(
		target_filepath,
		stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | stat.S_IROTH)
	#
	# initrd
	#

	# Delete existing initrd* file in target directory
	pattern = os.path.join(target_directory, 'initrd*')
	delete_files_with_pattern(pattern)

	# Get selected initrd filepath.
	source_filepath = os.path.join(
		source_directory,
		liststore[selected_index][3])

	# Get target initrd filepath.
	target_filename = liststore[selected_index][4]
	target_filepath = os.path.join(target_directory, target_filename)

	# Copy selected initrd* file to target directory
	copy_boot_file(source_filepath, 1, target_filepath, 2, thread)
	# $ chmod a=r ./custom-live-iso/casper/initrd{.???}
	# $ chmod a=r ./custom-live-iso/install/initrd{.???} (ex. ubuntu-14.04.5-server-amd64.iso)
	os.chmod(
		target_filepath,
		stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | stat.S_IROTH)


#
# Create relative links
#

# TODO: Make sure the following *relative* links are created:
#       - squashfs-root/initrd.img --> /boot/initrd.img-4.8.0-37-generic
#       - squashfs-root/vmlinuz    --> /boot/vmlinuz-4.8.0-37-generic
#       - Use the function: os.symlink(src, dst)


def delete_files_with_pattern(pattern):
	dbg('Delete existig  files with pattern', pattern)
	[os.remove(delete_filepath) for delete_filepath in glob.glob(pattern)]


def calculate_vmlinuz_filename(filepath):
	filename = 'vmlinuz'
	dbg(filename,nd=2)
	return filename


def calculate_initrd_filename(filepath):
	# Determine extension for initrd file.
	# Use initrd.lz, initrd.gz, or initrd depending on the compression type.
	command = 'file "%s"' % filepath
	result, error = exec_sync(command)
	dbg('The file type informaton is', result)
	compression = None
	match = re.search(r':\s(.*)\scompressed data', result)
	if match: compression = match.group(1)
	else: dbg('Compression for initrd not found in', filepath)
	dbg('The compression for initrd is', compression)
	if compression == 'LZMA': filename = 'initrd.lz'
	elif compression == 'gzip': filename = 'initrd.gz'
	else: filename = 'initrd'
	dbg(filename,nd=2)
	return filename


def copy_boot_file(asrc, afno, adest, atotal, athread):
	dbg('%s of %s' % (afno + 1, atotal))
	dbg('The source file is', asrc)
	dbg('The target file is', adest)
	command = (
		'rsync'
		' --archive'
		' --no-relative'
		' --no-implied-dirs'
		' --info=progress2'
		' "%s" "%s"' % (asrc,adest)
	)
	start_time = datetime.datetime.now()
	exec_async(command, athread)
	progress_initial_global = int(round(100 * afno / atotal, 0))
	progress_initial = 0
	progress_target = 100
	progress_display = progress_initial - 1
	progress_current = progress_initial
	while (athread.process.exitstatus is
		   None) or (progress_display < progress_target
					 and not athread.process.exitstatus):
		try:
			line = athread.process.read_nonblocking(100, 0.05)
			result = re.search(r'([0-9]{1,3})%', str(line))
			if result:
				progress_current = progress_initial + int(result.group(1))
		except pexpect.TIMEOUT:
			pass
		except pexpect.EOF:
			if progress_current < progress_target:
				progress_current = progress_target
			time.sleep(0.01)
		if progress_current > progress_display:
			progress_display += 1
			if progress_display % 10 == 0:
				dbg('Completed', '%i%%' % progress_display)
			if progress_display == 0:
				ui.update_progressbar_text(
					'repackage_iso_page__copy_boot_files_progressbar',
					None)
			ui.update_progressbar_percent(
				'repackage_iso_page__copy_boot_files_progressbar',
				progress_initial_global + progress_display / atotal)
	
	end_time = datetime.datetime.now()
	dbg('Completed in', end_time-start_time)
	time.sleep(0.01)


def squashfs_mk(athread):
	source_path = vars.prj.path.c_squash
	dbg('The source path is', source_path)
	target_path = os.path.join(
		vars.prj.path.c_iso,
		model.casper_relative_directory,
		'filesystem.squashfs')
	dbg('The target path is', target_path)
	command = (
		'mksquashfs "%s" "%s"'
		' -noappend'
		' -comp xz'
		' -wildcards'
		' -e "root/.bash_history"'
		' -e "root/.cache"'
		' -e "root/.wget-hsts"'
		' -e "home/*/.bash_history"'
		' -e "home/*/.cache"'
		' -e "home/*/.wget-hsts"'
		' -e "tmp/*"'
		' -e "tmp/.*"' % (source_path,
						  target_path))
	start_time = datetime.datetime.now()
	exec_async(command, athread)
	progress_initial = 0
	progress_target = 100
	progress_display = progress_initial - 1
	progress_current = progress_initial
	while (athread.process.exitstatus is
		   None) or (progress_display < progress_target
					 and not athread.process.exitstatus):
		try:
			line = athread.process.read_nonblocking(100, 0.05)
			result = re.search(r'([0-9]{1,3})%', str(line))
			if result:
				progress_current = progress_initial + int(result.group(1))
		except pexpect.TIMEOUT:
			pass
		except pexpect.EOF:
			if progress_current < progress_target:
				progress_current = progress_target
			time.sleep(0.05)
		if progress_current > progress_display:
			progress_display += 1
			if progress_display % 10 == 0:
				dbg('Completed', '%i%%' % progress_display)
			if progress_display == 0:
				ui.update_progressbar_text(
					'repackage_iso_page__create_squashfs_progressbar',
					None)
			ui.update_progressbar_percent(
				'repackage_iso_page__create_squashfs_progressbar',
				progress_display)
	
	end_time = datetime.datetime.now()
	dbg('Completed in', end_time-start_time)
	time.sleep(0.10)


def update_filesystem_size(thread):
	dbg('Update filesystem size')
	command = 'du --summarize --one-file-system --block-size=1 "%s"' % vars.prj.path.c_squash
	result, error = exec_sync(command, thread)
	size = '0'
	if not error:
		size_information = re.search(r'^([0-9]+)\s', result)
		if size_information:
			size = size_information.group(1)
		else:
			dbg(
				'Unable to get filesystem size for %s' %
				vars.prj.path.c_squash,
				result)
	else:
		dbg(
			'Unable to get filesystem size for %s' % vars.prj.path.c_squash,
			result)
	filepath = os.path.join(
		vars.prj.path.c_iso,
		model.casper_relative_directory,
		'filesystem.size')
	dbg('Write filesystem size to', filepath)
	with open(filepath, 'w') as file:
		file.write('%s' % size)

	dbg(size,nd=2)
	return int(size)


def update_disk_name():
	dbg('Update disk name')
	try:
		filepath = os.path.join(
			vars.prj.path.c_iso,
			'README.diskdefines')
		search_text = r'^#define DISKNAME.*'
		replacement_text = '#define DISKNAME %s' % model.custom_iso_disk_name
		dbg('Write disk name to', filepath)
		replace_text_in_file(filepath, search_text, replacement_text)
	except Exception as exception:
		dbg(
			'Ignoring exception while updating disk name in README.diskdefines',
			exception)


def update_disk_info():
	dbg('Update disk information')

	try:
		filepath = os.path.join(vars.prj.path.c_iso, '.disk', 'info')
		current_time = datetime.datetime.now()
		formatted_time = '{:%Y%m%d}'.format(current_time)
		text = '%s (%s)' % (model.custom_iso_disk_name, formatted_time)
		dbg(
			'Write custom ISO image disk name and release date',
			text)
		dbg('Write to', filepath)
		with open(filepath, 'w') as file:
			file.write('%s' % text)
	except Exception as exception:
		dbg(
			'Ignoring exception while updating disk name in .disk/info',
			exception)


def calculate_md5_hash_for_file(filepath, blocksize=2**20):
	# filepath = os.path.realpath(filepath)
	hash = hashlib.md5()
	with open(filepath, 'rb') as file:
		while True:
			buffer = file.read(blocksize)
			if not buffer: break
			hash.update(buffer)
	result=hash.hexdigest()
	dbg(result,nd=2)
	return result


def update_md5_checksums_WITHOUT_PROGRESS( checksums_filepath, start_directory, exclude_paths):
	dbg('Update md5 sums')
	checksums_filepath = os.path.realpath(checksums_filepath)
	start_directory = os.path.realpath(start_directory)
	exclude_paths = [os.path.realpath(path) for path in exclude_paths]
	dbg('Write md5 sums to', checksums_filepath)
	count = 0
	with open(checksums_filepath, 'w') as file:
		for directory, directory_names, filenames in os.walk(start_directory):
			if directory not in exclude_paths:
				for filename in filenames:
					filepath = os.path.join(directory, filename)
					if (filepath not in exclude_paths):
						count += 1
						hash = calculate_md5_hash_for_file(filepath)
						relative_filepath = os.path.relpath(
							filepath,
							start_directory)
						file.write('%s  ./%s\n' % (hash, relative_filepath))
						# dbg('%s  ./%s' % (hash, relative_filepath))
	
	dbg(count,nd=2)
	return count


def update_md5_checksums(checksums_filepath, start_directory, exclude_paths):
	dbg('Update MD5 checksums')
	checksums_filepath = os.path.realpath(checksums_filepath)
	start_directory = os.path.realpath(start_directory)
	exclude_paths = [os.path.realpath(path) for path in exclude_paths]
	dbg('Write MD5 checksums to', checksums_filepath)

	# Get filepaths.
	filepaths = []
	for directory, directory_names, filenames in os.walk(start_directory):
		if directory not in exclude_paths:
			for filename in filenames:
				filepath = os.path.join(directory, filename)
				if (filepath not in exclude_paths):
					filepaths.append(filepath)
	filepaths.sort(key=lambda filepath: filepath.lower())

	# Calculate MD5 checksums and display progress.
	file_number = 0
	total_files = len(filepaths)
	with open(checksums_filepath, 'w') as file:
		for filepath in filepaths:
			file_number += 1
			ui.update_progressbar_text(
				'repackage_iso_page__update_checksums_progressbar',
				'Calculating checksum for file %i of %i' %
				(file_number,
				 total_files))
			hash = calculate_md5_hash_for_file(filepath)
			relative_filepath = os.path.relpath(filepath, start_directory)
			file.write('%s  ./%s\n' % (hash, relative_filepath))
			# dbg('%s  ./%s' % (hash, relative_filepath))
			ui.update_progressbar_percent(
				'repackage_iso_page__update_checksums_progressbar',
				100 * file_number / total_files)
			time.sleep(0.01)

	dbg(total_files,nd=2)
	return total_files


def count_lines(filepath, thread):
	# filepath = os.path.realpath(filepath)
	command = 'wc --lines "%s"' % filepath
	## exec_async(command, thread)
	## line = thread.process.read()
	result, error = exec_sync(command, thread)
	count = 0
	if not error:
		count_information = re.search(r'([0-9]*) ', str(result))
		if count_information:
			count = count_information.group(1)
		else:
			dbg('Unable to count lines for %s' % filepath, result)
	else:
		dbg('Unable to count lines for %s' % filepath, result)

	dbg(count,nd=2)
	return int(count)


def create_iso_image(thread):
	efi_image_filepath = os.path.join(
		vars.prj.path.c_iso,
		'boot/grub/efi.img')

	custom_iso_filepath = os.path.realpath(
		model.custom_iso_filepath)

	if os.path.exists('/usr/lib/ISOLINUX/isohdpfx.bin'):
		# Ubuntu 15.04 uses isolinux (/usr/lib/ISOLINUX/isohdpfx.bin).
		dbg('Using xorriso with isohybrid MBR',
			'/usr/lib/ISOLINUX/isohdpfx.bin')
		if os.path.exists(efi_image_filepath):
			command = (
				'xorriso'
				' -as mkisofs -r -V "%s" -cache-inodes -J -l'
				' -iso-level 3'
				' -isohybrid-mbr /usr/lib/ISOLINUX/isohdpfx.bin'
				' -c isolinux/boot.cat'
				' -b isolinux/isolinux.bin'
				'  -no-emul-boot'
				'  -boot-load-size 4'
				'  -boot-info-table'
				' -eltorito-alt-boot'
				'  -e boot/grub/efi.img'
				'  -no-emul-boot'
				'  -isohybrid-gpt-basdat'
				' -o "%s" .' %
				(model.custom_iso_volume_id,
				 custom_iso_filepath))
		else:
			command = (
				'xorriso'
				' -as mkisofs -r -V "%s" -cache-inodes -J -l'
				' -iso-level 3'
				' -isohybrid-mbr /usr/lib/ISOLINUX/isohdpfx.bin'
				' -c isolinux/boot.cat'
				' -b isolinux/isolinux.bin'
				'  -no-emul-boot'
				'  -boot-load-size 4'
				'  -boot-info-table'
				' -o "%s" .' %
				(model.custom_iso_volume_id,
				 custom_iso_filepath))
	elif os.path.exists('/usr/lib/syslinux/isohdpfx.bin'):
		# Ubuntu 14.04 uses syslinux-common (/usr/lib/syslinux/isohdpfx.bin).
		dbg(
			'Use xorriso with isohybrid MBR',
			'/usr/lib/syslinux/isohdpfx.bin')
		if os.path.exists(efi_image_filepath):
			command = (
				'xorriso'
				' -as mkisofs -r -V "%s" -cache-inodes -J -l'
				' -iso-level 3'
				' -isohybrid-mbr /usr/lib/syslinux/isohdpfx.bin'
				' -c isolinux/boot.cat'
				' -b isolinux/isolinux.bin'
				'  -no-emul-boot'
				'  -boot-load-size 4'
				'  -boot-info-table'
				' -eltorito-alt-boot'
				'  -e boot/grub/efi.img'
				'  -no-emul-boot'
				'  -isohybrid-gpt-basdat'
				' -o "%s" .' %
				(model.custom_iso_volume_id,
				 custom_iso_filepath))
		else:
			command = (
				'xorriso'
				' -as mkisofs -r -V "%s" -cache-inodes -J -l'
				' -iso-level 3'
				' -isohybrid-mbr /usr/lib/syslinux/isohdpfx.bin'
				' -c isolinux/boot.cat'
				' -b isolinux/isolinux.bin'
				'  -no-emul-boot'
				'  -boot-load-size 4'
				'  -boot-info-table'
				' -o "%s" .' %
				(model.custom_iso_volume_id,
				 custom_iso_filepath))
	else:
		dbg('Use mkisofs', 'No isohybrid MBR available.')
		command = (
			'mkisofs -r -V "%s" -cache-inodes -J -l'
			' -iso-level 3'
			' -c isolinux/boot.cat'
			' -b isolinux/isolinux.bin'
			'  -no-emul-boot'
			'  -boot-load-size 4'
			'  -boot-info-table'
			' -o "%s" .' %
			(model.custom_iso_volume_id,
			 custom_iso_filepath))
	current_time = datetime.datetime.now()
	# formatted_time = '{:%Y-%m-%d %I:%M:%S.%f %p}'.format(current_time)
	formatted_time = '{:%H:%M:%S.%f}'.format(current_time)
	exec_async(command, thread, vars.prj.path.c_iso)
	dbg('The start time is', formatted_time)
	progress_initial = 0
	progress_target = 100
	progress_display = progress_initial - 1
	progress_current = progress_initial

	while (thread.process.exitstatus is
		   None) or (progress_display < progress_target
					 and not thread.process.exitstatus):
		try:
			line = thread.process.read_nonblocking(100, 0.05)
			dbg('Status', line)
			result = re.search(r'([0-9]{1,3})%', str(line))
			if result:
				progress_current = progress_initial + int(result.group(1))
		except pexpect.TIMEOUT:
			pass
		except pexpect.EOF:
			if progress_current < progress_target:
				progress_current = progress_target
			time.sleep(0.05)
		if progress_current > progress_display:
			progress_display += 1
			if progress_display % 10 == 0:
				dbg('Completed', '%i%%' % progress_display)
			if progress_display == 0:
				ui.update_progressbar_text(
					'repackage_iso_page__create_iso_image_progressbar',
					None)
			ui.update_progressbar_percent(
				'repackage_iso_page__create_iso_image_progressbar',
				progress_display)
	current_time = datetime.datetime.now()
	# formatted_time = '{:%Y-%m-%d %I:%M:%S.%f %p}'.format(current_time)
	formatted_time = '{:%H:%M:%S.%f}'.format(current_time)
	dbg('The end time is', formatted_time)
	os.chown(custom_iso_filepath, model.user_id, model.group_id)
	time.sleep(0.10)


def calculate_md5_hash_for_iso():
	custom_iso_filepath = os.path.realpath(
		model.custom_iso_filepath)
	md5_sum = calculate_md5_hash_for_file(custom_iso_filepath)
	model.set_custom_iso_md5_sum(md5_sum)
	custom_iso_md5_filepath = os.path.realpath(
		model.custom_iso_md5_filepath)
	dbg('Write ISO md5sum to', custom_iso_md5_filepath)
	with open(custom_iso_md5_filepath, 'w') as md5_sum_file:
		md5_sum_file.write(
			'%s  %s' % (md5_sum,
						model.custom_iso_filename))
	os.chown(
		model.custom_iso_md5_filepath,
		model.user_id,
		model.group_id)
