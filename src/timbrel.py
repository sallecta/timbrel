#!/usr/bin/python3

########################################################################
#                                                            
# timbrel.py                                          
#                                                                   
# Copyright (C) 2019 Alexander Gribkov <https://github.com/sallecta/timbrel>             
# Copyright (C) 2015 PJ Singh <psingh.cubic@gmail.com>             
#                                                                      
########################################################################

########################################################################
#                                                                   
# This file is part of Timbrel - Custom Ubuntu ISO Creator.      
#                                                                    
# Timbrel is free software: you can redistribute it and/or modify      
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation, either version 3 of the License, or    
# (at your option) any later version.                               
#                                                                    
# Timbrel is distributed in the hope that it will be useful,         
# but WITHOUT ANY WARRANTY; without even the implied warranty of   
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the     
# GNU General Public License for more details.                        
#                                                                    
# You should have received a copy of the GNU General Public License    
# along with Timbrel. If not, see <http://www.gnu.org/licenses/>.       
#                                                                      #
########################################################################

from dbg import *
dbg = dbg_class(nd=0).dbg

import vars
import fn

vars.app.version=fn.version()
dbg('Starting Timbrel version',vars.app.version)


import pwd
import os
import sys
import traceback




import gi
# del gi.require_version('Gdk', '3.0')
# del gi.require_version('Gtk', '3.0')
try:
	GtkSourceVersion='4'
	gi.require_version('GtkSource', GtkSourceVersion)
except ValueError:
	GtkSourceVersion='3.0'
	gi.require_version('GtkSource', GtkSourceVersion)
vars.app.GtkSourceVersion=GtkSourceVersion
dbg('Using GtkSource version', vars.app.GtkSourceVersion)
# to eliminate PyGIWarning: GtkSource was imported
# without specifying a version first
from gi.repository import GtkSource
from gi.repository import Gdk
from gi.repository import GLib
from gi.repository import GObject
from gi.repository import Gtk


import ui
import model
import utils
import handlers



try:
	user_id = os.getuid()
	if user_id != 0:
		dbg('This program requires root access. Please run as "sudo -H '+__file__+'".')
		sys.exit(1)
	model.set_root_user_id(user_id)
	root_gid = os.getgid()
	model.set_root_group_id(root_gid)

# working directory
	if len(sys.argv) == 2:
		if os.path.exists(sys.argv[1]):
			working_directory = sys.argv[1]
		else:
			dbg('Directory '+sys.argv[1]+' does not exist. Usigng script location as working directory.')
			working_directory = vars.app.path
	else:
		working_directory = vars.app.path

	os.chdir(working_directory)
	current_directory = os.getcwd()
	dbg('The working directory is', current_directory)

	uid = int(os.getuid() )
	pw_name, pw_passwd, pw_uid, pw_gid, pw_gecos, pw_dir, pw_shell = pwd.getpwuid(uid)

	model.set_user_name(pw_name)
	model.set_user_id(pw_uid)
	model.set_group_id(pw_gid)
	model.set_home(os.path.expanduser(pw_dir))
	os.environ['HOME'] = model.home
	dbg('HOME environment variable set to', model.home)
	gtk_version = utils.get_package_version('gir1.2-gtk-3.0')[0:4]
	dbg('The GTK version is', gtk_version)
	if float(gtk_version) < 3.18:
		timbrel_ui_filename = vars.app.path + '/ui/timbrel_gtk310.ui'
		filechoosers_ui_filename = vars.app.path + '/ui/filechoosers_gtk310.ui'
	else:
		timbrel_ui_filename = vars.app.path + '/ui/timbrel_gtk318.ui'
		filechoosers_ui_filename = vars.app.path + '/ui/filechoosers_gtk318.ui'

	dbg('The Timbrel user interface filename is', timbrel_ui_filename)
	dbg(
		'The filechoosers user interface filename is',
		timbrel_ui_filename)

	GObject.type_register(GtkSource.View)
	vars.app.builder = Gtk.Builder.new_from_file(timbrel_ui_filename)
	vars.app.builder.add_from_file(filechoosers_ui_filename)

	dconf_directory = os.path.join(model.home, '.cache', 'dconf')
	try:
		os.chown(dconf_directory, model.user_id, model.group_id)
	except FileNotFoundError:
		pass

	vars.app.builder.connect_signals(handlers)

	model.set_page_name('project_directory_page')

	window = vars.app.builder.get_object('window')

	css_provider = Gtk.CssProvider()

	css_provider.load_from_path(vars.app.path + '/css/main.css')

	screen = Gdk.Screen.get_default()
	style_context = window.get_style_context()
	style_context.add_provider_for_screen(
		screen,
		css_provider,
		Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

# gtk theme and icons
	icon_theme = Gtk.IconTheme.get_default()
	theme_name = Gtk.Settings.get_default().get_property('gtk-theme-name')
	dbg('The current GTK theme is', theme_name)
	
	icon_theme_name = Gtk.Settings.get_default().get_property('gtk-icon-theme-name')
	dbg('Initial GTK icon_theme is', icon_theme_name)   
	Gtk.Settings.get_default().set_property("gtk-icon-theme-name", "Timbrel")
	icon_theme_name = Gtk.Settings.get_default().get_property('gtk-icon-theme-name')
	dbg('The current GTK icon_theme is', icon_theme_name) 

	dbg('Replaced GTK icon_theme_search_path from',icon_theme.get_search_path())     
	icon_theme_search_path = [vars.app.path + '/icons/gtk+-3.18.9', vars.app.path + '/icons' ]     
	Gtk.IconTheme.set_search_path(icon_theme,icon_theme_search_path)
	dbg('to', icon_theme.get_search_path())  

	theme_style = ui.get_theme_style(window)
	
	dbg('window.show()')   
	window.show()
	dbg('Gtk.main()')  
	Gtk.main()

except IndexError as exception:
	dbg('IndexError', traceback.format_exc())
except TypeError as exception:
	dbg('TypeError', traceback.format_exc())
except Exception as exception:
	dbg('Exception', traceback.format_exc())
