#!/usr/bin/python3

########################################################################
#                                                                      #
# model.py                                                             #
#                                                                      #
# Copyright (C) 2015 PJ Singh <psingh.cubic@gmail.com>                 #
#                                                                      #
########################################################################

########################################################################
#                                                                      #
# This file is part of Timbrel - Custom Ubuntu ISO Creator.              #
#                                                                      #
# Timbrel is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# (at your option) any later version.                                  #
#                                                                      #
# Timbrel is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public License    #
# along with Timbrel. If not, see <http://www.gnu.org/licenses/>.        #
#                                                                      #
########################################################################

from dbg import *
dbg = dbg_class(nd=1).dbg


import vars
########################################################################
# General
########################################################################

theme_style = None
default_icon_theme_search_path = None
terminal_pid = None
builder = None
root_user_id = None
root_group_id = None
user_name = None
user_id = None
group_id = None
password = None
home = None
uris = None
page_name = None
propagate = None
transition_thread = None
handler_id = None

########################################################################
# Original
########################################################################

original_iso_filename = None
original_iso_directory = None
original_iso_volume_id = None
original_iso_release_name = None
original_iso_disk_name = None

casper_relative_directory = None

########################################################################
# Custom
########################################################################

custom_iso_version_number = None
custom_iso_filepath = None
custom_iso_filename = None
custom_iso_dir = None
custom_iso_volume_id = None
custom_iso_release_name = None
custom_iso_disk_name = None

custom_iso_md5_sum = None
custom_iso_md5_filepath = None
custom_iso_md5_filename = None

########################################################################
# Status
########################################################################

is_success_copy_original_iso_files = None
is_success_extract_squashfs = None

########################################################################
# Options
########################################################################

undo_index = 0
undo_list = []
delete_list = []

########################################################################
# General
########################################################################


def set_terminal_pid(new_terminal_pid):
	global terminal_pid
	terminal_pid = new_terminal_pid
	dbg('The terminal pid is', terminal_pid)


def set_root_user_id(new_root_user_id):
	global root_user_id
	try:
		root_user_id = int(new_root_user_id)
	except TypeError:
		root_user_id = -1
	except ValueError:
		root_user_id = -1
	dbg('The root user id is', root_user_id)


def set_root_group_id(new_root_group_id):
	global root_group_id
	try:
		root_group_id = int(new_root_group_id)
	except TypeError:
		root_group_id = -1
	except ValueError:
		root_group_id = -1
	dbg('The root group id is', root_group_id)


def set_user_name(aname):
	global user_name
	user_name = aname.strip() if aname else ''
	dbg('The user name is', user_name)


def set_user_id(new_user_id):
	global user_id
	try:
		user_id = int(new_user_id)
	except TypeError:
		user_id = -1
	except ValueError:
		user_id = -1
	dbg('The user id is', user_id)


def set_group_id(new_group_id):
	global group_id
	try:
		group_id = int(new_group_id)
	except TypeError:
		group_id = -1
	except ValueError:
		group_id = -1
	dbg('The group id is', group_id)


def set_password(new_password):
	global password
	password = new_password.strip() if new_password else ''
	dbg('The password is', password)


def set_home(new_home):
	global home
	home = new_home.strip() if new_home else ''
	dbg('The home directory is', home)


def set_uris(new_uris):
	global uris
	uris = new_uris
	dbg('The uris is', uris)


def set_page_name(new_page_name):
	global page_name
	page_name = new_page_name.strip() if new_page_name else ''
	dbg('The current page name is', page_name)


def set_propagate(new_propagate):
	global propagate
	propagate = new_propagate
	dbg(
		'Propagate assigned values to calculate dependant values?',
		propagate)


def set_handler_id(new_handler_id):
	global handler_id
	handler_id = new_handler_id
	dbg('The handler id is', handler_id)


########################################################################
# Original
########################################################################


def set_original_iso_filename(new_original_iso_filename):
	global original_iso_filename
	original_iso_filename = new_original_iso_filename.strip(
	) if new_original_iso_filename else ''
	dbg(
		'The original ISO image filename is',
		original_iso_filename)


def set_original_iso_directory(new_original_iso_directory):
	global original_iso_directory
	original_iso_directory = new_original_iso_directory.strip(
	) if new_original_iso_directory else ''
	dbg(
		'The original ISO image directory is',
		original_iso_directory)


def set_original_iso_volume_id(new_original_iso_volume_id):
	global original_iso_volume_id
	original_iso_volume_id = new_original_iso_volume_id.strip(
	) if new_original_iso_volume_id else ''
	dbg(
		'The original ISO image volume id is',
		original_iso_volume_id)


def set_original_iso_release_name(new_original_iso_release_name):
	global original_iso_release_name
	original_iso_release_name = new_original_iso_release_name.strip(
	) if new_original_iso_release_name else ''
	dbg(
		'The original ISO image release name is',
		original_iso_release_name)


def set_original_iso_disk_name(new_original_iso_disk_name):
	global original_iso_disk_name
	original_iso_disk_name = new_original_iso_disk_name.strip(
	) if new_original_iso_disk_name else ''
	dbg(
		'The original ISO image disk name is',
		original_iso_disk_name)


def set_casper_relative_directory(new_casper_relative_directory):
	global casper_relative_directory
	casper_relative_directory = new_casper_relative_directory.strip(
	) if new_casper_relative_directory else ''
	dbg(
		'The casper relative directory is',
		casper_relative_directory)


########################################################################
# Custom
########################################################################


def set_custom_iso_version_number(new_custom_iso_version_number):
	global custom_iso_version_number
	custom_iso_version_number = new_custom_iso_version_number.strip(
	) if new_custom_iso_version_number else ''
	dbg(
		'The custom ISO image version number is',
		custom_iso_version_number)


def set_custom_iso_filepath(new_custom_iso_filepath):
	global custom_iso_filepath
	custom_iso_filepath = new_custom_iso_filepath.strip(
	) if new_custom_iso_filepath else ''
	dbg(
		'The custom ISO image filepath is',
		custom_iso_filepath)


def set_custom_iso_filename(new_custom_iso_filename):
	global custom_iso_filename
	custom_iso_filename = new_custom_iso_filename.strip(
	) if new_custom_iso_filename else ''
	dbg(
		'The custom ISO image filename is',
		custom_iso_filename)


def set_custom_iso_dir(apath):
	global custom_iso_dir
	custom_iso_dir = apath.strip(
	) if apath else ''
	dbg(
		'The custom ISO image directory is',
		custom_iso_dir)


def set_custom_iso_volume_id(new_custom_iso_volume_id):
	global custom_iso_volume_id
	custom_iso_volume_id = new_custom_iso_volume_id.strip(
	) if new_custom_iso_volume_id else ''
	dbg(
		'The custom ISO image volume id is',
		custom_iso_volume_id)


def set_custom_iso_release_name(new_custom_iso_release_name):
	global custom_iso_release_name
	custom_iso_release_name = new_custom_iso_release_name.strip(
	) if new_custom_iso_release_name else ''
	dbg(
		'The custom ISO image release name is',
		custom_iso_release_name)


def set_custom_iso_disk_name(new_custom_iso_disk_name):
	global custom_iso_disk_name
	custom_iso_disk_name = new_custom_iso_disk_name.strip(
	) if new_custom_iso_disk_name else ''
	dbg(
		'The custom ISO image disk name is',
		custom_iso_disk_name)


def set_custom_iso_md5_sum(new_custom_iso_md5_sum):
	global custom_iso_md5_sum
	custom_iso_md5_sum = new_custom_iso_md5_sum.strip(
	) if new_custom_iso_md5_sum else ''
	dbg(
		'The custom ISO image md5 sum is',
		custom_iso_md5_sum)


def set_custom_iso_md5_filepath(new_custom_iso_md5_filepath):
	global custom_iso_md5_filepath
	custom_iso_md5_filepath = new_custom_iso_md5_filepath.strip(
	) if new_custom_iso_md5_filepath else ''
	dbg(
		'The custom ISO image md5 filepath is',
		custom_iso_md5_filepath)


def set_custom_iso_md5_filename(new_custom_iso_md5_filename):
	global custom_iso_md5_filename
	custom_iso_md5_filename = new_custom_iso_md5_filename.strip(
	) if new_custom_iso_md5_filename else ''
	dbg(
		'The custom ISO image md5 filename is',
		custom_iso_md5_filename)


########################################################################
# Status
########################################################################


def set_is_success_copy_original_iso_files(aresult):
	global is_success_copy_original_iso_files
	is_success_copy_original_iso_files = aresult
	dbg(is_success_copy_original_iso_files)


def set_is_success_extract_squashfs(new_is_success_extract_squashfs):
	global is_success_extract_squashfs
	is_success_extract_squashfs = new_is_success_extract_squashfs
	dbg(
		'Is success extract squashfs?',
		is_success_extract_squashfs)


########################################################################
# Status
########################################################################






# TODO: Add a function add boot configurations as a list.
# TODO: Add a function add boot configurations as a tuple.
# TODO: Add function to remove a single boot configuration.
