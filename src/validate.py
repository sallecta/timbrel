#!/usr/bin/python3

########################################################################
#                                                                      #
# validate.py                                                        #
#                                                                      #
# Copyright (C) 2015 PJ Singh <psingh.cubic@gmail.com>                 #
#                                                                      #
########################################################################

########################################################################
#                                                                      #
# This file is part of Timbrel - Custom Ubuntu ISO Creator.              #
#                                                                      #
# Timbrel is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# (at your option) any later version.                                  #
#                                                                      #
# Timbrel is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public License    #
# along with Timbrel. If not, see <http://www.gnu.org/licenses/>.        #
#                                                                      #
########################################################################
from dbg import *
dbg = dbg_class(nd=1).dbg

import vars
import ui
import model
import utils


def project_directory_page():
	result = (bool(vars.prj.path.root.strip()))
	ui.set_sensitive('next_button', result)
	dbg('Is directory page, original section, valid?',result)


def new_project_page_custom():

	is_page_complete = True

	# Custom Iso Image Version Number (Optional)
	is_field_complete = bool(model.custom_iso_version_number.strip())
	# is_page_complete = is_page_complete and is_field_complete
	# ui.set_label_error(
	#     'new_project_page__custom_iso_version_number_label',
	#      not is_field_complete)
	# ui.set_entry_error(
	#     'new_project_page__custom_iso_version_number_entry',
	#     not is_field_complete)
	status = ui.OK if is_field_complete else ui.OPTIONAL
	ui.update_status(
		'new_project_page__custom_iso_version_number',
		status)

	# Custom Iso Image Filename
	is_field_complete = bool(
		model.custom_iso_filename.strip()
		and model.custom_iso_filename.strip()[0] != '.')
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'new_project_page__custom_iso_filename_label',
		not is_field_complete)
	ui.set_entry_error(
		'new_project_page__custom_iso_filename_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'new_project_page__custom_iso_filename',
		status)

	# Custom Iso Image Directory
	is_field_complete = bool(model.custom_iso_dir.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'new_project_page__custom_iso_dir_label',
		not is_field_complete)
	ui.set_entry_error(
		'new_project_page__custom_iso_dir_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'new_project_page__custom_iso_dir',
		status)

	# Custom Iso Image Volume Id
	is_field_complete = bool(model.custom_iso_volume_id.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'new_project_page__custom_iso_volume_id_label',
		not is_field_complete)
	ui.set_entry_error(
		'new_project_page__custom_iso_volume_id_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'new_project_page__custom_iso_volume_id',
		status)

	# Custom Iso Image Release Name (Optional)
	is_field_complete = bool(model.custom_iso_release_name.strip())
	# is_page_complete = is_page_complete and is_field_complete
	# ui.set_label_error(
	#     'new_project_page__custom_iso_release_name_label',
	#      not is_field_complete)
	# ui.set_entry_error(
	#     'new_project_page__custom_iso_release_name_entry',
	#     not is_field_complete)
	status = ui.OK if is_field_complete else ui.OPTIONAL
	ui.update_status(
		'new_project_page__custom_iso_release_name',
		status)

	# Custom Iso Image Disk Name
	is_field_complete = bool(model.custom_iso_disk_name.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'new_project_page__custom_iso_disk_name_label',
		not is_field_complete)
	ui.set_entry_error(
		'new_project_page__custom_iso_disk_name_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'new_project_page__custom_iso_disk_name',
		status)

	#
	# Set fields sensitive/insensitive or editable/non-editable based on results.
	#

	ui.set_sensitive('next_button', is_page_complete)

	dbg(
		'Is new project page, custom section, valid?',
		is_page_complete)


def new_project_page():

	is_page_complete = True

	#
	# Validate new project page, original section.
	#

	# Original Iso Image Filename
	is_field_complete = (
		bool(model.original_iso_filename.strip())
		and utils.is_mounted(vars.prj.path.o_iso,vars.prj.path.o_iso_m))
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'new_project_page__original_iso_filename_label',
		not is_field_complete)
	ui.set_entry_error(
		'new_project_page__original_iso_filename_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'new_project_page__original_iso_filename',
		status)

	# Original Iso Image Directory
	is_field_complete = bool(model.original_iso_directory.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'new_project_page__original_iso_directory_label',
		not is_field_complete)
	ui.set_entry_error(
		'new_project_page__original_iso_directory_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'new_project_page__original_iso_directory',
		status)

	# Original Iso Image Volume Id
	is_field_complete = bool(model.original_iso_volume_id.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'new_project_page__original_iso_volume_id_label',
		not is_field_complete)
	ui.set_entry_error(
		'new_project_page__original_iso_volume_id_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'new_project_page__original_iso_volume_id',
		status)

	# Original Iso Image Release Name
	is_field_complete = bool(model.original_iso_release_name.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'new_project_page__original_iso_release_name_label',
		not is_field_complete)
	ui.set_entry_error(
		'new_project_page__original_iso_release_name_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'new_project_page__original_iso_release_name',
		status)

	# Original Iso Image Disk Name
	is_field_complete = bool(model.original_iso_disk_name.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'new_project_page__original_iso_disk_name_label',
		not is_field_complete)
	ui.set_entry_error(
		'new_project_page__original_iso_disk_name_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'new_project_page__original_iso_disk_name',
		status)

	#
	# Set fields sensitive/insensitive or editable/non-editable based on results.
	#

	ui.set_entry_editable(
		'new_project_page__custom_iso_version_number_entry',
		is_page_complete)
	ui.set_entry_editable(
		'new_project_page__custom_iso_filename_entry',
		is_page_complete)
	# ui.set_entry_editable(
	#     'new_project_page__custom_iso_dir_entry',
	#     is_page_complete)
	ui.set_sensitive(
		'new_project_page__custom_iso_dir_filechooser__open_button',
		is_page_complete)
	ui.set_entry_editable(
		'new_project_page__custom_iso_volume_id_entry',
		is_page_complete)
	ui.set_entry_editable(
		'new_project_page__custom_iso_release_name_entry',
		is_page_complete)
	ui.set_entry_editable(
		'new_project_page__custom_iso_disk_name_entry',
		is_page_complete)

	dbg(
		'Is new project page, original section, valid?',
		is_page_complete)

	#
	# Validate new project page, custom section.
	#

	# Custom Iso Image Version Number (Optional)
	is_field_complete = bool(model.custom_iso_version_number.strip())
	# is_page_complete = is_page_complete and is_field_complete
	# ui.set_label_error(
	#     'new_project_page__custom_iso_version_number_label',
	#     not is_field_complete)
	# ui.set_entry_error(
	#     'new_project_page__custom_iso_version_number_entry',
	#     not is_field_complete)
	status = ui.OK if is_field_complete else ui.OPTIONAL
	ui.update_status(
		'new_project_page__custom_iso_version_number',
		status)

	# Custom Iso Image Filename
	is_field_complete = bool(
		model.custom_iso_filename.strip()
		and model.custom_iso_filename.strip()[0] != '.')
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'new_project_page__custom_iso_filename_label',
		not is_field_complete)
	ui.set_entry_error(
		'new_project_page__custom_iso_filename_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'new_project_page__custom_iso_filename',
		status)

	# Custom Iso Image Directory
	is_field_complete = bool(model.custom_iso_dir.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'new_project_page__custom_iso_dir_label',
		not is_field_complete)
	ui.set_entry_error(
		'new_project_page__custom_iso_dir_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'new_project_page__custom_iso_dir',
		status)

	# Custom Iso Image Volume Id
	is_field_complete = bool(model.custom_iso_volume_id.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'new_project_page__custom_iso_volume_id_label',
		not is_field_complete)
	ui.set_entry_error(
		'new_project_page__custom_iso_volume_id_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'new_project_page__custom_iso_volume_id',
		status)

	# Custom Iso Image Release Name (Optional)
	is_field_complete = bool(model.custom_iso_release_name.strip())
	# is_page_complete = is_page_complete and is_field_complete
	# ui.set_label_error(
	#     'new_project_page__custom_iso_release_name_label',
	#     not is_field_complete)
	# ui.set_entry_error(
	#     'new_project_page__custom_iso_release_name_entry',
	#     not is_field_complete)
	status = ui.OK if is_field_complete else ui.OPTIONAL
	ui.update_status(
		'new_project_page__custom_iso_release_name',
		status)

	# Custom Iso Image Disk Name
	is_field_complete = bool(model.custom_iso_disk_name.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'new_project_page__custom_iso_disk_name_label',
		not is_field_complete)
	ui.set_entry_error(
		'new_project_page__custom_iso_disk_name_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'new_project_page__custom_iso_disk_name',
		status)

	#
	# Set fields sensitive/insensitive or editable/non-editable based on results.
	#

	ui.set_sensitive('next_button', is_page_complete)

	dbg(
		'Is new project page, custom section, valid?',
		is_page_complete)


# TODO: This function is not used.
def existing_project_page_original():

	is_page_complete = True

	# Original Iso Image Filename
	is_field_complete = (
		bool(model.original_iso_filename.strip())
		and utils.is_mounted(vars.prj.path.o_iso,vars.prj.path.o_iso_m))
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__original_iso_filename_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__original_iso_filename_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__original_iso_filename',
		status)

	# Original Iso Image Directory
	is_field_complete = bool(model.original_iso_directory.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__original_iso_directory_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__original_iso_directory_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__original_iso_directory',
		status)

	# Original Iso Image Volume Id
	is_field_complete = bool(model.original_iso_volume_id.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__original_iso_volume_id_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__original_iso_volume_id_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__original_iso_volume_id',
		status)

	# Original Iso Image Release Name
	is_field_complete = bool(model.original_iso_release_name.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__original_iso_release_name_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__original_iso_release_name_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__original_iso_release_name',
		status)

	# Original Iso Image Disk Name
	is_field_complete = bool(model.original_iso_disk_name.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__original_iso_disk_name_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__original_iso_disk_name_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__original_iso_disk_name',
		status)

	#
	# Set fields sensitive/insensitive or editable/non-editable based on results.
	#

	ui.set_entry_editable(
		'existing_project_page__custom_iso_version_number_entry',
		is_page_complete)
	ui.set_entry_editable(
		'existing_project_page__custom_iso_filename_entry',
		is_page_complete)
	# ui.set_entry_editable(
	#     'existing_project_page__custom_iso_dir_entry',
	#     is_page_complete)
	ui.set_sensitive(
		'existing_project_page__custom_iso_dir_filechooser__open_button',
		is_page_complete)
	ui.set_entry_editable(
		'existing_project_page__custom_iso_volume_id_entry',
		is_page_complete)
	ui.set_entry_editable(
		'existing_project_page__custom_iso_release_name_entry',
		is_page_complete)
	ui.set_entry_editable(
		'existing_project_page__custom_iso_disk_name_entry',
		is_page_complete)

	dbg(
		'Is existing project page, original section, valid?',
		is_page_complete)


def existing_project_page_custom():

	is_page_complete = True

	# Custom Iso Image Version Number (Optional)
	is_field_complete = bool(model.custom_iso_version_number.strip())
	# is_page_complete = is_page_complete and is_field_complete
	# ui.set_label_error(
	#     'existing_project_page__custom_iso_version_number_label',
	#     not is_field_complete)
	# ui.set_entry_error(
	#     'existing_project_page__custom_iso_version_number_entry',
	#     not is_field_complete)
	status = ui.OK if is_field_complete else ui.OPTIONAL
	ui.update_status(
		'existing_project_page__custom_iso_version_number',
		status)

	# Custom Iso Image Filename
	is_field_complete = bool(
		model.custom_iso_filename.strip()
		and model.custom_iso_filename.strip()[0] != '.')
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__custom_iso_filename_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__custom_iso_filename_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__custom_iso_filename',
		status)

	# Custom Iso Image Directory
	is_field_complete = bool(model.custom_iso_dir.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__custom_iso_dir_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__custom_iso_dir_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__custom_iso_dir',
		status)

	# Custom Iso Image Volume Id
	is_field_complete = bool(model.custom_iso_volume_id.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__custom_iso_volume_id_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__custom_iso_volume_id_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__custom_iso_volume_id',
		status)

	# Custom Iso Image Release Name (Optional)
	is_field_complete = bool(model.custom_iso_release_name.strip())
	# is_page_complete = is_page_complete and is_field_complete
	# ui.set_label_error(
	#     'existing_project_page__custom_iso_release_name_label',
	#     not is_field_complete)
	# ui.set_entry_error(
	#     'existing_project_page__custom_iso_release_name_entry',
	#     not is_field_complete)
	status = ui.OK if is_field_complete else ui.OPTIONAL
	ui.update_status(
		'existing_project_page__custom_iso_release_name',
		status)

	# Custom Iso Image Disk Name
	is_field_complete = bool(model.custom_iso_disk_name.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__custom_iso_disk_name_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__custom_iso_disk_name_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__custom_iso_disk_name',
		status)

	#
	# Set fields sensitive/insensitive or editable/non-editable based on results.
	#

	ui.set_sensitive('next_button', is_page_complete)

	dbg(
		'Is existing project page, custom section, valid?',
		is_page_complete)


def existing_project_page():

	is_page_complete = True

	#
	# Validate existing project page, original section.
	#

	# Original Iso Image Filename
	is_field_complete = (
		bool(model.original_iso_filename.strip())
		and utils.is_mounted(vars.prj.path.o_iso,vars.prj.path.o_iso_m))
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__original_iso_filename_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__original_iso_filename_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__original_iso_filename',
		status)

	# Original Iso Image Directory
	is_field_complete = bool(model.original_iso_directory.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__original_iso_directory_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__original_iso_directory_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__original_iso_directory',
		status)

	# Original Iso Image Volume Id
	is_field_complete = bool(model.original_iso_volume_id.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__original_iso_volume_id_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__original_iso_volume_id_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__original_iso_volume_id',
		status)

	# Original Iso Image Release Name
	is_field_complete = bool(model.original_iso_release_name.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__original_iso_release_name_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__original_iso_release_name_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__original_iso_release_name',
		status)

	# Original Iso Image Disk Name
	is_field_complete = bool(model.original_iso_disk_name.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__original_iso_disk_name_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__original_iso_disk_name_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__original_iso_disk_name',
		status)

	#
	# Set fields sensitive/insensitive or editable/non-editable based on results.
	#

	ui.set_entry_editable(
		'existing_project_page__custom_iso_version_number_entry',
		is_page_complete)
	ui.set_entry_editable(
		'existing_project_page__custom_iso_filename_entry',
		is_page_complete)
	# ui.set_entry_editable(
	#     'existing_project_page__custom_iso_dir_entry',
	#     is_page_complete)
	ui.set_sensitive(
		'existing_project_page__custom_iso_dir_filechooser__open_button',
		is_page_complete)
	ui.set_entry_editable(
		'existing_project_page__custom_iso_volume_id_entry',
		is_page_complete)
	ui.set_entry_editable(
		'existing_project_page__custom_iso_release_name_entry',
		is_page_complete)
	ui.set_entry_editable(
		'existing_project_page__custom_iso_disk_name_entry',
		is_page_complete)

	dbg(
		'Is existing project page, original section, valid?',
		is_page_complete)

	#
	# Validate existing project page, custom section.
	#

	# Custom Iso Image Version Number (Optional)
	is_field_complete = bool(model.custom_iso_version_number.strip())
	# is_page_complete = is_page_complete and is_field_complete
	# ui.set_label_error(
	#     'existing_project_page__custom_iso_version_number_label',
	#     not is_field_complete)
	# ui.set_entry_error(
	#     'existing_project_page__custom_iso_version_number_entry',
	#     not is_field_complete)
	status = ui.OK if is_field_complete else ui.OPTIONAL
	ui.update_status(
		'existing_project_page__custom_iso_version_number',
		status)

	# Custom Iso Image Filename
	is_field_complete = bool(
		model.custom_iso_filename.strip()
		and model.custom_iso_filename.strip()[0] != '.')
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__custom_iso_filename_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__custom_iso_filename_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__custom_iso_filename',
		status)

	# Custom Iso Image Directory
	is_field_complete = bool(model.custom_iso_dir.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__custom_iso_dir_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__custom_iso_dir_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__custom_iso_dir',
		status)

	# Custom Iso Image Volume Id
	is_field_complete = bool(model.custom_iso_volume_id.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__custom_iso_volume_id_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__custom_iso_volume_id_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__custom_iso_volume_id',
		status)

	# Custom Iso Image Release Name (Optional)
	is_field_complete = bool(model.custom_iso_release_name.strip())
	# is_page_complete = is_page_complete and is_field_complete
	# ui.set_label_error(
	#     'existing_project_page__custom_iso_release_name_label',
	#     not is_field_complete)
	# ui.set_entry_error(
	#     'existing_project_page__custom_iso_release_name_entry',
	#     not is_field_complete)
	status = ui.OK if is_field_complete else ui.OPTIONAL
	ui.update_status(
		'existing_project_page__custom_iso_release_name',
		status)

	# Custom Iso Image Disk Name
	is_field_complete = bool(model.custom_iso_disk_name.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__custom_iso_disk_name_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__custom_iso_disk_name_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__custom_iso_disk_name',
		status)

	#
	# Set fields sensitive/insensitive or editable/non-editable based on results.
	#

	ui.set_sensitive('next_button', is_page_complete)

	dbg(
		'Is existing project page, custom section, valid?',
		is_page_complete)


def existing_project_page_for_delete():

	is_page_complete = True

	#
	# Validate existing project page, original section.
	#

	# Original Iso Image Filename
	is_field_complete = (
		bool(model.original_iso_filename.strip())
		and utils.is_mounted(vars.prj.path.o_iso,vars.prj.path.o_iso_m))
	is_page_complete = is_page_complete and is_field_complete

	ui.set_label_error(
		'existing_project_page__original_iso_filename_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__original_iso_filename_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__original_iso_filename',
		status)

	# Original Iso Image Directory
	is_field_complete = bool(model.original_iso_directory.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__original_iso_directory_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__original_iso_directory_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__original_iso_directory',
		status)

	# Original Iso Image Volume Id
	is_field_complete = bool(model.original_iso_volume_id.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__original_iso_volume_id_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__original_iso_volume_id_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__original_iso_volume_id',
		status)

	# Original Iso Image Release Name
	is_field_complete = bool(model.original_iso_release_name.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__original_iso_release_name_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__original_iso_release_name_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__original_iso_release_name',
		status)

	# Original Iso Image Disk Name
	is_field_complete = bool(model.original_iso_disk_name.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__original_iso_disk_name_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__original_iso_disk_name_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__original_iso_disk_name',
		status)

	#
	# Set fields sensitive/insensitive or editable/non-editable based on results.
	#

	ui.set_entry_editable(
		'existing_project_page__custom_iso_version_number_entry',
		False)
	ui.set_entry_editable(
		'existing_project_page__custom_iso_filename_entry',
		False)
	# ui.set_entry_editable(
	#     'existing_project_page__custom_iso_dir_entry',
	#     False)
	ui.set_sensitive(
		'existing_project_page__custom_iso_dir_filechooser__open_button',
		False)
	ui.set_entry_editable(
		'existing_project_page__custom_iso_volume_id_entry',
		False)
	ui.set_entry_editable(
		'existing_project_page__custom_iso_release_name_entry',
		False)
	ui.set_entry_editable(
		'existing_project_page__custom_iso_disk_name_entry',
		False)

	dbg(
		'Is existing project page, original section, for delete valid?',
		is_page_complete)

	#
	# Validate existing project page, custom section.
	#

	# Custom Iso Image Version Number (Optional)
	is_field_complete = bool(model.custom_iso_version_number.strip())
	# is_page_complete = is_page_complete and is_field_complete
	# ui.set_label_error(
	#     'existing_project_page__custom_iso_version_number_label',
	#     not is_field_complete)
	# ui.set_entry_error(
	#     'existing_project_page__custom_iso_version_number_entry',
	#     not is_field_complete)
	status = ui.OK if is_field_complete else ui.OPTIONAL
	ui.update_status(
		'existing_project_page__custom_iso_version_number',
		status)

	# Custom Iso Image Filename
	is_field_complete = bool(
		model.custom_iso_filename.strip()
		and model.custom_iso_filename.strip()[0] != '.')
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__custom_iso_filename_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__custom_iso_filename_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__custom_iso_filename',
		status)

	# Custom Iso Image Directory
	is_field_complete = bool(model.custom_iso_dir.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__custom_iso_dir_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__custom_iso_dir_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__custom_iso_dir',
		status)

	# Custom Iso Image Volume Id
	is_field_complete = bool(model.custom_iso_volume_id.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__custom_iso_volume_id_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__custom_iso_volume_id_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__custom_iso_volume_id',
		status)

	# Custom Iso Image Release Name (Optional)
	is_field_complete = bool(model.custom_iso_release_name.strip())
	# is_page_complete = is_page_complete and is_field_complete
	# ui.set_label_error(
	#     'existing_project_page__custom_iso_release_name_label',
	#     not is_field_complete)
	# ui.set_entry_error(
	#     'existing_project_page__custom_iso_release_name_entry',
	#     not is_field_complete)
	status = ui.OK if is_field_complete else ui.OPTIONAL
	ui.update_status(
		'existing_project_page__custom_iso_release_name',
		status)

	# Custom Iso Image Disk Name
	is_field_complete = bool(model.custom_iso_disk_name.strip())
	is_page_complete = is_page_complete and is_field_complete
	ui.set_label_error(
		'existing_project_page__custom_iso_disk_name_label',
		not is_field_complete)
	ui.set_entry_error(
		'existing_project_page__custom_iso_disk_name_entry',
		not is_field_complete)
	status = ui.OK if is_field_complete else ui.ERROR
	ui.update_status(
		'existing_project_page__custom_iso_disk_name',
		status)

	#
	# Set fields sensitive/insensitive or editable/non-editable based on results.
	#

	ui.set_sensitive('next_button', True)

	dbg(
		'Is existing project page, custom section, for delete valid?',
		is_page_complete)
